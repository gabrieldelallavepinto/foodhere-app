import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:tabitas/core/models/settings/setting_home.dart';

class AppConfig {
  //Tabitas
  static const String businessId = 'zLIzDEFA1hVwXKWLCxLR';
  static const String businessName = 'Tabita\'s';
  static const String androidAppId = 'com.foodhere.tabitas';
  static const String iOSAppId = '1280283241';
  static const String googleMapsApiKey_android = 'AIzaSyDYU2J43B4nYoB7JE1gwOXOKqv4PuyKG4s';
  static const String googleMapsApiKey_iOS = 'AIzaSyAi1yPX6E9PYTRiEV3tNeBGRfDmiQfV_w4';

  static const Map<String, String> statusName = {
    'waiting_sp_validation' : 'En espera',
    'accepted' : 'Recibido',
    'preparation' : 'En cocina',
    'incident' : 'Incidencia',
    'ready' : 'Preparado',
    'cancelled' : 'Cancelado',
    'delivering' : 'En trayecto',
    'finished' : 'Finalizado',
  };

  /*
  //Pizzeria Mario
  static const String businessId = '1djUlxWwzwfcygzkwGr3';
  static const String businessName = 'Pizzería Burguer Mario';
  static const String androidAppId = 'com.foodhere.mario';
  static const String iOSAppId = '1280283241';
  static const String googleMapsApiKey_android = 'AIzaSyDYU2J43B4nYoB7JE1gwOXOKqv4PuyKG4s';
  static const String googleMapsApiKey_iOS = 'AIzaSyAi1yPX6E9PYTRiEV3tNeBGRfDmiQfV_w4';
   */
}