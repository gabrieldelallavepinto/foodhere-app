import 'dart:ui';
import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryText = Color.fromARGB(255, 91, 91, 91);
  static const Color secondaryText = Color.fromARGB(255, 204, 25, 10);
  static const Color accentText = Color.fromARGB(255, 115, 115, 115);
  static const Color keyDartGrey = Color.fromARGB(255, 91, 91, 91);

  static const Map<String, Color> statusColor = {
    'waiting_sp_validation' : Colors.orangeAccent,
    'accepted' : Colors.blueAccent,
    'preparation' : Colors.pinkAccent,
    'ready' : Colors.blueAccent,
    'incident' : Colors.orange,
    'cancelled' : Colors.redAccent,
    'delivering' : Colors.deepPurpleAccent,
    'finished' : Colors.green,
  };
}