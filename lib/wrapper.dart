import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tabitas/core/viewmodels/category_model.dart';
import 'core/models/business_settings.dart';
import 'core/models/category.dart';
import 'core/models/client.dart';
import 'core/models/payment_methods.dart';
import 'core/models/promotion.dart';
import 'core/viewmodels/promotion_model.dart';
import 'core/viewmodels/userViewModel.dart';
import 'ui/theme.dart';
import 'core/models/stores.dart';
import 'ui/views/shared/update_app.dart';
import 'ui/views/stores/store_selection.dart';
import 'core/viewmodels/store_model.dart';
import 'ui/views/home/home.dart';
import 'ui/views/loading/loading.dart';

import 'core/models/business.dart';
import 'core/viewmodels/business_model.dart';

class Wrapper extends StatefulWidget {
  int initScreen;
  bool dataInitialized = false;

  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  StreamSubscription iosSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        EdgeAlert.show(context,
            title: message['notification']['title'],
            description: message['notification']['body'],
            gravity: EdgeAlert.TOP,
            backgroundColor: theme.accentColor);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserViewModel>(context);

    if (userProvider.status == Status.Uninitialized ||
        userProvider.status == Status.Authenticating) {
      return Loading();
    }

    if (userProvider.status == Status.Unauthenticated) {
      return _buildInitialPage(null);
    }

    if (userProvider.status == Status.Authenticated) {
      return Consumer(builder: (context, UserViewModel user, _) {
        return _buildInitialPage(user);
      });
    }
  }

  Future<String> loadConfig() async {
    return await rootBundle.loadString('assets/config.json');
  }

  _saveDeviceToken(String uid) async {
    final businessProvider = Provider.of<BusinessModel>(context);
    String fcmToken = await _fcm.getToken();

    if (fcmToken != null) {
      var tokens = _db
          .collection('business')
          .document(businessProvider.business.id)
          .collection('clients')
          .document(uid)
          .collection('tokens')
          .document(fcmToken);

      await tokens.setData({
        'token': fcmToken,
        'createdAt': FieldValue.serverTimestamp(),
        'platform': Platform.operatingSystem
      });
    }
  }

  Future<void> setInitialScreenSeen() async {
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.initScreen = prefs.getInt("initScreen");
    await prefs.setInt("initScreen", 1);
  }

  Widget _buildInitialPage(UserViewModel user) {
    final businessProvider = Provider.of<BusinessModel>(context);
    final storeProvider = Provider.of<StoreModel>(context);
    final promotionProvider = Provider.of<PromotionModel>(context);
    final categoryProvider = Provider.of<CategoryModel>(context);

    return FutureBuilder(
        future: Future.wait([
          businessProvider.getBusiness(),
          businessProvider.getSettingPaymentMethods(),
          businessProvider.getSettingTermsConditions(),
          storeProvider.fetchStores(),
          promotionProvider.fetchPromotions(),
          storeProvider.readSavedStore(),
          categoryProvider.fetchCategories(),
        ]).then((value) => Merged(
            business: value[0],
            paymentMethods: value[1],
            termsConditions: value[2],
            stores: value[3],
            promotions: value[4],
            storeId: value[5],
            categories: value[6])),
        builder: (BuildContext context, AsyncSnapshot<Merged> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Loading();
          } else {
            Client client;
            if (user != null) {
              _saveDeviceToken(user.user.uid);
              client = user.userProfile;
            }
            if (snapshot.data?.storeId == null) {
              return _buildInitPage(storeProvider, client);
            } else {
              Store store = storeProvider.stores
                  .where((element) => element.id == snapshot.data.storeId)
                  .where((element) => element.active == true)
                  .first;
              if (store != null) {
                storeProvider.selectedStore = store;
                return UpdateApp(child: Home(user: client));
              } else {
                return _buildInitPage(storeProvider, client);
              }
            }
          }
        });
  }

  Widget _buildInitPage(StoreModel storeProvider, Client client) {
    List<Store> stores = storeProvider.stores
        .where((element) => element.active == true)
        .toList();
    if (stores.length > 1) {
      return StoreSelecction(data: {'cancelOrder': false, 'initial': true});
    } else if (stores.length == 1) {
      storeProvider.saveStore(stores.first.id);
      storeProvider.selectedStore = stores.first;
    }
    return UpdateApp(child: Home(user: client));
  }
}

class Merged {
  final Business business;
  final PaymentMethods paymentMethods;
  final TermsConditions termsConditions;
  final List<Store> stores;
  final List<Promotion> promotions;
  final String storeId;
  final List<Category> categories;

  Merged(
      {this.business,
      this.paymentMethods,
      this.termsConditions,
      this.stores,
      this.promotions,
      this.storeId,
      this.categories});
}
