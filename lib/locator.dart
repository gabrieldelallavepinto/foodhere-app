import 'package:get_it/get_it.dart';
import 'package:tabitas/core/viewmodels/app_channel_model.dart';
import 'package:tabitas/core/viewmodels/location_model.dart';
import 'package:tabitas/core/viewmodels/order_model.dart';
import 'core/services/api.dart';

import 'core/viewmodels/category_model.dart';
import 'core/viewmodels/store_model.dart';
import 'core/viewmodels/business_model.dart';
import 'core/viewmodels/product_model.dart';
import 'core/viewmodels/promotion_model.dart';


GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => BusinessModel()) ;
  locator.registerLazySingleton(() => AppChannelModel()) ;
  locator.registerLazySingleton(() => OrderModel()) ;
  locator.registerLazySingleton(() => CategoryModel()) ;
  locator.registerLazySingleton(() => ProductModel()) ;
  locator.registerLazySingleton(() => PromotionModel()) ;
  locator.registerLazySingleton(() => StoreModel()) ;
  locator.registerLazySingleton(() => LocationModel()) ;
}