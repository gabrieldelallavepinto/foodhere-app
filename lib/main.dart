import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/location_model.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import './ui/router.dart';
import './ui/theme.dart';
import 'core/viewmodels/business_model.dart';
import 'core/viewmodels/category_model.dart';
import 'core/viewmodels/order_model.dart';
import 'core/viewmodels/store_model.dart';
import 'core/viewmodels/product_model.dart';
import 'core/viewmodels/promotion_model.dart';
import 'locator.dart';

main() {
  setupLocator();
  runApp(FoodApp());
}

class FoodApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => locator<ProductModel>()),
        ChangeNotifierProvider(create: (_) => locator<OrderModel>()),
        ChangeNotifierProvider(create: (_) => locator<CategoryModel>()),
        ChangeNotifierProvider(create: (_) => locator<StoreModel>()),
        ChangeNotifierProvider(create: (_) => locator<BusinessModel>()),
        ChangeNotifierProvider(create: (_) => locator<PromotionModel>()),
        ChangeNotifierProvider(create: (_) => locator<LocationModel>()),
        ChangeNotifierProvider(create: (_) => UserViewModel.instance())
      ],
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('es', 'ES'), // Español
          const Locale('en', 'US'), // English
        ],
        debugShowCheckedModeBanner: false,
        initialRoute: 'wrapper',
        title: 'Product App',
        theme: theme
        ,
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }


}
