import 'package:cloud_firestore/cloud_firestore.dart';

class AppVersion {
  static Future<DocumentSnapshot> getVersion() async {
    return await Firestore.instance
        .collection('channels')
        .document('app')
        .get();
  }
}
