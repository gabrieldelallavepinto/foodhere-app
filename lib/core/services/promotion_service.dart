import '../../core/models/image_config.dart';
import '../../core/models/promotion.dart';

class PromotionService {

  static String getPromoList( Promotion promotion) {
    String url = 'https://via.placeholder.com/150';
    promotion.images.forEach( (image)  {
      if (image.id == 'promo_list') {
        url =  image.imageUrl;
      }
    });

    return url;
  }

  static String getPromoHighlighted( Promotion promotion) {
    String url = 'https://via.placeholder.com/150';
    promotion.images.forEach( (image)  {
      if (image.id == 'promo_highlight') {
        url =  image.imageUrl;
      }
    });
    return url;
  }
}