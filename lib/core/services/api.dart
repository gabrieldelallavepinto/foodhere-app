import 'package:cloud_firestore/cloud_firestore.dart';
import '../../appConfig/appconfig.dart';

class Api {
  final Firestore _db = Firestore.instance;
  DocumentReference businessDoc;
  CollectionReference ref;

  Api() {
    businessDoc = _db.collection('business').document(AppConfig.businessId);
  }

  void setCollection({String collection}) {
    ref = businessDoc.collection(collection);
  }

  Future<QuerySnapshot> getDataCollection() {
    return ref.getDocuments();
  }

  Stream<QuerySnapshot> streamDataCollection() {
    return ref.snapshots();
  }

  Future<DocumentSnapshot> getDocumentById(String id) {
    return ref.document(id).get();
  }

  Stream<QuerySnapshot> streamProductsCollection({String categoryId}) {
    return ref
        .where('category_id', isEqualTo: categoryId)
        .orderBy('sort_order', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> streamCategoriesCollection() {
    return ref
        .where('active', isEqualTo: true)
        .orderBy('sort_order', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> streamStripeCardsCollection(String uid) {
    return ref.document(uid).collection('stripe_cards').snapshots();
  }

  Stream<QuerySnapshot> streamOrdersCollection({String uid}) {
    return ref
        .where('client.id', isEqualTo: uid)
        .orderBy('execution_time', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> streamTrackingOrderCollection({String id}) {
    return ref
        .document(id)
        .collection('tracking')
        //.where('public', isEqualTo: true)
        .orderBy('date', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> streamPromotionsCollection() {
    return ref
        .where('active', isEqualTo: true)
        .orderBy('sort_order', descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> streamHighlightedPromotionsCollection() {
    return ref
        .where('active', isEqualTo: true)
        .where('highlighted', isEqualTo: true)
        .orderBy('sort_order', descending: false)
        .snapshots();
  }

  Future<DocumentSnapshot> getBusinessDocument() {
    return businessDoc.get();
  }

  Future<DocumentSnapshot> getAppChannelDocument() {
    return _db.collection('channels').document('app').get();
  }

  Future<void> removeDocument(String id) {
    return ref.document(id).delete();
  }

  Future<DocumentReference> addDocument(Map data) {
    return ref.add(data);
  }

  Future<void> addDocumentWithId(Map data, String id) {
    return ref.document(id).setData(data);
  }

  Future<DocumentReference> updateDocument(Map data, String id) {
    return ref.document(id).updateData(data);
  }
}
