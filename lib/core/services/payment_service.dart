import 'package:cloud_firestore/cloud_firestore.dart';
import '../../appConfig/appconfig.dart';

class PaymentService {
  addCard(
      {Map<String, dynamic> paymentMethod,
      String paymentMethodId,
      String uid,
      String orderId}) {
    Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(uid)
        .collection('stripe_cards')
        .document(paymentMethodId)
        .setData(paymentMethod)
        .then((value) {
      print('card saved');
    });
  }

  deleteCard(String id, String uid) {
    Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(uid)
        .collection('stripe_cards')
        .document(id)
        .delete();
  }

  setSelected(String id, String uid) {
    Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(uid)
        .collection('stripe_cards')
        .document(id)
        .updateData({"selected": true});
  }
}
