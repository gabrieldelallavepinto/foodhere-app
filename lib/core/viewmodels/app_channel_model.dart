import 'package:flutter/material.dart';
import 'package:tabitas/core/models/app_channel.dart';
import 'package:tabitas/core/services/api.dart';
import 'package:tabitas/locator.dart';

class AppChannelModel  extends ChangeNotifier  {
  Api _api = locator<Api>();
  AppChannel appChannel;

  Future<AppChannel> getVersion() async {
    var doc = await _api.getAppChannelDocument();
    appChannel = AppChannel.fromMap(doc.data);
    return  appChannel ;
  }
}