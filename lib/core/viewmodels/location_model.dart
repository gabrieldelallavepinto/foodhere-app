import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class LocationModel extends ChangeNotifier {
  Position position;

  Future<Position> getLocation() async {
    Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
    GeolocationStatus geolocationStatus =
        await Geolocator().checkGeolocationPermissionStatus();
    position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  Future<double> calculateDistance({double latitude, double longitude}) async {
    return await Geolocator().distanceBetween(
        position.latitude, position.longitude, latitude, longitude);
  }
}
