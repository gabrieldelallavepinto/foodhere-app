import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../../core/models/promotion.dart';
import '../../locator.dart';
import '../services/api.dart';

class PromotionModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Promotion> promotions;

  Future<List<Promotion>> fetchPromotions() async {
    _api.setCollection(collection: 'promotions');
    var result = await _api.getDataCollection();
    promotions =
        result.documents.map((doc) => Promotion.fromMap(doc.data)).toList();
    return promotions;
  }

  Stream<QuerySnapshot> fetchPromotionsAsStream() {
    _api.setCollection(collection: 'promotions');
    return _api.streamPromotionsCollection();
  }

  Stream<QuerySnapshot> fetchHighlightedPromotionsAsStream() {
    _api.setCollection(collection: 'promotions');
    return _api.streamHighlightedPromotionsCollection();
  }


  Future<Promotion> getPromotionById(String id) async {
    _api.setCollection(collection: 'promotions');
    var doc = await _api.getDocumentById(id);
    return Promotion.fromMap(doc.data);
  }

  Future removePromotion(String id) async {
    _api.setCollection(collection: 'promotions');
    await _api.removeDocument(id);
    return;
  }

  Future updatePromotion(Promotion data, String id) async {
    _api.setCollection(collection: 'promotions');
    await _api.updateDocument(data.toJson(), id);
    return;
  }

  Future addPromotion(Promotion data) async {
    _api.setCollection(collection: 'promotions');
    return await _api.addDocument(data.toJson());
  }
}
