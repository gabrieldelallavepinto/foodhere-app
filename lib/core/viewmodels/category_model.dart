import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';
import '../models/category.dart';
import '../services/api.dart';

class CategoryModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Category> categories;

  Future<List<Category>> fetchCategories() async {
    _api.setCollection(collection: 'categories');
    var result = await _api.getDataCollection();
    categories = result.documents
        .map((doc) => Category.fromMap(doc.data))
        .toList();
    return categories;
  }

  Stream<QuerySnapshot> fetchCategoriesAsStream() {
    _api.setCollection(collection: 'categories');
    return _api.streamCategoriesCollection();
  }

  Future<Category> getStoreById(String id) async {
    _api.setCollection(collection: 'categories');
    var doc = await _api.getDocumentById(id);
    return Category.fromMap(doc.data);
  }

  Future removeCategory(String id) async {
    _api.setCollection(collection: 'categories');
    await _api.removeDocument(id);
    return Future.delayed(Duration.zero);
  }

  Future updateCategory(Category data, String id) async {
    _api.setCollection(collection: 'categories');
    await _api.updateDocument(data.toJson(), id);
    return Future.delayed(Duration.zero);
  }

  Future addCategory(Category data) async {
    _api.setCollection(collection: 'categories');
    await _api.addDocument(data.toJson());
    return Future.delayed(Duration.zero);
  }
}
