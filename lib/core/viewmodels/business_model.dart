import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tabitas/core/models/business_settings.dart';
import 'package:tabitas/core/models/payment_methods.dart';
import '../models/business.dart';
import '../../locator.dart';
import '../services/api.dart';

class BusinessModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Business> allBusiness;
  Business business;
  PaymentMethods paymentMethods;
  TermsConditions termConditions;

  Future<List<Business>> fetchBusiness() async {
    var result = await _api.getDataCollection();
    allBusiness = result.documents
        .map((doc) => Business.fromMap(doc.data))
        .toList();
    return allBusiness;
  }

  Stream<QuerySnapshot> fetchBusinessAsStream() {
    return _api.streamDataCollection();
  }

  Future<PaymentMethods> getSettingPaymentMethods() async {
    _api.setCollection(collection: 'settings');
    var doc = await _api.getDocumentById('payment_methods');
    paymentMethods = PaymentMethods.fromMap(doc.data);
    return paymentMethods;
  }

  Future<TermsConditions> getSettingTermsConditions() async {
    _api.setCollection(collection: 'settings');
    var doc = await _api.getDocumentById('terms_conditions');
    termConditions = TermsConditions.fromMap(doc.data);
    return termConditions;
  }

  Future<Business> getBusiness() async {
    var doc = await _api.getBusinessDocument();
    business = Business.fromMap(doc.data);
    return business;
  }

  Future removeBusiness(String id) async {
    await _api.removeDocument(id);
    return;
  }

  Future updateBusiness(Business data, String id) async {
    await _api.updateDocument(data.toJson(), id);
    return;
  }

  Future addBusiness(Business data) async {
    await _api.addDocument(data.toJson());
    return;
  }
}
