import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tabitas/core/models/modifier_option.dart';
import 'package:tabitas/core/models/variant.dart';

import '../../core/models/modifier.dart';
import '../../core/models/product.dart';
import '../../locator.dart';
import '../services/api.dart';

class ProductModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Product> products;
  Product _currentConfigProduct;
  Product _currentConfigComboProduct;

  Product get currentConfigProduct {
    return _currentConfigProduct;
  }

  Product get currentConfigComboProduct {
    return _currentConfigComboProduct;
  }

  void set currentConfigProduct(Product data) {
    _currentConfigProduct = data;
  }

  void set currentConfigComboProduct(Product data) {
    _currentConfigComboProduct = data;
  }

  Future<List<Product>> fetchProducts() async {
    _api.setCollection(collection: 'products');
    var result = await _api.getDataCollection();
    products = result.documents
        .map((doc) => Product.fromMap(doc.data, doc.documentID))
        .toList();
    return products;
  }

  Stream<QuerySnapshot> fetchProductsAsStream(String categoryId) {
    _api.setCollection(collection: 'products');
    return _api.streamProductsCollection(categoryId: categoryId);
  }

  Stream<QuerySnapshot> fetchCategoriesAsStream() {
    _api.setCollection(collection: 'categories');
    return _api.streamCategoriesCollection();
  }

  Future<Product> getProductById(String id) async {
    _api.setCollection(collection: 'products');
    var doc = await _api.getDocumentById(id);
    return Product.fromMap(doc.data, doc.documentID);
  }

  Future removeProduct(String id) async {
    _api.setCollection(collection: 'products');
    await _api.removeDocument(id);
    return;
  }

  Future updateProduct(Product data, String id) async {
    _api.setCollection(collection: 'products');
    await _api.updateDocument(data.toJson(), id);
    return;
  }

  Future addProduct(Product data) async {
    await _api.addDocument(data.toJson());
    return Future.delayed(Duration.zero);
  }

  /// Inicializa las opciones seleccionadas de los modificadores de un producto
  double initProductOptions() {
    int q = 0;
    double price = 0;
    _currentConfigProduct.modifiers?.where((v) => v.active == true)?.forEach((m) {
      if (m.maxOptions == 1) {
        m.options.forEach((opt) {
          q += opt.quantity;
        });
      }
    });
    if (q == 0) {
      _currentConfigProduct.modifiers?.where((v) => v.active == true)?.forEach((m) {
        if (m.maxOptions == 1) {
          m.options.forEach((opt) {
            opt.quantity = 0;
          });
          m.options[0].quantity = 1;
          if (m.options[0].quantity > 0 && m.maxFreeOptions == 0) {
            price =
                price + m.options[0].quantity * m.options[0].price;
          }
        }
      });
    } else {
      _currentConfigProduct.modifiers.where((v) => v.active == true)?.forEach((m) {
        if (m.maxOptions == 1) {
          m.options.forEach((opt) {
            if (opt.quantity > 0 && m.maxFreeOptions == 0) {
              price = price + opt.quantity * opt.price;
            }
          });
        }
      });
    }

    return price;
  }

  /// Inicializa el producto con la variante seleccionada
  /// Actualiza precio, modificadores y la marca como seleccionada
  initVariantSelection(int value) {
    List<Variant> variants = _currentConfigProduct.variants?.where((v) => v.active == true)?.toList();
    if (variants != null && variants.isNotEmpty) {
      _currentConfigProduct.variants.map((e) => e.selected = false);
      variants[value].selected = true;
      _currentConfigProduct.price = variants[value].price;
      _currentConfigProduct.modifiers = variants[value].modifiers;
    }
  }

  /// Marca una opcion como la única seleccionada en un modificador
  setSelectedSingle({int value, int modifierIndex}) {
    Modifier modifier = _currentConfigProduct.modifiers[modifierIndex];
    modifier.options.forEach((opt) {
      opt.quantity = 0;
    });
    if (modifier.maxFreeOptions <= 1) {
      modifier.options[value].quantity = 1;
    }
  }

  /// Calcula el precio de un modificador de una sola opción
  double _calculatePriceDropdownModifier(Modifier modifier) {
    double price = 0;
    if (modifier.maxOptions == 1) {
      modifier.options.forEach((option) {
        if (option.quantity > 0 && modifier.maxFreeOptions == 0) {
          price = price + option.price * option.quantity;
        }
      });
    }
    return price;
  }

  /// Calcula el precio de un modificador de multiple opciones
  double _calculatePriceDialogModifier(Modifier modifier) {
    double price = 0;
    int quantityCount = modifier.maxFreeOptions;
    if (modifier.maxOptions > 1) {
      modifier.options.forEach((opt) {
        if (opt.quantity > 0) {
          int c = 0;
          for (var i = opt.quantity; i > 0; i--) {
            if (quantityCount - i >= 0) {
              if (quantityCount < 0) price = price + i * opt.price;
              quantityCount = quantityCount - i;
              break;
            } else {
              c++;
            }
          }
          if (c > 0) {
            quantityCount = quantityCount - c;
            if (quantityCount < 0) {
              price = price + c * opt.price;
            }
          }
        }
      });
    }

    return price;
  }

  /// Calcula el precio total de un producto simple
  double setTotalAmount({Product product, int quantity}) {
    double price = double.parse(product.price.toString());
    product.modifiers?.forEach((modifier) {
      price += _calculatePriceDropdownModifier(modifier);
      price += _calculatePriceDialogModifier(modifier);
    });

    return price * quantity;
  }

  /// Calcula el precio total de un producto combo
  /// Incluye el precio de los extras conn coste
  double setTotalComboAmount({Product product, int quantity}) {
    double price = double.parse(product.price.toString());
    product.productsSections.forEach((section) {
      if (section.selected != null) {
        section.selected.modifiers?.where((v) => v.active == true)?.forEach((modifier) {
          price += _calculatePriceDropdownModifier(modifier);
          price += _calculatePriceDialogModifier(modifier);
        });
      }
    });

    return price * quantity;
  }
}
