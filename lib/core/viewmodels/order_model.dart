import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:tabitas/core/models/modifier.dart';
import '../../core/models/variant.dart';
import '../../appConfig/config.dart';
import '../../core/models/Order_line_product.dart';
import '../../core/models/client.dart';
import '../../core/models/modifier_option.dart';
import '../../core/models/order.dart';
import '../../core/models/order_line.dart';
import '../../core/models/order_line_menu.dart';
import '../../core/models/order_line_product_option.dart';
import '../../core/models/product.dart';
import '../../core/models/product_section.dart';
import '../../core/models/stores.dart';
import '../../locator.dart';
import '../services/api.dart';

class OrderModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Order> orders;
  Order _order;

  Order get order {
    return _order;
  }

  void set order(Order data) {
    _order = data;
  }

  Future<void> setOrderStore(Store store) async {
    order.store = store;
    return await Future.delayed(Duration.zero);
  }

  Future<void> setOrderType(String type) async {
    order.type = type;
    return await Future.delayed(Duration.zero);
  }

  Future<void> updateQuantity(int index, int value) async {
    OrderLine line = order.orderLines[index];
    line.quantity = line.quantity + value;
    line.totalAmount = line.unitPrice * line.quantity;
    refreshTotalAmount();
    return await Future.delayed(Duration.zero);
  }

  Future<void> refreshTotalAmount() async {
    double total = 0;
    order.orderLines.forEach((line) {
      total += line.totalAmount;
    });
    order.totalAmount = total;
    return await Future.delayed(Duration.zero);
  }

  Future<void> removeOrderLine({int index}) async {
    order.orderLines.removeAt(index);
    refreshTotalAmount();
    return await Future.delayed(Duration.zero);
  }

  void resetOrder() {
    order = null;
  }

  Future<List<Order>> fetchUserOrders() async {
    _api.setCollection(collection: 'orders');
    var result = await _api.getDataCollection();
    orders = result.documents
        .map((doc) => Order.fromMap(doc.data, doc.documentID))
        .toList();
    return orders;
  }

  Stream<QuerySnapshot> fetchOrdersAsStream({String uid}) {
    _api.setCollection(collection: 'orders');
    return _api.streamOrdersCollection(uid: uid);
  }

  Stream<QuerySnapshot> fetchTrackingOrder({String orderId}) {
    print(orderId);
    _api.setCollection(collection: 'orders');
    return _api.streamTrackingOrderCollection(id: orderId);
  }

  Future<Order> getOrderById(String id) async {
    _api.setCollection(collection: 'orders');
    var doc = await _api.getDocumentById(id);
    return Order.fromMap(doc.data, doc.documentID);
  }

  Future removeOrder(String id) async {
    _api.setCollection(collection: 'orders');
    await _api.removeDocument(id);
    return;
  }

  Future updateOrder(Order order, String id) async {
    _api.setCollection(collection: 'orders');
    await _api.updateDocument(order.toJson(), id);
    return;
  }

  Future<void> initOrder(Client client) {
    order = new Order(
        totalAmount: 0,
        client: client,
        businessId: AppConfig.businessId,
        printed: null,
        ref: null,
        status: 'accepted');
    return Future.delayed(Duration.zero);
  }

  Future<void> cancelOrder() async {
    _order = null;
    return Future.delayed(Duration.zero);
  }

  Future addOrder() async {
    _api.setCollection(collection: 'orders');
    String id = _api.ref.document().documentID;
    order.id = id;
    order.printed = null;
    order.storeId = order.store.id;
    order.businessId = AppConfig.businessId;
    order.channel = 'app';
    //order.date = Timestamp.fromDate(DateTime.now());

    //TODO Fecha y hora de recogida/entrega pedido

    await initializeDateFormatting('es_ES', null).then((_) {
      order.dateLabel =
          new DateFormat("d'/'M 'a las' HH:mm").format(new DateTime.now());
      order.date = new Timestamp.fromDate(DateTime.now());
    });

    return await _api.addDocumentWithId(order.toJson(), id);
  }

  Future<void> addOrderLine({dynamic line}) async {
    order.orderLines.add(line);
    double total = 0;
    order.orderLines.forEach((element) {
      OrderLine line = element;
      total += line.totalAmount;
    });
    order.totalAmount = total;
    return await Future.delayed(Duration.zero);
  }

  Future<void> addProductOrderLine({OrderLine productOrderLine}) async {
    if (order.orderLines == null) order.orderLines = [];
    order.orderLines.add(productOrderLine);
    double total = 0;
    order.orderLines.forEach((line) {
      total += line.totalAmount;
    });
    order.totalAmount = total;
    return await Future.delayed(Duration.zero);
  }

  String _setProductLabelFromVariantSelected({Product product}) {
    if (product.type == 'simple') {
      if (product.variants != null && product.variants.isNotEmpty) {
        Variant selectedVariant =
            product.variants?.firstWhere((variant) => variant.selected == true);
        return '${product.name} ${selectedVariant.shortName}';
      } else {
        return product.name;
      }
    } else {
      return product.name;
    }
  }

  Future<void> generateOrder(
      {double totalAmount,
      int quantity,
      Product product,
      String comment}) async {
    OrderLine orderLine = _createProductOrderLine(
        totalAmount: totalAmount,
        quantity: quantity,
        product: product,
        comment: comment);

    await addProductOrderLine(productOrderLine: orderLine).then((_) {});
  }

  OrderLine _createProductOrderLine(
      {double totalAmount, int quantity, Product product, String comment}) {
    OrderLine orderLine = new OrderLine(
      totalAmount: totalAmount,
      unitPrice: totalAmount / quantity,
      quantity: quantity,
      tax: double.parse(product.tax.toString()),
      comment: comment,
      label: _setProductLabelFromVariantSelected(product: product),
      type: product.type == 'simple' ? 'PRODUCT_SIMPLE' : 'PRODUCT_COMBO',
      product: product.type == 'simple'
          ? new OrderLineProduct(
              id: product.id, sinqroId: 0 // TODO Sinqro integration
              )
          : null,
      menu: product.type == 'combo'
          ? new OrderLineMenu(
              id: product.id, sinqroId: 0 // TODO Sinqro integration
              )
          : null,
      childrenOrderLines: product.type == 'simple'
          ? _createModifierOrderLines(product: product)
          : _createProductComboOrderLines(
              productSection: product.productsSections),
    );

    return orderLine;
  }

  OrderLine _createModifierOrderLine({Modifier modifier, Product product}) {
    OrderLine orderLine = new OrderLine(
      type: 'PRODUCT_MODIFIER',
      label: modifier.shortName,
      totalAmount: 0,
      quantity: 1,
      unitPrice: 0,
      childrenOrderLines:
          _createOptionsOrderLines(product: product, modifier_id: modifier.id),
    );
    return orderLine;
  }

  List<OrderLine> _createModifierOrderLines({Product product}) {
    List<OrderLine> modifierOrderLines = [];
    product.modifiers?.forEach((modifier) {
      OrderLine ol =
      _createModifierOrderLine(modifier: modifier, product: product);
      modifierOrderLines.add(ol);
    });

    return modifierOrderLines;
  }

  List<OrderLine> _createProductComboOrderLines(
      {List<ProductSection> productSection}) {
    List<OrderLine> productOrderLines = [];
    productSection.forEach((section) {
      OrderLine ol = _createProductOrderLine(
          comment: '', product: section.selected, quantity: 1, totalAmount: 0);
      double totalAmount = 0;
      ol.childrenOrderLines?.forEach((orderOptionline) {
        totalAmount += orderOptionline.totalAmount;
      });
      ol.totalAmount = totalAmount;
      productOrderLines.add(ol);
    });
    return productOrderLines;
  }

  List<OrderLine> _createOptionsOrderLines(
      {Product product, String modifier_id}) {
    print('create options order lines');
    List<OrderLine> optionsOrderLines = [];

    product.modifiers?.where((v) => v.active == true)?.forEach((modifier) {
      //Single selector
      if (modifier.maxOptions == 1) {
        modifier.options.forEach((option) {
          if (option.active == true) {
            if (option.quantity > 0) {
              if (modifier.showSelectedOptions == true) {
                if (modifier_id == modifier.id) {
                  optionsOrderLines.add(_createProductOptionOrderLine(
                      option: option,
                      unSelected: modifier.showUnselectedOptions,
                      selected: modifier.showSelectedOptions,
                      price: modifier.maxFreeOptions == 0 ? option.price : 0));
                }
              }
            } else {
              if (modifier.showUnselectedOptions == true) {
                if (modifier_id == modifier.id) {
                  optionsOrderLines.add(_createProductOptionOrderLine(
                      option: option,
                      unSelected: modifier.showUnselectedOptions,
                      selected: modifier.showSelectedOptions,
                      price: modifier.maxFreeOptions == 0 ? option.price : 0));
                }
              }
            }
          }
        });
      }

      //Multiple selector
      int quantityCount = modifier.maxFreeOptions;
      if (modifier.maxOptions > 1) {
        if (modifier.showSelectedOptions == true) {
          List<ModifierOption> options = modifier.options
              .where((element) => element.quantity > 0)
              .toList();
          options.forEach((option) {
            if (option.active == true) {
              if (modifier_id == modifier.id) {
                int c = 0;
                double price = 0;
                for (var i = option.quantity; i > 0; i--) {
                  if (quantityCount - i >= 0) {
                    if (quantityCount < 0) {
                      price = price + i * option.price;
                    } else {
                      price = price + 0;
                    }
                    quantityCount = quantityCount - i;
                    break;
                  } else {
                    c++;
                  }
                }
                if (c > 0) {
                  quantityCount = quantityCount - c;
                  if (quantityCount < 0) {
                    price = price + c * option.price;
                  }
                }

                optionsOrderLines.add(_createProductOptionOrderLine(
                    option: option,
                    selected: modifier.showSelectedOptions,
                    unSelected: modifier.showUnselectedOptions,
                    price: price));
              }
            }
          });
        }

        if (modifier.showUnselectedOptions == true) {
          List<ModifierOption> options = modifier.options
              .where((element) => element.quantity == 0)
              .toList();
          options.forEach((option) {
            if (option.active == true) {
              if (modifier_id == modifier.id) {
                optionsOrderLines.add(_createProductOptionOrderLine(
                    option: option,
                    selected: modifier.showSelectedOptions,
                    unSelected: modifier.showUnselectedOptions,
                    price: 0));
              }
            }
          });
        }
      }
    });
    return optionsOrderLines.isNotEmpty ? optionsOrderLines : null;
  }

  OrderLine _createProductOptionOrderLine(
      {ModifierOption option, bool unSelected, bool selected, double price}) {
    OrderLine orderLine = new OrderLine(
      quantity: option.quantity,
      totalAmount: price,
      label: option.name,
      modifierShowSelectedOptions: selected,
      modifierShowUnselectedOptions: unSelected,
      productOption: new OrderLineProductOption(sinqroId: 0, id: option.id),
      unitPrice: option.price,
      type: 'PRODUCT_OPTION',
    );

    return orderLine;
  }

  /// Devuelve el número de conceptos en el pedido
  int counterOrderLines() {
    if (order != null) {
      if (order.orderLines == null) return 0;
      return order.orderLines.length;
    } else {
      return 0;
    }
  }
}
