import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../locator.dart';
import '../models/stores.dart';
import '../services/api.dart';

class StoreModel extends ChangeNotifier {
  Api _api = locator<Api>();

  List<Store> stores;

  Store selectedStore;

  StoreModel() {
    _api.setCollection(collection: 'stores');
  }

  Future<List<Store>> fetchStores() async {
    _api.setCollection(collection: 'stores');
    var result = await _api.getDataCollection();
    stores = result.documents
        .map((doc) => Store.fromMap(doc.data, doc.documentID))
        .toList();
    return stores;
  }

  Stream<QuerySnapshot> fetchStoresAsStream() {
    _api.setCollection(collection: 'stores');
    return _api.streamDataCollection();
  }

  Future<Store> getStoreById(String id) async {
    var doc = await _api.getDocumentById(id);
    return Store.fromMap(doc.data, doc.documentID);
  }

  Future removeStore(String id) async {
    await _api.removeDocument(id);
    return;
  }

  Future updateStore(Store data, String id) async {
    await _api.updateDocument(data.toJson(), id);
    return;
  }

  Future addStore(Store data) async {
    await _api.addDocument(data.toJson());
    return;
  }

  int getNumberAvailableStores() {
    List<Store> _stores =
        stores.where((element) => element.active == true).toList();
    return _stores.length;
  }

  Store getFirstAvailableStore() {
    List<Store> _stores =
        stores.where((element) => element.active == true).toList();
    return _stores.first;
  }

  saveStore(String storeId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('storeId', storeId);
  }

  Future<String> readSavedStore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String storeId = (prefs.getString('storeId') ?? null);
    print('store saved: $storeId');
    return storeId;
  }
}
