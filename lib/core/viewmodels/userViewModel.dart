import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import '../../appConfig/appconfig.dart';
import '../../core/models/address.dart';
import '../../core/models/client.dart';
import '../../locator.dart';
import '../services/api.dart';
import 'package:cloud_functions/cloud_functions.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class UserViewModel with ChangeNotifier {
  Api _api = locator<Api>();

  FirebaseAuth _auth;
  FirebaseUser _user;
  Client _userProfile;
  Position _position;

  CollectionReference ref;
  Status _status = Status.Uninitialized;

  UserViewModel.instance() : _auth = FirebaseAuth.instance {
    _auth.onAuthStateChanged.listen(_onAuthStateChanged);
  }

  void set position(Position position) {
    _position = position;
  }

  Position get position => _position;

  Status get status => _status;

  FirebaseUser get user => _user;

  Client get userProfile => _userProfile;

  Future<bool> signIn(String email, String password) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      var data = await _auth.signInWithEmailAndPassword(email: email, password: password);
      bool isClient = true;
      await getClientProfileById(data.user.uid).then((DocumentSnapshot value) {
        if (!value.exists) {
          signOut();
          isClient = false;
        } else {
          isClient = true;
        }
      });
      return isClient;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future<FirebaseUser> register(String _email, String _password) async {
    return await _auth
        .createUserWithEmailAndPassword(email: _email, password: _password)
        .then((AuthResult value) {
      return value.user;
    });
  }

  Future<void> createUserProfile(String uid, Map<String, dynamic> data) async {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients');

    await ref.document(uid).setData(data);
    _userProfile = Client.fromMap(data, uid);
  }

  Future signOut() async {
    _auth.signOut();
    _userProfile = null;
    _user = null;
    _status = Status.Unauthenticated;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  Future<void> sendPasswordResetEmail(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  Future<void> _onAuthStateChanged(FirebaseUser firebaseUser) async {
    if (firebaseUser == null) {
      _status = Status.Unauthenticated;
      notifyListeners();
    } else {
      getClientProfileById(firebaseUser.uid).then((DocumentSnapshot value) {
        if (!value.exists) {
          signOut();
          return;
        }

        _user = firebaseUser;
        _userProfile = Client.fromMap(value.data, value.documentID);
        _status = Status.Authenticated;
        notifyListeners();
      });
    }
  }

  Future<DocumentSnapshot> getClientProfileById(String id) async {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients');
    return await ref.document(id).get();
  }

  Stream<QuerySnapshot> fetchOrdersAsStream(String uid) {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(uid)
        .collection('orders');
    return ref.orderBy('date').snapshots();
  }

  Stream<QuerySnapshot> fetchAddresses() {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(_user.uid)
        .collection('addresses');
    return ref.snapshots();
  }

  Address addAddress(PickResult result) {
    Map<String, dynamic> data = {
      'formatted_address': result.formattedAddress,
      'location': {
        'lat': result.geometry.location.lat,
        'lng': result.geometry.location.lng,
        'zoom': 0
      }
    };
    result.addressComponents.forEach((component) {
      switch (component.types[0]) {
        case 'postal_code':
          data['postal_code'] = component.longName;
          break;
        case 'route':
          data['address_line_1'] = component.longName;
          break;
        case 'street_number':
          data['address_line_2'] = component.longName;
          break;
        case 'locality':
          data['locality'] = component.longName;
          break;
        case 'administrative_area_level_2':
          data['state'] = component.longName;
          break;
        case 'country':
          data['country'] = component.shortName;
          break;
      }
    });

    Address address = Address.fromMap(data, null);
    return address;
  }

  Future<void> deleteAddress(Address address) async {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(user.uid)
        .collection('addresses');

    await ref.document(address.id).delete();
  }

  Future<void> saveAddress(Address address) async {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients')
        .document(user.uid)
        .collection('addresses');

    String id = ref.document().documentID;
    await ref.document(id).setData(address.toJson());
    return Future.delayed(Duration.zero);
  }

  saveClientProfile(Map<String, dynamic> data) async {
    ref = Firestore.instance
        .collection('business')
        .document(AppConfig.businessId)
        .collection('clients');
    await ref.document(user.uid).updateData({
      'first_name':
          data['first_name'] == '' ? FieldValue.delete() : data['first_name'],
      'last_name':
          data['last_name'] == '' ? FieldValue.delete() : data['last_name'],
      'birthdate':
          data['birthdate'] == '' ? FieldValue.delete() : data['birthdate']
    });
    _userProfile.firstName = data['first_name'];
    _userProfile.lastName = data['last_name'];
    _userProfile.birthdate = data['birthdate'];
    notifyListeners();

  }

  Stream<QuerySnapshot> fetchStripeCardsAsStream() {
    _api.setCollection(collection: 'clients');
    return _api.streamStripeCardsCollection(user.uid);
  }
}
