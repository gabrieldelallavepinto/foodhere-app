import 'package:cloud_firestore/cloud_firestore.dart';
import '../../core/models/address.dart';
import '../../core/models/client.dart';
import '../../core/models/order_line.dart';
import '../../core/models/stores.dart';

class Order {
  String id;
  String ref;
  bool printed;
  dynamic sinqro_id;
  dynamic totalAmount;
  String dateLabel;
  String comment;
  String cancellationComment; // Sólo si es cancelado
  Client client;
  String status;
  Timestamp date;
  String type; //delivery, collection, insitu, reservation
  Store store;
  List<dynamic> payments;
  List<OrderLine> orderLines;
  dynamic deliveryAmount;
  Address deliveryAddress;
  List<dynamic> deliveries;
  dynamic dinersNumber;
  bool isCancellable;
  String posOrderId;
  String marketOrderId;
  String marketOrderCode;
  String businessId;
  String storeId;
  String channel;

  Order({
    this.id,
    this.sinqro_id,
    this.ref,
    this.printed,
    this.totalAmount,
    this.dateLabel,
    this.date,
    this.comment,
    this.cancellationComment,
    this.client,
    this.status,
    this.type,
    this.store,
    this.payments,
    this.orderLines,
    this.deliveryAmount,
    this.deliveryAddress,
    this.deliveries,
    this.dinersNumber,
    this.isCancellable,
    this.posOrderId,
    this.marketOrderId,
    this.marketOrderCode,
    this.businessId,
    this.storeId,
    this.channel,
  });

  Order.fromMap(Map snapshot, String id)
      : id = id ?? '',
        sinqro_id = snapshot['sinqro_id'],
        ref = snapshot['ref'] ?? null,
        printed = snapshot['printed'] ?? false,
        businessId = snapshot['business_id'] ?? null,
        storeId = snapshot['store_id'] ?? null,
        totalAmount = snapshot['total_amount'],
        dateLabel = snapshot['date_label'],
        date = snapshot['date'],
        channel = snapshot['channel'],
        comment = snapshot['comment'],
        cancellationComment = snapshot['cancellation_comment'],
        client = Client.fromMap(snapshot['client'], snapshot['client']['id']),
        status = snapshot['status'],
        type = snapshot['type'],
        orderLines = _orderLines(snapshot['order_lines']),
        store = Store.fromMap(snapshot['store'], snapshot['store']['id']),
        payments = snapshot['payments'] ?? [],
        deliveryAmount = snapshot['delivery_amount'],
        deliveryAddress = snapshot['delivery_address'] == null
            ? null
            : Address.fromMap(snapshot['delivery_address'],
                snapshot['delivery_address']['id']),
        deliveries = snapshot['deliveries'],
        dinersNumber = snapshot['diners_number'],
        isCancellable = snapshot['is_cancellable'],
        posOrderId = snapshot['pos_order_id'],
        marketOrderId = snapshot['market_order_id'],
        marketOrderCode = snapshot['market_order_code'];

  toJson() {
    return {
      'order_lines': orderLines != null
          ? orderLines.map((e) => e.toJson()).toList()
          : null,
      'id': id,
      'id_sinqro': sinqro_id,
      'ref': ref,
      'total_amount': totalAmount,
      'date_label': dateLabel,
      'date': date,
      'channel': channel,
      'comment': comment,
      'cancellation_comment': cancellationComment,
      'client': client.toJson(),
      'status': status,
      'type': type,
      'payments': payments,
      'delivery_amount': deliveryAmount,
      'delivery_address':
          deliveryAddress != null ? deliveryAddress.toJson() : null,
      'deliveries': deliveries,
      'diners_number': dinersNumber,
      'is_cancellable': isCancellable,
      'pos_order_id': posOrderId,
      'market_order_id': marketOrderId,
      'market_order_code': marketOrderCode,
      'printed': printed,
      'business_id': businessId,
      'store_id': storeId,
      'store': store.toJson(),
    };
  }

  static List<OrderLine> _orderLines(List<dynamic> orderLines) {
    if (orderLines == null) {
      return null;
    } else {
      return orderLines.map((e) => OrderLine.fromMap(e)).toList();
    }
  }
}
