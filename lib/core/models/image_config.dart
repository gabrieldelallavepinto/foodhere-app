class ImageConfig {
  String id;
  String label;
  String imageUrl;

  ImageConfig({
    this.id,
    this.label,
    this.imageUrl,
  });

  ImageConfig.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        label = snapshot['label'] ?? null,
        imageUrl = snapshot['image_url'] ?? null;

  toJson() {
    return {"id": id, "label": label, "image_url": imageUrl};
  }
}
