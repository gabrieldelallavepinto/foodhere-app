import '../../core/models/modifier_option.dart';

class Modifier {
  bool active;
  String businessId;
  String categoryId;
  String description;
  String id;
  String label;
  int maxFreeOptions;
  int maxOptions;
  String name;
  String shortName;
  bool showSelectedOptions;
  bool showUnselectedOptions;
  List<ModifierOption> options;
  int sortOrder;
  bool noApplyRecipe;
  List<ModifierOption> recipeOptions;

  Modifier(
      {this.active,
      this.businessId,
      this.categoryId,
      this.description,
      this.id,
      this.label,
      this.maxFreeOptions,
      this.maxOptions,
      this.name,
      this.shortName,
      this.options,
      this.showSelectedOptions,
      this.showUnselectedOptions,
      this.sortOrder,
      this.noApplyRecipe});

  Modifier.fromMap(Map snapshot)
      : active = snapshot['active'] ?? null,
        businessId = snapshot['businesss_id'] ?? null,
        categoryId = snapshot['category_id'] ?? null,
        description = snapshot['description'] ?? null,
        id = snapshot['id'] ?? null,
        label = snapshot['label'] ?? null,
        maxFreeOptions =
            int.parse(snapshot['max_free_options'].toString()) ?? null,
        maxOptions = int.parse(snapshot['max_options'].toString()) ?? null,
        name = snapshot['name'] ?? null,
        shortName = snapshot['short_name'] ?? null,
        showSelectedOptions = snapshot['show_selected_options'] ?? null,
        showUnselectedOptions = snapshot['show_unselected_options'] ?? null,
        options = _mappingOptions(snapshot['options']),
        recipeOptions = _mappingOptions(snapshot['recipe_options']),
        noApplyRecipe = snapshot['no_apply_recipe'] ?? false,

        sortOrder = int.parse(snapshot['sort_order'].toString()) ?? null;

  toJson() {
    return {
      'active': active,
      'business_id': businessId,
      'category_id': categoryId,
      'description': description,
      'id': id,
      'label': label,
      'max_free_options': maxFreeOptions,
      'max_options': maxOptions,
      'name': name,
      'short_name': shortName,
      'show_selected_options': showSelectedOptions,
      'show_unselected_options': showUnselectedOptions,
      'no_apply_recipe': noApplyRecipe,
      'options':
          options != null ? options.map((e) => e.toJson()).toList() : null,
      'sort_order': sortOrder,
      'recipe_options': recipeOptions
    };
  }

  static _mappingOptions(List<dynamic> options) {
    if (options == null) {
      return null;
    } else {
      return options.map((e) => ModifierOption.fromMap(e)).toList();
    }
  }
}
