import 'package:tabitas/core/models/product.dart';
import 'package:tabitas/core/models/product_item.dart';

class ProductSection {
  String name;
  String description;
  Product selected;
  List<ProductItem> products;

  ProductSection.fromMap(Map snapshot)
      : name = snapshot['name'] ?? null,
        description = snapshot['description'] ?? null,
        selected = snapshot['selected'] ?? null,
        products =  _mappingProducts(snapshot['products']);

  toJson() {
    return {
      'name': name,
      'description' : description,
      'selected' : selected,
      'products' : products != null
          ? products.map((e) => e.toJson()).toList()
          : null,
    };
  }

  static List<ProductItem> _mappingProducts(List<dynamic> products) {
    if (products == null) {
      return null;
    } else {
      return products.map((e) => ProductItem.fromMap(e)).toList();
    }
  }
}
