class ModifierOption {
  bool active;
  String icon;
  String id;
  bool isAvailable;
  int maxQuantity;
  int minQuantity;
  String name;
  double price;
  int quantity;
  String shortName;

  ModifierOption.fromMap(Map snapshot)
      : active = snapshot['active'] ?? null,
        id = snapshot['id'] ?? null,
        icon = snapshot['icon'] ?? null,
        isAvailable = snapshot['is_available'] ?? null,
        maxQuantity = int.parse(snapshot['max_quantity'].toString()) ?? null,
        minQuantity = int.parse(snapshot['min_quantity'].toString()) ?? null,
        name = snapshot['name'] ?? null,
        price = double.parse(snapshot['price'].toString()) ?? null,
        quantity = int.parse(snapshot['quantity'].toString()) ?? null,
        shortName = snapshot['short_name'] ?? null;

  toJson() {
    return {
      'active' : active,
      'id' : id,
      'icon' : icon,
      'is_available' : isAvailable,
      'max_quantity' : maxQuantity,
      'min_quantity' : minQuantity,
      'name' : name,
      'price' : price,
      'quantity' : quantity,
      'short_name' : shortName
    };
  }
}
