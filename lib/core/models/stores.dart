import 'package:tabitas/core/models/order_type.dart';
import 'address.dart';

class Store {

  bool active;
  Address address;
  String businessId;
  String description;
  String email;
  String id;
  List<dynamic> images;
  String name;
  OrderTypes orderTypes;
  String phone;
  String optionalPhone;
  String privateEmail;
  String privatePhone;
  Map<String, dynamic> schedule;
  String scheduleLabel;
  String website;
  String status;
  String type;
  double distance;

  Store({
    this.active,
    this.address,
    this.businessId,
    this.email,
    this.id,
    this.images,
    this.name,
    this.optionalPhone,
    this.schedule,
    this.orderTypes,
    this.phone,
    this.scheduleLabel,
    this.website,
    this.description,
    this.type,
    this.status,
    this.privateEmail,
    this.privatePhone,
    this.distance,

  });

  Store.fromMap(Map snapshot, String id)
      :
        id = id ?? '',
        active = snapshot['active'] ?? false,
        status = snapshot['status'] ?? 'closed',
        name = snapshot['name'] ?? '',
        address = Address.fromMap(snapshot['address'], null ) ?? null,
        businessId = snapshot['business_id'] ?? '',
        description = snapshot['description'] ?? null,
        email = snapshot['email'] ?? '',
        images = snapshot['images'] ?? '',
        optionalPhone = snapshot['optional_phone'] ?? '',
        schedule = snapshot['schedule'] ?? null,
        orderTypes = OrderTypes.fromMap(snapshot['order_types']) ?? null,
        phone = snapshot['phone'] ?? '',
        scheduleLabel = snapshot['schedule_label'] ?? '',
        website = snapshot['website'] ?? ''
  ;

  toJson() {
    return {
      "active": active,
      "address": address.toJson(),
      "business_id": businessId,
      "email": email,
      "id": id,
      "images" : images,
      "name": name,
      "optional_phone" : optionalPhone,
      "order_schedule" : schedule,
      "order_types": orderTypes.toJson(),
      "phone": phone,
      "schedule_label": scheduleLabel,
      "website": website,
      "distance" : distance
    };
  }

}

enum StatusEnum {
  enabled,
  disabled,
}