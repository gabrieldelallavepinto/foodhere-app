import 'package:tabitas/core/models/image_config.dart';

class Category {
  bool active;
  bool allowedDelete;
  List<dynamic> availableStores;
  String businessId;
  String description;
  String id;
  List<ImageConfig> images;
  String name;
  String shortName;
  int sortOrder;
  String type; // products, promotions

  Category({
    this.active,
    this.allowedDelete,
    this.availableStores,
    this.businessId,
    this.description,
    this.id,
    this.images,
    this.name,
    this.shortName,
    this.sortOrder,
    this.type,
  });

  Category.fromMap(Map snapshot)
      : active = snapshot['active'] ?? null,
        allowedDelete = snapshot['allowed_delete'] ?? null,
        availableStores = snapshot['available_stores'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        description = snapshot['description'] ?? null,
        id = snapshot['id'] ?? null,
        images = _mappingImages(snapshot['images']),
        name = snapshot['name'] ?? null,
        shortName = snapshot['short_name'] ?? null,
        sortOrder = snapshot['sort_order'] ?? null,
        type = snapshot['type'] ?? null;

  toJson() {
    return {
      "active": active,
      "allowed_delete": allowedDelete,
      "available_stores": availableStores,
      "business_id": businessId,
      "description": description,
      "id": id,
      "images": images != null ? images.map((e) => e.toJson()).toList() : null,
      "name": name,
      "short_name": shortName,
      "sort_order": sortOrder,
      "type": type
    };
  }

  static List<ImageConfig> _mappingImages(List<dynamic> images) {
    if (images == null) {
      return null;
    } else {
      return images.map((e) => ImageConfig.fromMap(e)).toList();
    }
  }
}
