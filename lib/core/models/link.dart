class Link {
  String id;
  String url;

  Link({this.id, this.url});

  Link.fromMap(Map snapshot):
        id = snapshot['id'] ?? null,
        url = snapshot['url'] ?? null;

  toJson() {
    return {
      'id' : id,
      'url': url
    };
  }
}

class DataLinkConfig {
  String title;
  String url;
  bool zoom;

  DataLinkConfig(
      {this.title, this.url, this.zoom});
}