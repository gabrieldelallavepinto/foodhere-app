class PaymentMethods {
  PaypalPaymentMethod paypal;
  StripePaymentMethod stripe;
  DataphonePaymentMethod dataphone;
  CashPaymentMethod cash;

  PaymentMethods({this.paypal, this.stripe, this.dataphone, this.cash});

  PaymentMethods.fromMap(Map snapshots)
      : paypal = PaypalPaymentMethod.fromMap(snapshots['paypal']) ?? null,
        stripe = StripePaymentMethod.fromMap(snapshots['stripe']) ?? null,
        cash = CashPaymentMethod.fromMap(snapshots['cash']) ?? null,
        dataphone =
            DataphonePaymentMethod.fromMap(snapshots['dataphone']) ?? null;

  toJson() {
    return {
      "paypal": paypal.toJson(),
      "stripe": stripe.toJson(),
      "dataphone": dataphone.toJson(),
      "cash": cash.toJson()
    };
  }
}

class DataphonePaymentMethod {
  String id;
  bool enabled;
  String description;
  String label;

  DataphonePaymentMethod({this.id, this.enabled, this.label, this.description});

  DataphonePaymentMethod.fromMap(Map snapshots)
      : id = snapshots['id'] ?? null,
        enabled = snapshots['enabled'] ?? false,
        label = snapshots['label'] ?? null,
        description = snapshots['description'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "label": label,
      "description": description,
    };
  }
}

class CashPaymentMethod {
  String id;
  bool enabled;
  String description;
  String label;

  CashPaymentMethod({this.id, this.enabled, this.label, this.description});

  CashPaymentMethod.fromMap(Map snapshots)
      : id = snapshots['id'] ?? null,
        enabled = snapshots['enabled'] ?? false,
        label = snapshots['label'] ?? null,
        description = snapshots['description'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "label": label,
      "description": description,
    };
  }
}

class StripePaymentMethod {
  String id;
  bool enabled;
  String description;
  String label;
  String clientId;

  StripePaymentMethod(
      {this.id, this.enabled, this.label, this.description, this.clientId});

  StripePaymentMethod.fromMap(Map snapshots)
      : id = snapshots['id'] ?? null,
        enabled = snapshots['enabled'] ?? false,
        label = snapshots['label'] ?? null,
        clientId = snapshots['client_id'] ?? null,
        description = snapshots['description'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "label": label,
      "description": description,
      "client_id": clientId
    };
  }
}

class PaypalPaymentMethod {
  String id;
  bool enabled;
  String description;
  String label;

  PaypalPaymentMethod({this.id, this.enabled, this.label, this.description});

  PaypalPaymentMethod.fromMap(Map snapshots)
      : id = snapshots['id'] ?? null,
        enabled = snapshots['enabled'] ?? false,
        label = snapshots['label'] ?? null,
        description = snapshots['description'] ?? null;

  toJson() {
    return {
      "id": id,
      "enabled": enabled,
      "label": label,
      "description": description,
    };
  }
}
