class StripeCard {
  String id;
  Map<String, dynamic> card;
  dynamic created;
  String type;
  bool livemode;
  bool selected;

  StripeCard(
      {this.id,
      this.card,
      this.created,
      this.type,
        this.livemode,
      this.selected});

  StripeCard.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        card = snapshot['card'] ?? null,
        created = snapshot['created'] ?? null,
        type = snapshot['type'] ?? null,
        selected = snapshot['selected'] ?? false,
        livemode = snapshot['livemode'] ?? false;

  toJson() {
    return {
      "id" : id,
      "created" : created,
      "type" : type,
      "card" : card,
      "selected" : selected,
      "livemode" : livemode
    };
  }
}
