class OrderStatus {
  String code;
  String name;
  String color;
  bool isFinalStatus;
  String globalStatusCode;

  OrderStatus({this.code, this.name, this.color, this.isFinalStatus,
    this.globalStatusCode});

  OrderStatus.fromMap( Map snapshot) :
        code = snapshot['code'] ?? 'waiting_sp_validation',
        name = snapshot['name'] ?? '',
        color = snapshot['color'] ?? '',
        isFinalStatus = snapshot['isFinalStatusCode'] ?? '',
        globalStatusCode = snapshot['globalStatusCode'] ?? '';

  toJson() {
    return {
      'code' : code,
      'name' : name,
      'color' : color,
      'isFinalStatatus': isFinalStatus,
      'globalStatusCode' : globalStatusCode
    };
  }

}