import '../../../core/models/sinqro/sinqro_order_status.dart';

class SinqroOrder {
  String id;
  dynamic totalAmount;
  String dateLabel;
  String date;
  String comment;
  String cancellationComment; // Sólo si es cancelado
  Map<String, dynamic> consumer;
  OrderStatus status;
  String type; //delivery, collection, insitu, reservation
  Map<String, dynamic> sellingPoint;
  List<dynamic> payments;
  List<dynamic> orderLines;
  dynamic deliveryAmount;
  Map<String, dynamic> deliveryAddress;
  List<dynamic> deliveries;
  dynamic dinersNumber;
  bool isCancellable;
  String posOrderId;
  String marketOrderId;
  String marketOrderCode;

  SinqroOrder(
      {this.id,
      this.totalAmount,
      this.dateLabel,
      this.date,
      this.comment,
      this.cancellationComment,
      this.consumer,
      this.status,
      this.type,
      this.sellingPoint,
      this.payments,
      this.orderLines,
      this.deliveryAmount,
      this.deliveryAddress,
      this.deliveries,
      this.dinersNumber,
      this.isCancellable,
      this.posOrderId,
      this.marketOrderId,
      this.marketOrderCode});

  SinqroOrder.fromMap(Map snapshot, String id)
      : id = id ?? '',
        totalAmount = snapshot['total_amount'],
        dateLabel = snapshot['date_label'],
        date = snapshot['date'],
        comment = snapshot['comment'],
        cancellationComment = snapshot['cancellation_comment'],
        consumer = snapshot['consumer'],
        status = snapshot['status'],
        type = snapshot['type'],
        sellingPoint = snapshot['selling_pooint'],
        payments = snapshot['payments'],
        orderLines = snapshot['order_lines'],
        deliveryAmount = snapshot['delivery_amount'],
        deliveryAddress = snapshot['delivery_address'],
        deliveries = snapshot['deliveries'],
        dinersNumber = snapshot['diners_number'],
        isCancellable = snapshot['is_cancellable'],
        posOrderId = snapshot['posOrderId'],
        marketOrderId = snapshot['market_order_id'],
        marketOrderCode = snapshot['market_order_code'];

  toJson() {
    return {
      'id': id,
      'totalAmount': totalAmount,
      'dateLabel': dateLabel,
      'date': date,
      'comment': comment,
      'cancellationComment': cancellationComment,
      'consumer': consumer,
      'status': status,
      'type': type,
      'sellingPoint': sellingPoint,
      'payments': payments,
      'orderLines': orderLines,
      'deliveryAmount': deliveryAmount,
      'deliveryAddress': deliveryAddress,
      'deliveries': deliveries,
      'dinersNumber': dinersNumber,
      'isCancellable': isCancellable,
      'posOrderId': posOrderId,
      'marketOrderId': marketOrderId,
      'marketOrderCode': marketOrderCode
    };
  }
}
