import 'package:flutter_inner_drawer/inner_drawer.dart';

class SettingHome {
  bool darkMode;
  String backgroundType;
  double logoWidth;
  SettingHomeSpinner spinner;
  SettingHomeDrawer drawer;

  SettingHome(
      {this.darkMode,
      this.backgroundType,
      this.logoWidth,
      this.spinner,
      this.drawer});
}

class SettingHomeSpinner {
  String type;
  double size;

  SettingHomeSpinner({this.type, this.size});
}

class SettingHomeDrawer {
  double drawerBorderRadius;
  InnerDrawerAnimation drawerLeftAnimationType;
  InnerDrawerAnimation drawerRightAnimationType;
  IDOffset drawerOffset;
  IDOffset drawerScale;

  SettingHomeDrawer(
      {this.drawerBorderRadius,
      this.drawerLeftAnimationType,
      this.drawerOffset,
      this.drawerRightAnimationType,
      this.drawerScale});
}
