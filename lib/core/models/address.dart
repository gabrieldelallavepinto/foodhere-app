class Address {
  String id;
  String formattedAddress;
  String addressLine1;
  String addressLine2;
  String country;
  String state;
  String locality;
  String postalCode;
  Geolocation location;
  String note;
  bool selected;

  Address(
      {this.id,
      this.addressLine1,
      this.addressLine2,
      this.formattedAddress,
      this.country,
      this.state,
      this.locality,
      this.postalCode,
      this.location,
      this.selected,
      this.note});

  Address.fromMap(Map snapshots, String id)
      : id = id ?? null,
        addressLine1 = snapshots['address_line_1'] ?? null,
        addressLine2 = snapshots['address_line_2'] ?? null,
        formattedAddress = snapshots['formatted_address'] ?? null,
        country = snapshots['country'] ?? null,
        state = snapshots['state'] ?? null,
        locality = snapshots['locality'] ?? null,
        postalCode = snapshots['postal_code'] ?? null,
        note = snapshots['note'] ?? null,
        selected = snapshots['selected'] ?? null,
        location = Geolocation.fromMap(snapshots['location'] ?? null);

  toJson() {
    return {
      'id': id,
      'address_line_1': addressLine1,
      'address_line_2': addressLine2,
      'formatted_address': formattedAddress,
      'country': country,
      'state': state,
      'locality': locality,
      'postal_code': postalCode,
      'note': note,
      'selected': selected,
      'location': location.toJson()
    };
  }
}

class Geolocation {
  dynamic lat;
  dynamic lng;
  dynamic zoom;

  Geolocation({this.lat, this.lng, this.zoom});

  Geolocation.fromMap(Map snapshots)
      : lat = snapshots['lat'] ?? 0.0,
        lng = snapshots['lng'] ?? 0.0,
        zoom = snapshots['zoom'] ?? 0.0;

  toJson() {
    return {'lat': lat, 'lng': lng, 'zoom': zoom};
  }
}
