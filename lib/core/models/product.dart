import 'package:cloud_firestore/cloud_firestore.dart';
import '../../core/models/allergen.dart';
import '../../core/models/image_config.dart';
import '../../core/models/modifier.dart';
import '../../core/models/variant.dart';

import '../../core/models/product_section.dart';
import '../../core/models/category.dart';

class Product {
  List<Allergen> allergens;
  String id;
  bool allowObservations;
  List<String> availableStores;
  String barcode;
  String businessId;
  String categoryId;
  Timestamp createAt;
  Timestamp deleteAt;
  String description;
  List<ImageConfig> images;
  bool isAvailable;
  bool isFeatured;
  bool isNew;
  bool isPublic;
  List<Modifier> modifiers;
  List<Modifier> originalModifiers;
  String name;
  double price;
  String qrCode;
  String ref;
  String shortDescription;
  String shortName;
  int sortOrder;
  List<String> tags;
  double tax;
  String type;
  Timestamp updateAt;
  List<Variant> variants;
  bool useVariants;
  List<ProductSection> productsSections;

  Product(
      this.id,
      this.allergens,
      this.allowObservations,
      this.availableStores,
      this.barcode,
      this.businessId,
      this.categoryId,
      this.createAt,
      this.deleteAt,
      this.description,
      this.images,
      this.isAvailable,
      this.isFeatured,
      this.isNew,
      this.isPublic,
      this.modifiers,
      this.originalModifiers,
      this.name,
      this.price,
      this.qrCode,
      this.ref,
      this.shortDescription,
      this.shortName,
      this.sortOrder,
      this.tags,
      this.tax,
      this.type,
      this.updateAt,
      this.variants,
      this.useVariants,
      this.productsSections);

  Product.fromMap(Map snapshot, String id)
      : allergens = _mappingAllergens(snapshot['allergens']),
        allowObservations = snapshot['allow_observations'] ?? null,
        availableStores = _mappingAvailableStores(snapshot['available_stores']),
        barcode = snapshot['barcode'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        categoryId = snapshot['category_id'] ?? null,
        createAt = snapshot['create_at'] ?? null,
        deleteAt = snapshot['delete_at'] ?? null,
        description = snapshot['description'] ?? null,
        id = id ?? null,
        images = _mappingImages(snapshot['images']),
        isAvailable = snapshot['is_available'] ?? null,
        isFeatured = snapshot['is_featured'] ?? null,
        isNew = snapshot['is_new'] ?? null,
        isPublic = snapshot['is_public'] ?? null,
        modifiers = _mappingModifiers(snapshot['modifiers'], snapshot['variants']) ?? null,
        originalModifiers = _mappingOriginalModifiers(snapshot['modifiers']) ?? null,
        name = snapshot['name'] ?? null,
        price = double.parse(snapshot['price'].toString()) ?? null,
        qrCode = snapshot['qr_code'] ?? null,
        ref = snapshot['ref'] ?? null,
        shortDescription = snapshot['short_description'] ?? null,
        sortOrder = snapshot['sort_order'] ?? 9999,
        tags = _mappingTags(snapshot['tags']),
        tax = double.parse(snapshot['tax'].toString()) ?? null,
        type = snapshot['type'] ?? null,
        updateAt = snapshot['update_at'] ?? null,
        variants = _mappingVariants(snapshot['variants']),
        useVariants = snapshot['use_variants'] ?? null,
        productsSections =
            _mappingProductSections(snapshot['products_sections']);

  toJson() {
    return {
      "allergens":
          allergens != null ? allergens.map((e) => e.toJson()).toList() : null,
      "allow_observations": allowObservations,
      "available_stores": availableStores,
      "barcode": barcode,
      "business_id": businessId,
      "category_id": categoryId,
      "create_at": createAt,
      "delete_at": deleteAt,
      "description": description,
      "id": id,
      "images": images != null ? images.map((e) => e.toJson()).toList() : null,
      "is_available": isAvailable,
      "is_featured": isFeatured,
      "is_new": isNew,
      "is_public": isPublic,
      "modifiers":
          modifiers != null ? modifiers.map((e) => e.toJson()).toList() : null,
      "original_modifiers":
      modifiers != null ? modifiers.map((e) => e.toJson()).toList() : null,
      "name": name,
      "price": price,
      "qr_code": qrCode,
      "ref": ref,
      "short_description": shortDescription,
      "sor_order": sortOrder,
      "tags": tags,
      "tax": tax,
      "type": type,
      "update_at": updateAt,
      "variants":
          variants != null ? variants.map((e) => e.toJson()).toList() : null,
      "use_variants": useVariants,
      "products_sections": productsSections != null
          ? productsSections.map((e) => e.toJson()).toList()
          : null,
    };
  }

  static List<Allergen> _mappingAllergens(List<dynamic> allergens) {
    if (allergens == null) {
      return null;
    } else {
      return allergens.map((e) => Allergen.fromMap(e)).toList();
    }
  }

  static List<ProductSection> _mappingProductSections(List<dynamic> sections) {
    if (sections == null) {
      return null;
    } else {
      return sections.map((e) => ProductSection.fromMap(e)).toList();
    }
  }

  static List<ImageConfig> _mappingImages(List<dynamic> images) {
    if (images == null) {
      return null;
    } else {
      return images.map((e) => ImageConfig.fromMap(e)).toList();
    }
  }

  static List<Modifier> _mappingOriginalModifiers(List<dynamic> modifiers) {
    if (modifiers == null) {
      return null;
    } else {
      return modifiers.map((e) => Modifier.fromMap(e)).toList();
    }
  }

  static List<Modifier> _mappingModifiers(List<dynamic> modifiers, List<dynamic> variants) {
    if (modifiers == null) {
      return null;
    } else {

      if (variants == null) {
        List<Modifier> mdfs = modifiers.map((e) => Modifier.fromMap(e)).toList();
        mdfs.forEach((modifier) {
          if (modifier.noApplyRecipe == true) {
            modifier.options.forEach((option) {
              option.quantity = 0;
            });
          }
        });
        return mdfs;
      } else {
        return modifiers.map((e) => Modifier.fromMap(e)).toList();
      }


    }
  }

  static List<Variant> _mappingVariants(List<dynamic> variants) {
    if (variants == null) {
      return null;
    } else {
      return variants.map((e) => Variant.fromMap(e)).toList();
    }
  }


  static List<String> _mappingTags(List<dynamic> tags) {
    if (tags == null) {
      return null;
    } else {
      return tags.map((e) => e.toString()).toList();
    }
  }

  static List<String> _mappingAvailableStores(List<dynamic> availableStores) {
    if (availableStores == null) {
      return null;
    } else {
      return availableStores.map((e) => e.toString()).toList();
    }
  }
}

class ProductFormat {
  String id;
  String name;
  String ticketName;
  String description;
  bool active;
  dynamic price;
  dynamic sortOrder;

  ProductFormat(
      {this.id,
      this.name,
      this.ticketName,
      this.description,
      this.active,
      this.price,
      this.sortOrder});

  ProductFormat.fromMap(Map snapshot, String id)
      : id = id ?? '',
        name = snapshot['name'] ?? '',
        ticketName = snapshot['ticket_name'] ?? '',
        description = snapshot['description'] ?? '',
        active = snapshot['active'] ?? false,
        price = snapshot['price'].toDouble() ?? 0.00,
        sortOrder = snapshot['sort_order'];
}

class DataProductConfig {
  Category category;
  Product product;
  ProductSection section;
  String variantId;

  DataProductConfig(
      {this.category, this.product, this.section, this.variantId});
}
