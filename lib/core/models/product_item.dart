class ProductItem {
  String id;
  String name;
  String description;
  String variantId;

  ProductItem.fromMap(Map snapshot) :
      id = snapshot['id'] ?? null,
      name = snapshot['name'] ?? null,
      description = snapshot['description'] ?? null,
      variantId = snapshot['variant_id'] ?? null;

  toJson() {
    return {
      'id' : id,
      'name' : name,
      'description' : description,
      'variant_id' : variantId
    };
  }
}