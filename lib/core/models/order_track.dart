import 'package:cloud_firestore/cloud_firestore.dart';

class OrderTrack {
  String id;
  String status;
  Timestamp date;
  String comment;
  String label;
  String description;

  OrderTrack.fromMap(Map snapshot, String id)
      : id = id,
        status = snapshot['status'] ?? null,
        date = snapshot['date'] ?? null,
        comment = snapshot['comment'] ?? null,
        label = snapshot['label'] ?? null,
        description = snapshot['description'] ?? null;

  toJson() {
    return {
      'id': id,
      'status': status,
      'date': date,
      'comment': comment,
      'label': label,
      'description': description
    };
  }
}
