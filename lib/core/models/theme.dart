class Theme {

  String color;
  String mode;

  Theme({this.color, this.mode});
}