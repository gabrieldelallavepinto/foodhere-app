import '../../core/models/modifier.dart';

class Variant {
  bool active;
  String businessId;
  String categoryId;
  String description;
  String id;
  List<Modifier> modifiers;
  String name;
  double price;
  bool selected;
  String shortName;
  int sortOrder;

  Variant(
      {this.active,
      this.businessId,
      this.price,
      this.name,
      this.description,
      this.id,
      this.sortOrder,
      this.categoryId,
      this.selected,
      this.shortName,
      this.modifiers});

  Variant.fromMap(Map snapshot)
      : active = snapshot['active'] ?? null,
        id = snapshot['id'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        price = double.parse(snapshot['price'].toString()) ?? null,
        name = snapshot['name'] ?? null,
        description = snapshot['description'] ?? null,
        sortOrder = snapshot['sort_order'] ?? 999999,
        categoryId = snapshot['category_id'] ?? null,
        selected = snapshot['selected'] ?? null,
        modifiers = _mappingModifiers(snapshot['modifiers']),
        shortName = snapshot['short_name'] ?? null;

  toJson() {
    return {
      "id": id,
      "active": active,
      "business_id": businessId,
      "category_id": categoryId,
      "price": price,
      "name": name,
      "description": description,
      "sort_order": sortOrder,
      "selected": selected,
      "short_name": shortName,
      "modifiers":
          modifiers != null ? modifiers.map((e) => e.toJson()).toList() : null,
    };
  }

  static List<Modifier> _mappingModifiers(List<dynamic> modifiers) {
    if (modifiers == null) {
      return null;
    } else {
      List<Modifier> mdfs = modifiers.map((e) => Modifier.fromMap(e)).toList();
      mdfs.forEach((modifier) {
        if (modifier.noApplyRecipe == true) {
          modifier.options.forEach((option) {
            option.quantity = 0;
          });
        }
      });
      return mdfs;
    }
  }
}
