import '../../core/models/address.dart';
import '../../core/models/link.dart';

class Business {
  String id;
  bool active;
  String status;
  String domain;
  String name;
  String businessName;
  String documentation;
  String phone;
  String optionalPhone;
  Address address;
  String website;
  List<Link> links;
  dynamic images;

  Business(
      {this.id,
      this.active,
      this.status,
      this.domain,
      this.name,
      this.businessName,
      this.documentation,
      this.optionalPhone,
      this.phone,
      this.address,
      this.website,
      this.images,
      this.links});

  Business.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        active = snapshot['active'] ?? null,
        status = snapshot['status'] ?? null,
        domain = snapshot['domain'] ?? null,
        name = snapshot['name'] ?? null,
        businessName = snapshot['business_name'] ?? null,
        documentation = snapshot['documentation'] ?? null,
        phone = snapshot['phone'] ?? null,
        optionalPhone = snapshot['optional_phone'] ?? null,
        address =
            Address.fromMap(snapshot['address'], snapshot['address']['id']) ??
                null,
        website = snapshot['website'] ?? null,
        images = snapshot['images'],
        links = _mappingLinks(snapshot['links']);

  toJson() {
    return {
      "id": id,
      "active": active,
      "status": status,
      "domain": domain,
      "name": name,
      "business_name": businessName,
      "documentation": documentation,
      "phone": phone,
      "optional_phone": optionalPhone,
      "address": address,
      "website": website,
      "images": images,
      "links" : links != null ? links.map((e) => e.toJson()).toList() : null
    };
  }

  static List<Link> _mappingLinks(List<dynamic> links) {
    if (links == null) {
      return null;
    } else {
      return links.map((e) => Link.fromMap(e)).toList();
    }
  }
}
