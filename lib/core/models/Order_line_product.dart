class OrderLineProduct {
  String id;
  int sinqroId;

  OrderLineProduct({this.id, this.sinqroId});

  OrderLineProduct.fromMap(Map snapshot)
      : id = snapshot['id'] ?? null,
        sinqroId = snapshot['sinqro_id'] ?? null;

  toJson() {
    return {'id': id, 'sinqro_id': sinqroId};
  }
}
