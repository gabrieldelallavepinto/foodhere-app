import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tabitas/core/models/image_config.dart';

class Promotion {
  PromotionAction action;
  bool active;
  List<dynamic> availableStores;
  List<dynamic> availableOrderTypes;
  String businessId;
  Timestamp createAt;
  Timestamp dateEnd;
  Timestamp dateStart;
  Timestamp deleteAt;
  String description;
  bool highlighted;
  String id;
  List<ImageConfig> images;
  String name;
  String shortName;
  int sortOrder;
  Timestamp updateAt;

  Promotion(
      {this.action,
      this.active,
      this.availableStores,
      this.availableOrderTypes,
      this.businessId,
      this.createAt,
      this.dateEnd,
      this.dateStart,
      this.deleteAt,
      this.description,
      this.highlighted,
      this.id,
      this.images,
      this.name,
      this.shortName,
      this.sortOrder,
      this.updateAt}
      );

  Promotion.fromMap(Map snapshot)
      : action = snapshot['action'] == null ? null : PromotionAction.fromMap(snapshot['action']),
        active = snapshot['active'] ?? null,
        availableStores = snapshot['available_stores'] ?? null,
        availableOrderTypes = snapshot['available_order_types'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        createAt = snapshot['created_at'] ?? null,
        dateEnd = snapshot['date_end'] ?? null,
        dateStart = snapshot['date_start'] ?? null,
        deleteAt = snapshot['delete_at'] ?? null,
        description = snapshot['description'] ?? null,
        highlighted = snapshot['highlighted'] ?? null,
        id = snapshot['id'] ?? null,
        images = _mappingImages(snapshot['images']),
        name = snapshot['name'] ?? null,
        shortName = snapshot['short_name'] ?? null,
        sortOrder = snapshot['sort_order'] ?? null,
        updateAt = snapshot['update_at'] ?? null;

  toJson() {
    return {
      "action": action,
      "active": active,
      "available_stores": availableStores,
      "available_order_types": availableOrderTypes,
      "business_id": businessId,
      "create_at": createAt,
      "date_end": dateEnd,
      "date_start": dateStart,
      "delete_at": deleteAt,
      "description": description,
      "highlighted": highlighted,
      "id": id,
      "images": images != null ? images.map((e) => e.toJson()).toList() : null,
      "name": name,
      "short_name": shortName,
      "sortOrder": sortOrder,
      "update_at": updateAt,
    };
  }

  static List<ImageConfig> _mappingImages(List<dynamic> images) {
    if (images == null) {
      return null;
    } else {
      return images.map((e) => ImageConfig.fromMap(e)).toList();
    }
  }
}

class PromotionAction {
  dynamic data;
  PromotionActionFilters filters;
  String link;
  dynamic ref;
  String type;

  PromotionAction.fromMap(Map snapshot) :
      data = _getData(snapshot['data'], snapshot['type']) ?? null,
      filters = snapshot['filters'] != null ? PromotionActionFilters.fromMap(snapshot['filters']) : null,
      ref = snapshot['ref'] ?? null,
      link = snapshot['link'] ?? null,
      type = snapshot['type'] ?? null;

  toJson() {
    return {
      "data": data,
      "filters": filters,
      "ref": ref,
      "type": type
    };
  }

  static dynamic _getData(Map data, String type) {
    switch (type) {
      case 'product_discount':
        return ActionProductDiscount.fromMap(data);
        break;
      case 'buyX_getY':
        return ActionBuyXGetY.fromMap(data);
        break;
      default:
        return null;
        break;
    }
  }
}

class ActionProductDiscount {
  double priceReduction;
  String priceReductionType;

  ActionProductDiscount.fromMap(Map snapshot):
    priceReduction = snapshot['price_reduction'] ?? null,
    priceReductionType = snapshot['price_reduction_type'] ?? null;

  toJson() {
    return {
      'price_reduction' : priceReduction,
      'price_reduction_type' : priceReductionType
    };
  }
}

class ActionBuyXGetY {
  int buyX;
  int getY;
  double priceReduction;
  String priceReductionType;
  String chargeType;

  ActionBuyXGetY.fromMap(Map snapshot)
      : buyX = snapshot['buy_x'] ?? null,
        getY = snapshot['get_y'] ?? null,
        priceReduction =
            double.parse(snapshot['price_reduction'].toString()) ?? null,
        priceReductionType = snapshot['price_reduction_type'] ?? null,
        chargeType = snapshot['charge_type'] ?? null;

  toJson() {
    return {
      "buy_x": buyX,
      "get_y": getY,
      "price_reduction": priceReduction,
      "price_reduction_type": priceReductionType,
      "charge_type": chargeType
    };
  }
}

class PromotionActionFilters {
  List<PromotionActionFilter> categories;
  List<PromotionActionFilter> products;
  List<PromotionActionFilter> variants;

  PromotionActionFilters({this.categories, this.products, this.variants});

  PromotionActionFilters.fromMap(Map snapshot):
      categories = _getFilters(snapshot['categories']),
      products = _getFilters(snapshot['products']),
      variants = _getFilters(snapshot['variants']);

  toJson() {
    return {
      'categories' : categories,
      'products' : products,
      'variants' : variants,
    };
  }

  static List<PromotionActionFilter> _getFilters(List<dynamic> snapshots) {
    List<PromotionActionFilter> filters = [];
    snapshots.forEach((filter) {
      filters.add(PromotionActionFilter.fromMap(filter));
    });
    return filters;
  }
}

class PromotionActionFilter {
  String id;
  String name;

  PromotionActionFilter.fromMap(Map snapshot) :
      id =  snapshot['id'],
      name = snapshot['name'];

  toJson() {
    return {
      'id' : id,
      'name' : name
    };
  }
}
