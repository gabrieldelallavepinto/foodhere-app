class OrderLineCoupon {
  String code;

  OrderLineCoupon({this.code});

  toJson() {
    return {
      'code': code,
    };
  }
}
