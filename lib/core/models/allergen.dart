class Allergen {
  String label;
  String name;
  bool status;

  Allergen.fromMap(Map snapshot) :
      label = snapshot['label'] ?? null,
      name = snapshot['name'] ?? null,
      status = snapshot['status'] ?? null;

  toJson() {
    return {
      "label": label,
      "name": name,
      "status": status
    };
  }
}