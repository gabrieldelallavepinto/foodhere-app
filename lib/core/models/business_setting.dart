import 'payment_methods.dart';

class BusinessSetting {
  PaymentMethods paymentMethods;

  BusinessSetting({this.paymentMethods});

  BusinessSetting.fromMap(Map snapshot)
      : paymentMethods =
            PaymentMethods.fromMap(snapshot['payment_methods']) ?? null;

  toJson() {
    return {"payment_methods": paymentMethods};
  }
}
