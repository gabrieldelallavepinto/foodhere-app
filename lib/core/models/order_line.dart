import 'package:tabitas/core/models/order_line_product_modifier.dart';

import '../../core/models/Order_line_product.dart';
import '../../core/models/order_line_coupon.dart';
import '../../core/models/order_line_discount.dart';
import '../../core/models/order_line_menu.dart';
import '../../core/models/order_line_product_option.dart';

class OrderLine {
  double totalAmount;
  double unitPrice;
  int quantity;
  double tax;
  String comment;
  String label;
  bool modifierShowSelectedOptions;
  bool modifierShowUnselectedOptions;
  String type; // PRODUCT_SIMPLE, PRODUCT_COMBO, PRODUCT_OPTION, PRODUCT_MODIFIER, DISCOUNT, DELIVERY, CUSTOM
  double distance;
  OrderLineProduct product;
  OrderLineMenu menu;
  OrderLineProductOption productOption;
  OrderLineProductModifier productModifier;
  OrderLineDiscount discount;
  OrderLineCoupon coupon;
  List<OrderLine> childrenOrderLines;

  OrderLine({this.totalAmount, this.unitPrice, this.quantity, this.comment,
      this.label, this.modifierShowSelectedOptions, this.modifierShowUnselectedOptions, this.type, this.distance, this.product, this.menu,
      this.productOption, this.productModifier, this.discount, this.coupon, this.childrenOrderLines, this.tax});

  OrderLine.fromMap(Map snapshot) :
      totalAmount = double.parse(snapshot['total_amount'].toString()) ?? 0.0,
      unitPrice = double.parse(snapshot['unit_price'].toString()) ?? 0.0,
      quantity = snapshot['quantity'] ?? 0,
      comment = snapshot['comment'] ?? null,
      label = snapshot['label'] ?? null,
      modifierShowSelectedOptions = snapshot['modifier_show_selected_options'] ?? null,
      modifierShowUnselectedOptions = snapshot['modifier_show_unselected_options'] ?? null,
      type = snapshot['type'] ?? null,
      distance = snapshot['distance'] ?? null,
      product = snapshot['product'] != null ? OrderLineProduct.fromMap(snapshot['product']) : null,
      menu = snapshot['menu'] != null ? OrderLineMenu.fromMap(snapshot['menu']) : null,
      productModifier = snapshot['product_modifier'] != null ? OrderLineProductModifier.fromMap(snapshot['product_modifier']) : null,
      productOption = snapshot['product_option'] != null ? OrderLineProductOption.fromMap(snapshot['product_option']): null,
      discount = snapshot['discount'] ?? null,
      coupon = snapshot['coupon'] ?? null,
      tax = snapshot['tax'] != null ? double.parse(snapshot['tax'].toString()) : null,
      childrenOrderLines = _mappingOrderLines(snapshot['children_order_lines']);

  toJson() {
    return {
      'total_amount': totalAmount,
      'unit_price': unitPrice,
      'quantity' : quantity,
      'comment' : comment,
      'label' : label,
      'modifier_show_unselected_options' : modifierShowUnselectedOptions,
      'modifier_show_selected_options' : modifierShowSelectedOptions,
      'type' : type,
      'distance' : distance,
      'product' : product != null ? product.toJson() : null,
      'menu' : menu != null ? menu.toJson() : null,
      'product_option': productOption != null ? productOption.toJson() : null,
      'product_modifier': productModifier != null ? productModifier.toJson() : null,
      'discount' : discount != null ? discount.toJson() : null,
      'coupon' : coupon != null ? coupon.toJson() : null,
      'tax' : tax,
      'children_order_lines' : childrenOrderLines != null ? childrenOrderLines.map((e) => e.toJson()).toList() : null
    };
  }

  static List<OrderLine> _mappingOrderLines(List<dynamic> orderLines) {
    if (orderLines == null) {
      return null;
    } else {
      return orderLines.map((e) => OrderLine.fromMap(e)).toList();
    }
  }

}