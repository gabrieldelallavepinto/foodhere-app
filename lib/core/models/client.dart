import 'package:cloud_firestore/cloud_firestore.dart';

class Client {
  String avatarUrl;
  Timestamp birthdate;
  String businessId;
  String customerId;
  String email;
  String firstName;
  String id;
  String gender;
  String lastName;
  String membershipCode;
  String phone;

  Client({
    this.avatarUrl,
    this.birthdate,
    this.businessId,
    this.customerId,
    this.email,
    this.firstName,
    this.gender,
    this.id,
    this.lastName,
    this.membershipCode,
    this.phone,
  });

  Client.fromMap(Map snapshot, String id)
      : id = id ?? null,
        avatarUrl = snapshot['avatar_url'] ?? null,
        birthdate = snapshot['birthdate'] ?? null,
        businessId = snapshot['business_id'] ?? null,
        customerId = snapshot['customer_id'] ?? null,
        email = snapshot['email'] ?? null,
        firstName = snapshot['first_name'] ?? null,
        gender = snapshot['gender'] ?? null,
        lastName = snapshot['last_name'] ?? null,
        membershipCode = snapshot['memberchip_code'] ?? null,
        phone = snapshot['phone'] ?? null;

  toJson() {
    return {
      "avatar_url": avatarUrl,
      "birthdate": birthdate,
      "business_id": businessId,
      "customer_id": customerId,
      "email": email,
      "first_name": firstName,
      "gender": gender,
      "id": id,
      "last_name": lastName,
      "membership_code": membershipCode,
      "phone": phone
    };
  }
}
