import 'package:tabitas/core/models/payment_methods.dart';

class BusinessSetting {
  PaymentMethods paymentMethods;
  TermsConditions termsConditions;

  BusinessSetting.fromMap(Map snapshot) :
      paymentMethods = _mappingPaymentMethods(snapshot['payment_methods']);

  toJson() {
   return {
   'payment_methods' : paymentMethods == null ? null : paymentMethods.toJson()
   };
  }

  static _mappingPaymentMethods(Map snapshot) {
    if (snapshot == null) {
      return null;
    } else {
      return PaymentMethods.fromMap(snapshot['payment_methods']);
    }
  }
}

class TermsConditions {
  String text;

  TermsConditions.fromMap(Map snapshot):
      text = snapshot['text'] ?? null;

  toJson() {
    return {
      'text' : text
    };
  }
}