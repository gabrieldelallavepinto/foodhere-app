import 'package:flutter/material.dart';
import '../../../../core/models/address.dart';

class AddressItem extends StatefulWidget {

  final Address address;
  final Function onTapItem;
  final Function onDeleteAddress;
  final bool isEditor;

  AddressItem({@required this.address, this.isEditor, this.onDeleteAddress, @required this.onTapItem});

  @override
  _AddressItemState createState() => _AddressItemState();
}

class _AddressItemState extends State<AddressItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => _onTapItem(),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(Icons.place, color: Colors.blueAccent),
                ),
                Expanded(child: Text(widget.address.formattedAddress)),
                widget.isEditor == true ? Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 8.0),
                  child: IconButton(
                    icon: Icon(Icons.remove_circle, color: Colors.red,),
                    onPressed: () => _deleteAddress(widget.address),
                  )
                ): SizedBox(),
                widget.isEditor == true ? SizedBox() : Icon(Icons.keyboard_arrow_right, color: Colors.black38,)
              ],
            ),
            Divider(height: 10,)
          ],
        ),
      ),
    );
  }

  _deleteAddress(Address address) {
    widget.onDeleteAddress(address);
  }

  _onTapItem() {
    print('on tap');
    widget.onTapItem(widget.address);
  }
}
