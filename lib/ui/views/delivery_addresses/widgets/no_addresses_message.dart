import 'package:flutter/material.dart';
import '../values.dart';

class NoAddressesMessage extends StatelessWidget {

  final String message;

  NoAddressesMessage({@required this.message});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Icon(
            Icons.info,
            color: ModuleColors.NoAddressesMessage_icon,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              message,
              style: TextStyle(color: ModuleColors.NoAddressesMessage_message),
            ),
          ),
        ],
      ),
    );
  }
}
