import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/ui/views/delivery_addresses/widgets/no_addresses_message.dart';
import './values.dart';

import '../../../ui/theme.dart';
import '../../../appConfig/appconfig.dart';
import '../../../core/models/address.dart';
import '../../../core/models/stores.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/store_model.dart';
import '../../../core/viewmodels/userViewModel.dart';
import './widgets/address_item.dart';

class DeliveryAddresses extends StatefulWidget {
  final bool editor;

  DeliveryAddresses({this.editor});

  @override
  _DeliveryAddressesState createState() => _DeliveryAddressesState();
}

class _DeliveryAddressesState extends State<DeliveryAddresses> {
  OrderModel _orderProvider;
  UserViewModel _userProvider;

  _onTapItem(Address address) {
    List<dynamic> zones = _orderProvider.order.store.orderTypes.delivery.zones
        .where((e) => e['postal_code'] == address.postalCode)
        .toList();
    if (zones.isNotEmpty) {
      _orderProvider.order.deliveryAddress = address;
      Navigator.of(context).pushNamed('categories');
    } else {
      _showInvalidAddressAlert(context, _orderProvider);
    }
  }

  _onDeleteAddress(Address address) {
    _showDeleteAddressAlert(context, _userProvider, address);
  }

  @override
  Widget build(BuildContext context) {
    List<Address> addresses = [];
    UserViewModel userProvider = Provider.of<UserViewModel>(context);
    StoreModel storeProvider = Provider.of<StoreModel>(context);
    OrderModel orderProvider = Provider.of<OrderModel>(context);
    _orderProvider = orderProvider;
    _userProvider = userProvider;

    return Scaffold(
      appBar: AppBar(
        title: Text('Direcciones'),
        actions: [
          widget.editor == true
              ? FlatButton.icon(
                  icon: Icon(
                    Icons.add,
                    color: ModuleColors.appBarActionColor,
                  ),
                  textColor: ModuleColors.appBarActionColor,
                  label: Text('Nueva'),
                  onPressed: () {
                    _openPlacePicker(
                        context,
                        orderProvider,
                        userProvider,
                        userProvider.position,
                        storeProvider.selectedStore,
                        storeProvider.stores);
                  })
              : SizedBox()
        ],
      ),
      body: StreamBuilder(
        stream: userProvider.fetchAddresses(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            addresses = snapshot.data.documents
                .map((doc) => Address.fromMap(doc.data, doc.documentID))
                .toList();
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.editor == true
                        ? SizedBox()
                        : Text(
                            storeProvider.selectedStore.name,
                            style: TextStyle(
                                color: theme.accentColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                    widget.editor == true
                        ? SizedBox()
                        : Text(
                            'Pedido con envío a domicilio',
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                    addresses.isNotEmpty && widget.editor != true
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Text(
                              '¿Quieres usar una de tus direcciones?',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        : SizedBox(),
                    addresses.isNotEmpty
                        ? Column(
                            children: _generateItems(addresses),
                          )
                        : NoAddressesMessage(
                            message:
                                'Actualmente no dispones de direcciones almacenadas en tu perfil.'),
                    widget.editor == true
                        ? SizedBox()
                        : Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: FlatButton(
                                child: Text(addresses.isNotEmpty
                                    ? 'Otra dirección'
                                    : 'Obtener dirección'),
                                textColor: Colors.white,
                                onPressed: () {
                                  _openPlacePicker(
                                      context,
                                      orderProvider,
                                      userProvider,
                                      userProvider.position,
                                      storeProvider.selectedStore,
                                      storeProvider.stores);
                                },
                                color: theme.accentColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(
                                    color: theme.accentColor,
                                  ),
                                ),
                              ),
                            ),
                          )
                  ],
                ),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  List<Widget> _generateItems(List<Address> addresses) {
    List<Widget> widgets = [];
    addresses.forEach((address) {
      widgets.add(AddressItem(
        address: address,
        onTapItem: _onTapItem,
        isEditor: widget.editor,
        onDeleteAddress: _onDeleteAddress,
      ));
    });

    return widgets;
  }

  _openPlacePicker(
      BuildContext context,
      OrderModel orderProvider,
      UserViewModel userProvider,
      Position position,
      Store store,
      List<Store> stores) {
    bool AddressValid = false;
    Address _address;

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PlacePicker(
          searchingText: 'Buscando...',
          hintText: widget.editor == true
              ? 'Buscar una dirección'
              : 'Donde quieres tu pedido',
          selectedPlaceWidgetBuilder:
              (context, selectedPlace, state, isSearchBarFocused) {
            print("state: $state, isSearchBarFocused: $isSearchBarFocused");

            if (isSearchBarFocused) {
              return Container();
            } else {
              if (state != SearchingState.Searching) {
                selectedPlace.addressComponents.forEach((component) {
                  if (component.types[0] == 'postal_code') {
                    String postalCode = component.longName;

                    if (widget.editor == false) {
                      List<dynamic> zones = store.orderTypes.delivery.zones
                          .where((e) => e['postal_code'] == postalCode)
                          .toList();
                      AddressValid = zones.isNotEmpty;
                    } else {
                      int counter = 0;
                      stores
                          .where((element) => element.active == true)
                          .toList()
                          .forEach((element) {
                        List<dynamic> zones = store.orderTypes.delivery.zones
                            .where((e) => e['postal_code'] == postalCode)
                            .toList();
                        zones.isNotEmpty ? counter++ : counter = counter;

                        AddressValid = counter > 0;
                      });
                    }
                  }
                });
              }
            }

            return isSearchBarFocused
                ? Container(
                    height: 0,
                    width: 0,
                  )
                : FloatingCard(
                    bottomPosition: 10.0,
                    leftPosition: 10.0,
                    rightPosition: 10.0,
                    width: 500,
                    borderRadius: BorderRadius.circular(10.0),
                    child: state == SearchingState.Searching
                        ? Center(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    selectedPlace.formattedAddress,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                AddressValid == false
                                    ? Chip(
                                        label: Text(
                                            'No podemos atender el pedido en esta dirección'),
                                        backgroundColor: Colors.orange,
                                      )
                                    : FlatButton(
                                        child: Text(
                                          'Seleccionar',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color: theme.primaryColor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(18.0),
                                          side: BorderSide(
                                            color: theme.primaryColor,
                                          ),
                                        ),
                                        onPressed: () {
                                          print(selectedPlace);
                                          _address = userProvider
                                              .addAddress(selectedPlace);
                                          if (widget.editor == true) {
                                            _saveAddressAndContinue(
                                                userProvider, _address);
                                          } else {
                                            _showSaveAddressAlert(
                                                context,
                                                orderProvider,
                                                userProvider,
                                                _address);
                                          }
                                        },
                                      ),
                              ],
                            ),
                          ));
          },
          apiKey: Platform.isAndroid
              ? AppConfig.googleMapsApiKey_android
              : AppConfig.googleMapsApiKey_iOS,
          // Put YOUR OWN KEY here.

          useCurrentLocation: true,
        ),
      ),
    );
  }

  _showDeleteAddressAlert(
      BuildContext context, UserViewModel userProvider, Address address) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        child: AlertDialog(
          title: Text(AppConfig.businessName),
          content: Text('¿Quieres eliminar esta dirección?'),
          actions: <Widget>[
            FlatButton(
              child: Text("NO"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            FlatButton(
              child: Text("SI"),
              onPressed: () {
                _deleteAddresInOrder(context, userProvider, address);
                Navigator.of(context).pop(true);
              },
            ),
          ],
        ));
  }

  _showInvalidAddressAlert(BuildContext context, OrderModel orderProvider) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      child: AlertDialog(
        title: Text(AppConfig.businessName),
        content: Text(
            'La tienda ${orderProvider.order.store.name} no ofrece servicio a domicilio a la dirección seleccionada.'),
        actions: <Widget>[
          FlatButton(
            child: Text("Aceptar"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  _showSaveAddressAlert(BuildContext context, OrderModel orderProvider,
      UserViewModel userProvider, Address address) {
    print('save address alert');
    return showDialog(
      context: context,
      barrierDismissible: false,
      child: AlertDialog(
        title: Text(AppConfig.businessName),
        content: Text(
            '¿Quieres guardar esta dirección para utilizarla para otros pedidos?'),
        actions: <Widget>[
          FlatButton(
            child: Text("NO"),
            onPressed: () {
              _saveAddresInOrder(context, orderProvider, address);
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed('categories');
            },
          ),
          FlatButton(
            child: Text("SI"),
            onPressed: () {
              _saveAddresInOrder(context, orderProvider, address);
              _saveAddressAndContinue(userProvider, address);
            },
          ),
        ],
      ),
    );
  }

  _deleteAddresInOrder(
      BuildContext context, UserViewModel userProvider, Address address) {
    userProvider.deleteAddress(address);
    setState(() {});
  }

  _saveAddresInOrder(
      BuildContext context, OrderModel orderProvider, Address address) {
    orderProvider.order.deliveryAddress = address;
  }

  _saveAddressAndContinue(UserViewModel userProvider, Address address) async {
    if (widget.editor == true) {
      Navigator.of(context).pop(true);
      await userProvider.saveAddress(address).then((_) {
        setState(() {});
      });
    } else {
      Navigator.of(context).pop(true);
      await userProvider.saveAddress(address).then((_) {
        Navigator.of(context).pop(true);
        Navigator.of(context).pushNamed('categories');
      });
    }
  }
}
