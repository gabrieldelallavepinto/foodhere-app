import 'dart:ui';
import 'package:flutter/material.dart';

class ModuleColors {
  static const Color appBarActionColor = Colors.white;

  //NoAddressesMessage
  static const Color NoAddressesMessage_icon = Colors.orange;
  static const Color NoAddressesMessage_message = Colors.black54;
}