import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../../../../appConfig/appconfig.dart';
import '../../../../core/models/order.dart';
import '../../../../ui/theme.dart';

class ItemOrder extends StatefulWidget {
  final Order order;
  final Function onTapItem;

  ItemOrder({@required this.order, @required this.onTapItem});

  @override
  _ItemOrderState createState() => _ItemOrderState();
}

class _ItemOrderState extends State<ItemOrder> {
  @override
  void initState() {
    initializeDateFormatting('es_ES');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          if (widget.order.status == 'finished') {
            Navigator.pushNamed(context, 'ticket', arguments: widget.order);
          } else {
            Navigator.pushNamed(context, 'trackingOrder',
                arguments: {'order': widget.order, 'fromConfirmation': false});
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: widget.order.status != 'finished'
                ? Colors.green[100]
                : Colors.transparent,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    _iconType(widget.order.type),
                    color: theme.accentColor,
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${_formatDate()} ${_formatTime()}',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w700),
                      ),
                      Text(
                        'Ref: #${widget.order.ref}',
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                      ),

                      Text(
                        widget.order.dateLabel ?? '',
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                      widget.order.type == 'delivery'
                          ? Text(
                        'Pedido a domicilio',
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                      )
                          : SizedBox(),
                      widget.order.type == 'delivery'
                          ? Text(
                        'Entrega en ${widget.order.deliveryAddress.formattedAddress}',
                        style: TextStyle(fontSize: 14),
                      )
                          : SizedBox(),
                      widget.order.type != 'delivery'
                          ? Text(
                        'Recogido en la tienda ${widget.order.store.name}',
                        style: TextStyle(fontSize: 14),
                      )
                          : SizedBox(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 6.0, left: 6.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.statusColor[widget.order.status],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 2.0, horizontal: 4.0),
                      child: Text(
                        AppConfig.statusName[widget.order.status].toUpperCase(),
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  IconData _iconType(String type) {
    switch (type) {
      case 'delivery':
        return Icons.motorcycle;
        break;
      case 'collect':
        return Icons.store;
        break;
      case 'in_situ':
        return Icons.store;
    }
  }

  String _formatDate() {
    var date = widget.order.date.toDate();
    return DateFormat.yMMMMEEEEd('es_ES').format(date);
  }

  String _formatTime() {
    var date = widget.order.date.toDate();
    return DateFormat.Hm('es_ES').format(date);
  }
}
