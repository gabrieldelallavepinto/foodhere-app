import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/order.dart';
import 'package:tabitas/core/viewmodels/order_model.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import 'package:tabitas/ui/views/my_orders/widgets/item_order.dart';

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  List<Order> orders;

  @override
  Widget build(BuildContext context) {
    final UserViewModel userProvider = Provider.of<UserViewModel>(context);
    final OrderModel orderProvider = Provider.of<OrderModel>(context);

    return Scaffold(
        appBar: AppBar(title: Text('Mis pedidos')),
        body: StreamBuilder(
            stream:
                orderProvider.fetchOrdersAsStream(uid: userProvider.user.uid),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                orders = snapshot.data.documents
                    .map((doc) => Order.fromMap(doc.data, doc.documentID))
                    .toList();

                if (orders.isNotEmpty) {
                  return ListView.separated(
                    separatorBuilder: (BuildContext context, index) => Divider(),
                    itemCount: orders.length,
                      itemBuilder: (BuildContext, index) => ItemOrder(order: orders[index], onTapItem: _onTapItem));
                } else {
                  return Padding(
                    padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 32, bottom: 8.0),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Icon(Icons.receipt, color: Colors.black26, size: 60),
                          Text(
                            'No tienes ningún pedido realizado',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 18, color: Colors.black45),
                          ),
                          Text(
                            'En el momento que los realices podrás comprobar su estado desde aquí',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black45),
                          ),
                        ],
                      ),
                    ),
                  );
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            })
    );
  }

  _onTapItem({Order order}) {
    print(order.id);
  }
}
