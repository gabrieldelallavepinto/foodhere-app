import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/appConfig/appconfig.dart';
import 'package:tabitas/core/models/stores.dart';
import 'package:tabitas/core/viewmodels/business_model.dart';
import 'package:tabitas/core/viewmodels/order_model.dart';
import 'package:tabitas/core/viewmodels/store_model.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import 'package:tabitas/ui/theme.dart';

class TypeOrder extends StatefulWidget {
  final bool cancelOrder;

  TypeOrder({@required this.cancelOrder});

  @override
  _TypeOrderState createState() => _TypeOrderState();
}

class _TypeOrderState extends State<TypeOrder> {
  @override
  Widget build(BuildContext context) {
    BusinessModel businessProvider = Provider.of<BusinessModel>(context);
    StoreModel storeProvider = Provider.of<StoreModel>(context);
    OrderModel orderProvider = Provider.of<OrderModel>(context);

    Store selectedStore = storeProvider.selectedStore;

    Future<bool> _onBackPressed() {
      if (widget.cancelOrder == true) {
        orderProvider.order = null;
        Navigator.of(context).pop(true);
      } else {
        return showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text(AppConfig.businessName),
                content: Text('¿Estas seguro que quieres cancelar su pedido?'),
                actions: <Widget>[
                  FlatButton(
                    child: Text("NO"),
                    onPressed: () => Navigator.of(context).pop(false),
                  ),
                  FlatButton(
                    child: Text("SI"),
                    onPressed: () {
                      orderProvider.order = null;
                      Navigator.of(context).pop(true);
                    },
                  ),
                ],
              ),
            ) ??
            false;
      }
    }

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: theme.primaryColor, title: Text('Tipo pedido')),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text('Tienda que atenderá tu pedido',
                          textAlign: TextAlign.center),
                      Text(
                        selectedStore.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: theme.accentColor,
                            fontWeight: FontWeight.w700,
                            fontSize: 18),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text('Selecciona como quieres tu pedido',
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 10,
                      ),
                      selectedStore.orderTypes.delivery != null && selectedStore.orderTypes.delivery.enabled == true
                          ? Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: FlatButton(
                                onPressed: () {
                                  _setOrderData(
                                      type: 'delivery',
                                      store: selectedStore,
                                      orderProvider: orderProvider,
                                      businessProvider: businessProvider,
                                      context: context);
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(

                                    borderRadius:
                                        new BorderRadius.circular(18.0),
                                    side: BorderSide(
                                        color: theme.primaryColor,
                                        width: 2.0)),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(
                                    'A domiclio',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 10,
                      ),
                      selectedStore.orderTypes.collect != null && selectedStore.orderTypes.collect.enabled == true
                          ? Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: FlatButton(
                                onPressed: () {
                                  _setOrderData(
                                      type: 'collect',
                                      store: selectedStore,
                                      orderProvider: orderProvider,
                                      businessProvider: businessProvider,
                                      context: context);
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(18.0),
                                    side: BorderSide(
                                        color: theme.primaryColor,
                                        width: 2.0)),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(
                                    'Para recoger',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 10,
                      ),
                      selectedStore.orderTypes.inStore != null && selectedStore.orderTypes.inStore.enabled == true
                          ? Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: FlatButton(
                                onPressed: () {
                                  _setOrderData(
                                      type: 'in_store',
                                      store: selectedStore,
                                      orderProvider: orderProvider,
                                      businessProvider: businessProvider,
                                      context: context);
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(18.0),
                                    side: BorderSide(
                                        color: theme.primaryColor,
                                        width: 2.0)),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(
                                    'Para tomar en local',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _setOrderData(
      {String type,
      Store store,
      OrderModel orderProvider,
      BusinessModel businessProvider,
      BuildContext context}) {
    orderProvider.setOrderType(type);
    orderProvider.setOrderStore(store);
    if (type == 'delivery') {
      Navigator.pushNamed(context, 'deliveryAddresses');
    } else {
      Navigator.pushNamed(context, 'categories');
    }
  }
}
