import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../ui/views/categories/values.dart';

import '../../../core/viewmodels/order_model.dart';
import '../../../ui/views/shared/shoppingCartBadge.dart';
import '../../../core/models/category.dart';
import '../../../core/viewmodels/category_model.dart';
import './widgets/categoryItem.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    List<Category> categories;
    final categoryProvider = Provider.of<CategoryModel>(context);
    final orderProvider = Provider.of<OrderModel>(context);

    _showShoppingCart() {
      Navigator.pushNamed(context, 'shoppingCart').then((_) => setState(() {}));
    }

    _onTapItem(Category category) {
      Navigator.pushNamed(context,
              (category.type == 'products') ? 'products' : 'promotions',
              arguments: category)
          .then((_) => setState(() {}));
    }

    return Scaffold(
      appBar: AppBar(
        title: ModuleConfig.showTitle ? Text('Familias') : SizedBox(),
        centerTitle: true,
        actions: <Widget>[
          orderProvider.order != null
              ? ShoppingCartBadge(
                  onTap: _showShoppingCart,
                  number: orderProvider.counterOrderLines(),
                )
              : SizedBox()
        ],
      ),
      body: Container(
        child: StreamBuilder(
          stream: categoryProvider.fetchCategoriesAsStream(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              categories = snapshot.data.documents
                  .map((doc) => Category.fromMap(doc.data))
                  .toList();

              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  itemCount: categories.length,
                  itemBuilder: (buildContext, index) => CategoryItem(
                    categoryDetails: categories[index],
                    onTapItem: _onTapItem,
                  ),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
