import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../../../ui/views/categories/values.dart';
import '../../../../core/models/image_config.dart';
import '../../../../core/models/category.dart';

class CategoryItem extends StatelessWidget {
  final Category categoryDetails;
  final Function onTapItem;

  CategoryItem({@required this.categoryDetails, this.onTapItem});

  @override
  Widget build(BuildContext context) {
    ImageConfig imageCategoryList;

    categoryDetails.images.forEach((image) {
      if (image.id == 'image') {
        imageCategoryList = image;
      }
    });
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GestureDetector(
        onTap: () => onTapItem(categoryDetails),
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: CachedNetworkImage(
                imageUrl: imageCategoryList.imageUrl,
                placeholder: (context, url) => Container(
                  padding: EdgeInsets.all(10.0),
                  width: double.infinity,
                  child: SpinKitCircle(
                    color: ModuleColors.imageSpinnerColor,
                    size: 40,
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            Positioned(
              bottom: -11,
              right: 10,
              child: Text(
                categoryDetails.name.toUpperCase(),
                textAlign: TextAlign.end,
                style: TextStyle(
                    color: ModuleColors.titleTextColor,
                    fontWeight: FontWeight.w900,
                    fontSize: size.width * 0.085),
              ),
            )
          ],
        ),
      ),
    );
  }
}
