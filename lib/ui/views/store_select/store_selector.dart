import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../core/viewmodels/store_model.dart';
import './../../../ui/theme.dart';

class StoreSelector extends StatefulWidget {

  final Function onTapChangeStore;

  StoreSelector({@required this.onTapChangeStore});

  @override
  _StoreSelectorState createState() => _StoreSelectorState();
}

class _StoreSelectorState extends State<StoreSelector> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final storeProvider = Provider.of<StoreModel>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: Container(
        decoration: BoxDecoration(
          color: theme.primaryColor,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), topLeft: Radius.circular(20)),
        ),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Tienda seleccionada',
                        style: TextStyle(color: Colors.white, fontSize: 10),
                      ),
                      Text(
                        storeProvider.selectedStore.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 14),
                      )
                    ],
                  ),
                ),
                FlatButton(

                  child: Text(
                    'Cambiar',
                    style: TextStyle(fontSize: 12, ),
                  ),
                  textColor: theme.primaryColor,
                  color: Colors.white,
                  onPressed: () => _onTapChangeStore(),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onTapChangeStore() {
    widget.onTapChangeStore();
  }
}
