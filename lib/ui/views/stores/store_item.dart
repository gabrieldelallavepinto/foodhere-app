import 'package:flutter/material.dart';
import 'package:tabitas/core/models/order_type.dart';
import 'package:tabitas/core/models/stores.dart';
import 'package:tabitas/ui/theme.dart';

class StoreItem extends StatefulWidget {
  final bool complete;
  final Store store;
  final Function onTapItem;

  StoreItem({@required this.store, @required this.onTapItem, this.complete});

  @override
  _StoreItemState createState() => _StoreItemState();
}

class _StoreItemState extends State<StoreItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _onTapItem();
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Text(
                            widget.store.name,
                            style: TextStyle(
                                color: theme.accentColor,
                                fontWeight: FontWeight.w700,
                                fontSize: 16),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: widget.store.status == 'opened' ? Colors.green : Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),

                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical:2.0, horizontal: 4.0),
                            child: Text(
                              widget.store.status == 'opened'
                                  ? 'Abierto'
                                  : widget.store.status == 'closed' ? 'Cerrado' : 'Pausado',
                              style: TextStyle(
                                  fontSize: 10,
                                  color: widget.store.status == 'opened'
                                      ? Colors.white
                                      : Colors.black54),
                            ),
                          ),
                        ),
                      ],
                    ),

                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, top: 8),
                          child: Icon(Icons.place, size: 14,),
                        ),
                        Expanded(
                          child: Text(
                            widget.store.address.formattedAddress,
                            style: TextStyle(fontSize: 12, color: Colors.black45),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(Icons.phone, size: 14,),
                        ),
                        Expanded(
                          flex: 7,
                          child: Text(
                            '${widget.store.phone}'
                            '${widget.store.optionalPhone != null && widget.store.optionalPhone != '' ? '/${widget.store.optionalPhone}' : ''}',
                            style: TextStyle(fontSize: 12, color: Colors.black45),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(Icons.schedule, size: 14,),
                        ),
                        Expanded(
                          flex: 7,
                          child: Text(
                            widget.store.scheduleLabel,
                            style: TextStyle(fontSize: 12, color: Colors.black45),
                          ),
                        ),
                      ],
                    ),
                    _generateOrderTypes(widget.store.orderTypes)
                  ],
                )
        ),
      ),
    );
  }

  _onTapItem() {
    widget.onTapItem(store: widget.store);
  }

  Widget _generateOrderTypes(OrderTypes orderTypes) {
    List<Widget> widgets = [];

    List<String> typeLabels = [];
    if (orderTypes.delivery != null && orderTypes.delivery.enabled == true) {
      typeLabels.add('Domicilio');
    }
    if (orderTypes.collect != null && orderTypes.collect.enabled == true) {
      typeLabels.add('Recoger');
    }
    if (orderTypes.inStore != null && orderTypes.inStore.enabled == true) {
      typeLabels.add('Local');
    }

    typeLabels.forEach((label) {
      widgets.add(Padding(
        padding: const EdgeInsets.only(right: 6.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child:Padding(
            padding: const EdgeInsets.symmetric(vertical:2.0, horizontal: 4.0),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 10,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ));
    });

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top:8.0),
          child: Row(
            children: widgets,
          ),
        )
      ],
    );
  }
}
