import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tabitas/appConfig/appconfig.dart';
import 'package:tabitas/core/models/stores.dart';
import 'package:tabitas/core/viewmodels/order_model.dart';
import 'package:tabitas/core/viewmodels/store_model.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import 'package:tabitas/ui/views/stores/store_item.dart';

class StoreSelecction extends StatefulWidget {
  final dynamic data;

  StoreSelecction({@required this.data});

  @override
  _StoreSelecctionState createState() => _StoreSelecctionState();
}

class _StoreSelecctionState extends State<StoreSelecction> {
  List<Store> stores;

  @override
  Widget build(BuildContext context) {
    StoreModel storeProvider = Provider.of<StoreModel>(context);
    OrderModel orderProvider = Provider.of<OrderModel>(context);
    UserViewModel userProvider = Provider.of<UserViewModel>(context);

    _onTapItem({Store store}) {
      storeProvider.selectedStore = store;
      storeProvider.saveStore(store.id);
      if (widget.data['initial'] == true) {
        Navigator.pushReplacementNamed(context, 'home',
            arguments: userProvider.userProfile);
      } else {
        Navigator.pop(context);
      }
    }

    Future<bool> _onBackPressed() {
      if (widget.data['cancelOrder'] == false) {
        Navigator.of(context).pop(true);
      } else {
        return showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text(AppConfig.businessName),
                content: Text('¿Estas seguro que quieres cancelar su pedido?'),
                actions: <Widget>[
                  FlatButton(
                    child: Text("NO"),
                    onPressed: () => Navigator.of(context).pop(false),
                  ),
                  FlatButton(
                    child: Text("SI"),
                    onPressed: () {
                      orderProvider.order = null;
                      Navigator.of(context).pop(true);
                    },
                  ),
                ],
              ),
            ) ??
            false;
      }
    }

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        
        appBar: AppBar(
          title: Text('Elige tu tienda'),
        ),
        body: StreamBuilder(
            stream: storeProvider.fetchStoresAsStream(),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                stores = snapshot.data.documents
                    .map((doc) => Store.fromMap(doc.data, doc.documentID))
                    .where((element) => element.active == true)
                    .toList();

                return ListView.builder(
                  itemCount: stores.length,
                  itemBuilder: (buildContext, index) => StoreItem(
                    complete: widget.data['inital'],
                    store: stores[index],
                    onTapItem: _onTapItem,
                  ),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }
        ),
      ),
    );
  }
}
