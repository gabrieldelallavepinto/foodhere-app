import 'package:flutter/material.dart';
import '../values.dart';

class FoodHereFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            height: 40,
            child: Image.asset(
              ModuleConfig.darkMode
                  ? "assets/images/foodhere_light.png"
                  : "assets/images/foodhere_light.png",
              fit: BoxFit.cover,
              color: ModuleConfig.darkMode ? Colors.white : null,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Powered by FoodHere System v.2.0',
              style: TextStyle(
                  color: ModuleConfig.darkMode
                      ? ModuleColors.footerTextColorForDarkTheme
                      : ModuleColors
                      .footerTextColorForLightTheme,
                  fontSize: 12),
            ),
          )
        ],
      ),
    );
  }
}
