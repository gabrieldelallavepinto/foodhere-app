// Loading module version 1.0

import 'package:flutter/material.dart';
import './widgets/foodhere_footer.dart';
import 'values.dart';

class Loading extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ModuleColors.pageColor,
        body: Container(
          decoration: settingBackground(type: ModuleConfig.backgroundType),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                    flex: 5,
                    child: Container(
                      child: Center(
                        child: Container(
                          width: ModuleConfig.logoWidth,
                          child: Image.asset(
                            ModuleConfig.darkMode
                                ? "assets/images/logo_white.png"
                                : "assets/images/logo.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    )),
                Expanded(
                    flex: 3,
                    child: settingSpinner(type: ModuleConfig.SpinnerType)),
                Expanded(
                    flex: 2,
                    child: FoodHereFooter()
                ),
              ],
            ),
          ),
        )
    );
  }
}
