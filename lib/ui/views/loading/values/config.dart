class ModuleConfig {

  // Background
  static const bool darkMode = false;
  static const String backgroundType = 'solid'; // solid, radial, linear, asset
  // Logo
  static const double logoWidth = 200.0;
  //Spinner
  static const String SpinnerType = 'ChasingDots';
  static const double SpinnerSize = 40.0;

}