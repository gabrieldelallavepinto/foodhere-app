import 'dart:ui';
import 'package:flutter/material.dart';

class ModuleColors {

  //BgColor

  static const Color pageInnerGradationColor = Color.fromARGB(255, 230, 54, 64);
  static const Color pageOuterGradationColor = Color.fromARGB(255, 139, 9, 21) ;
  static const Color pageColor = Colors.white;

  //Light theme
  static const Color spinnerColorForLightTheme = Color.fromARGB(255, 229, 30, 37);
  static const Color footerTextColorForLightTheme = Colors.black87;

  //Dark theme
  static const Color footerTextColorForDarkTheme = Colors.white;
  static const Color spinnerColorForDarkTheme = Colors.white;

}
