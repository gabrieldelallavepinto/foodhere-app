import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'colors.dart';
import 'config.dart';

settingBackground({String type}) {
  switch (type) {
    case 'asset':
      return BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/background_loading.jpg"),
              fit: BoxFit.cover,
              colorFilter:
              ColorFilter.mode(Colors.black54, BlendMode.darken)));
      break;
    case 'solid':
      return BoxDecoration(color: ModuleColors.pageColor);
      break;
    case 'radial':
      return BoxDecoration(
          gradient: RadialGradient(
              radius: 1.0,
              center: Alignment.center,
              colors: [
                ModuleColors.pageInnerGradationColor,
                ModuleColors.pageOuterGradationColor
              ]));
      break;
    case 'linear':
      return BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                ModuleColors.pageInnerGradationColor,
                ModuleColors.pageOuterGradationColor
              ]));
      break;
  }
}

settingSpinner({String type}) {
  switch (type) {
    case 'CubeGrid':
      return SpinKitCubeGrid(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'Circle':
      return SpinKitCircle(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'DoubleBounce':
      return SpinKitDoubleBounce(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'DualRing':
      return SpinKitDualRing(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'ChasingDots':
      return SpinKitChasingDots(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'FadingCircle':
      return SpinKitFadingCircle(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
    case 'FadingCube':
      return SpinKitFadingCube(
        color: ModuleConfig.darkMode
            ? ModuleColors.spinnerColorForDarkTheme
            : ModuleColors.spinnerColorForLightTheme,
        size: ModuleConfig.SpinnerSize,
      );
      break;
  }
}