import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:intl/date_symbol_data_local.dart';

import '../../../core/models/order.dart';
import '../../../core/models/order_track.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/userViewModel.dart';
import '../../../ui/theme.dart';

class TrackingOrder extends StatefulWidget {
  final dynamic data;

  TrackingOrder({@required this.data});

  @override
  _TrackingOrderState createState() => _TrackingOrderState();
}

class _TrackingOrderState extends State<TrackingOrder> {
  List<OrderTrack> orderTracks = [];
  String _time = '';
  Timer _timer;
  Order _order;
  bool _fromConfirmation;

  @override
  void initState() {
    initializeDateFormatting('es_ES');
    //_startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final OrderModel orderProvider = Provider.of<OrderModel>(context);
    UserViewModel userProvider = Provider.of<UserViewModel>(context);
    _order = widget.data['order'];
    _fromConfirmation = widget.data['fromConfirmation'];
    /*
    if (_timer.isActive) {
      _timer.cancel();
      _startTimer();
    }
     */

    return Scaffold(
      appBar: AppBar(
        title: Text('Seguimiento de pedido'),
        actions: _fromConfirmation == true
            ? [
          FlatButton(
              onPressed: () {
                orderProvider.cancelOrder().then((value) {
                  Navigator.pushNamedAndRemoveUntil(
                      context, 'home', (_) => false,
                      arguments: userProvider.userProfile);
                });
              },
              textColor: Colors.white,
              child: Text('Aceptar'))
        ]
            : [],
      ),
      body: FutureBuilder(
        future: orderProvider.getOrderById(_order.id),
        builder: (BuildContext context, AsyncSnapshot<Order> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            print(snapshot.data.ref);
            return Column(
              children: [
                Container(
                  color: Colors.black12,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: [
                              Text(
                                _order.type == 'delivery'
                                    ? 'HORA DE ENTREGA'
                                    : 'HORA DE RECOGIDA',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                _order.dateLabel,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: [
                              Text(
                                'NÚMERO DE PEDIDO',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text('#${_order.ref}', textAlign: TextAlign.center)
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: StreamBuilder(
                      stream:
                      orderProvider.fetchTrackingOrder(orderId: _order.id),
                      builder:
                          (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.active) {
                          if (snapshot.hasData) {
                            orderTracks = snapshot.data.documents
                                .map((doc) => OrderTrack.fromMap(
                                doc.data, doc.documentID))
                                .toList();
                            print('tracks: $orderTracks');

                            if (orderTracks.isNotEmpty) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Timeline(
                                    children: _buildTimelines(
                                        orderTracks: orderTracks),
                                    lineWidth: 1,
                                    iconSize: 4,
                                    primary: true,
                                    lineColor: Colors.black26,
                                    position: TimelinePosition.Left),
                              );
                            } else {
                              return Center(
                                child: Text('No existe tracking'),
                              );
                            }
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        } else {
                          return Container();
                        }
                      }),
                ),
                SafeArea(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'ticket', arguments: _order);
                    },
                    child: Text('Ver ticket',
                        style: TextStyle(color: Colors.white)),
                    color: theme.accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: theme.accentColor),
                    ),
                  ),
                )
              ],
            );
          } else {
            return Container(
              color: Colors.black12,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  List<TimelineModel> _buildTimelines({List<OrderTrack> orderTracks}) {
    List<TimelineModel> items = [];
    orderTracks.forEach((track) {
      items.add(
        TimelineModel(
          Padding(
            padding: const EdgeInsets.only(
                top: 20.0, bottom: 20.0, left: 30, right: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  track.label ?? '',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: track == orderTracks.last ? 18 : 16,
                      color: track == orderTracks.last
                          ? Colors.indigo
                          : Colors.black54),
                ),
                Text(
                  track.description ?? '',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: track == orderTracks.last
                          ? Colors.indigoAccent[100]
                          : Colors.black54),
                )
              ],
            ),
          ),
          position: TimelineItemPosition.right,
          isLast: track == orderTracks.last,
          isFirst: track == orderTracks.first,
          iconBackground:
          track == orderTracks.last ? Colors.blueAccent : Colors.green,
          icon: Icon(
            Icons.blur_circular,
            color:
            track == orderTracks.last ? Colors.transparent : Colors.white,
          ),
        ),
      );
    });

    return items;
  }

  void _startTimer() {
    // Start the periodic timer which prints something every 1 seconds
    _timer = new Timer.periodic(new Duration(seconds: 1), (time) {
      var orderDate = _order.date.toDate();
      var minutes = orderDate.difference(DateTime.now()).inMinutes;
      print(minutes);
      print(_time);
      if (minutes < 0) {
        setState(() {
          _time = 'RETRASO';
        });
      } else {
        setState(() {
          var minLabel = minutes == 1 ? 'minuto' : 'minutos';
          minLabel = minutes <= 0 ? '' : 'minutos';
          _time = '${minutes} ${minLabel}';
        });
      }
    });
  }
}
