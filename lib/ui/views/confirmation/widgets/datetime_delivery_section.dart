import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_picker/flutter_picker.dart';
import '../../../../core/models/stores.dart';
import '../../../../core/viewmodels/order_model.dart';
import '../../../../core/viewmodels/store_model.dart';

class ConfirmationDateTimeDeliverySection extends StatefulWidget {
  ConfirmationDateTimeDeliverySection();

  @override
  _ConfirmationDateTimeDeliverySectionState createState() =>
      _ConfirmationDateTimeDeliverySectionState();
}

class _ConfirmationDateTimeDeliverySectionState
    extends State<ConfirmationDateTimeDeliverySection> {
  String hourSelectedLabel = '';
  DateTime hourSelected;
  List<String> pickerData;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    StoreModel storeProvider = Provider.of<StoreModel>(context);
    OrderModel orderProvider = Provider.of<OrderModel>(context);
    if (pickerData == null) {
      pickerData = _fillPicker(storeProvider, orderProvider);
      //print(pickerData);
    }

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    orderProvider.order.type == 'delivery'
                        ? 'Hora de entrega'
                        : 'Hora de recogida',
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
                  ),
                  Text(
                    hourSelectedLabel,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: Colors.green),
                  ),
                ],
              ),
            ),
            storeProvider.selectedStore.schedule['mode'] == 'programmed'
                ? FlatButton(
                    onPressed: () {
                      //_settingModal(context);
                      _showPicker(context);
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.red),
                    ),
                    child: Text('Cambiar'),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  _showPicker(BuildContext context) {
    return Picker(
        adapter: PickerDataAdapter(pickerdata: pickerData),
        changeToFirst: false,
        textAlign: TextAlign.center,
        cancelTextStyle: TextStyle(fontSize: 16, color: Colors.red),
        confirmTextStyle: TextStyle(fontSize: 16, color: Colors.green),
        textStyle: TextStyle(
            color: Colors.black87,
            fontSize: 16,
            fontWeight: FontWeight.normal,
            fontFamily: "Poppins"),
        selectedTextStyle: TextStyle(
            color: Colors.green,
            fontSize: 22,
            fontWeight: FontWeight.w700,
            fontFamily: "Poppins"),
        columnPadding: const EdgeInsets.all(8.0),
        onConfirm: (Picker picker, List value) {
          hourSelectedLabel = 'Aproximadamente a las ${picker.getSelectedValues()[0]}h';
          setState(() {});
        }).showModal(this.context);
  }

  List<String> _fillPicker(StoreModel storeProvider, OrderModel orderProvider) {
    List<TimeOfDay> data = [];
    Store store = storeProvider.selectedStore;
    int preparationTime;
    if (orderProvider.order.type == 'delivery') {
      preparationTime = store.orderTypes.delivery.preparationTime;
    } else if (orderProvider.order.type == 'collect') {
      preparationTime = store.orderTypes.collect.preparationTime;
    } else if (orderProvider.order.type == 'in_store') {
      preparationTime = store.orderTypes.inStore.preparationTime;
    }

    var now = new DateTime.now();
    //var now = new DateFormat("yyyy-MM-dd HH:mm:ss").parse('2020-07-28 00:00:00');
    var estimatedDeliveryHour = now.add(Duration(minutes: preparationTime));
    var scheduleDay = store.schedule['days'][now.weekday - 1];

    DateTime hoursRange1Opening = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
        '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_1']['opening_time']}:00');
    DateTime hoursRange1Closing = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
        '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_1']['closing_time']}:00');

    var startTime1 = TimeOfDay(
        hour: hoursRange1Opening.hour, minute: hoursRange1Opening.minute);
    var endTime1 = TimeOfDay(
        hour: hoursRange1Closing.hour, minute: hoursRange1Closing.minute);
    var step = Duration(minutes: 15);
    var startTime2;
    var endTime2;

    final List<TimeOfDay> times1 = getTimes(startTime1, endTime1, step)
        .toList();

    List<TimeOfDay> times2 = [];
    if (scheduleDay['hours_range_2']['opening_time'] != "") {
      DateTime hoursRange2Opening = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
          '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_2']['opening_time']}:00');
      DateTime hoursRange2Closing = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
          '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_2']['closing_time']}:00');
      startTime2 = TimeOfDay(
          hour: hoursRange2Opening.hour, minute: hoursRange2Opening.minute);
      endTime2 = TimeOfDay(
          hour: hoursRange2Closing.hour, minute: hoursRange2Closing.minute);
      times2 = getTimes(startTime2, endTime2, step).toList();
    }

    final rangeTimes = [...times1, ...times2];

    List<String> dataMap = [];

    if (storeProvider.selectedStore.schedule['mode'] == 'manual' ||
        storeProvider.selectedStore.schedule['mode'] == 'limited') {
      //Modo limitado y manual
      print('modo manula y limitado');

      hourSelected = estimatedDeliveryHour;
      dataMap = ['${estimatedDeliveryHour.hour}:${estimatedDeliveryHour.minute}'];

    } else {
      //Modo programado

      rangeTimes.forEach((time) {
        var date = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
            '${estimatedDeliveryHour.year}-${estimatedDeliveryHour.month}-${estimatedDeliveryHour.day} ${time.hour}:${time.minute}:00');
        if (estimatedDeliveryHour.isBefore(date)) {
          dataMap.add('${date.hour}:${date.minute.toString().padLeft(2, '0')}');
        }
      });
      hourSelected = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
          '${estimatedDeliveryHour.year}-${estimatedDeliveryHour.month}-${estimatedDeliveryHour.day} ${dataMap[0]}:00');
    }

    hourSelectedLabel = 'Aproximadamente a las ${dataMap[0]}h';
    orderProvider.order.date = Timestamp.fromDate(hourSelected);
    orderProvider.order.dateLabel = hourSelectedLabel;

    print('Hora de pedido: $now');
    print('Hora de entrega prevista: $estimatedDeliveryHour');
    print('Hora de entrega: ${orderProvider.order.date.toDate()}');

    return dataMap;
  }

  Iterable<TimeOfDay> getTimes(
      TimeOfDay startTime, TimeOfDay endTime, Duration step) sync* {
    var hour = startTime.hour;
    var minute = startTime.minute;

    do {
      yield TimeOfDay(hour: hour, minute: minute);
      minute += step.inMinutes;
      while (minute >= 60) {
        minute -= 60;
        hour++;
      }
    } while (hour < endTime.hour ||
        (hour == endTime.hour && minute <= endTime.minute));
  }
}
