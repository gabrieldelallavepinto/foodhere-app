import 'package:flutter/material.dart';
import '../../../../core/models/order.dart';
import '../../../../ui/theme.dart';

class ConfirmationStoreSection extends StatelessWidget {
  final Order order;

  ConfirmationStoreSection({@required this.order});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    order.store.name,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: theme.primaryColor,
                        fontSize: 15),
                  ),
                  Text(
                    order.store.address.formattedAddress,
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 14,
                        color: Colors.black54),
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.phone,
                        color: theme.accentColor,
                        size: 16,
                      ),
                      Text(
                        '${order.store.phone}${order.store.optionalPhone !=
                            null && order.store.optionalPhone != ''
                            ? ' / ${order.store.optionalPhone}'
                            : ''}',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.black54),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.check, color: Colors.green),
          )
        ],
      ),
    );
  }
}
