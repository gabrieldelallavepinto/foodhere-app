import 'package:flutter/material.dart';
import 'package:tabitas/core/models/order.dart';
import 'package:tabitas/ui/theme.dart';

class ConfirmationUserSection extends StatelessWidget {
  final Order order;

  ConfirmationUserSection({@required this.order});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [

                Text(
                  'Cliente',
                  style: TextStyle(fontWeight: FontWeight.w700),
                ),

                Row(
                  children: [
                    Text(
                      '${order.client.firstName} ${order.client.lastName}',
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 12,
                          color: Colors.black54),
                    ),
                    SizedBox(width: 10,),
                    Icon(
                      Icons.phone,
                      size: 16,
                      color: theme.accentColor,
                    ),
                    Text(
                      order.client.phone,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 12,
                          color: Colors.black54),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.check, color: Colors.green),
          )
        ],
      ),
    );
  }
}
