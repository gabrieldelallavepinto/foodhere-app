import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../ui/theme.dart';
import '../../../../core/models/order.dart';

class ConfirmationAmountSection extends StatelessWidget {
  final Order order;

  ConfirmationAmountSection({@required this.order});

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("##0.00", "es_ES");

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Resumen',
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Subtotal',
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
                  ),
                ),
                Text(
                  _getFormattedSubtotal(),
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: Colors.black87),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Gastos de envío',
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
                  ),
                ),
                Text(
                  _getFormattedDeliveryPrice(),
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: Colors.black87),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Descuentos',
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
                  ),
                ),
                Text(
                  _getFormattedDiscounts(),
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: Colors.black87),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Total',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ),
                Text(
                  _getFormattedTotalPrice(),
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: theme.primaryColor),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String _getFormattedDeliveryPrice() {
    var f = new NumberFormat("##0.00", "es_ES");
    if (order.type == 'delivery') {
      Map<String, dynamic> zone = order.store.orderTypes.delivery.zones
          .firstWhere((element) =>
              element['postal_code'] == order.deliveryAddress.postalCode);
      if (zone != null) {
        if (zone['delivery_amount'] == 0) {
          return 'Gratis';
        } else {
          return '${f.format(_getZonePrice())}€';
        }
      }
    }

    return '${f.format(0)}€';
  }

  double _getZonePrice() {
    if (order.type == 'delivery') {
      Map<String, dynamic> zone = order.store.orderTypes.delivery.zones
          .firstWhere((element) =>
              element['postal_code'] == order.deliveryAddress.postalCode);
      return double.parse(zone['delivery_amount'].toString());
    } else {
      return 0.0;
    }
  }

  String _getFormattedTotalPrice() {
    var f = new NumberFormat("##0.00", "es_ES");
    double total = order.totalAmount + _getZonePrice() + _getDiscounts();
    return '${f.format(total)}€';
  }

  String _getFormattedSubtotal() {
    var f = new NumberFormat("##0.00", "es_ES");
    return '${f.format(order.totalAmount)}€';
  }

  String _getFormattedDiscounts() {
    var f = new NumberFormat("##0.00", "es_ES");
    return '${f.format(_getDiscounts())}€';
  }

  double _getDiscounts() {
    double discounts = 0;
    return discounts;
  }
}
