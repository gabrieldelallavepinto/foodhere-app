import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import '../../../../core/models/order.dart';
import '../../../../core/models/payment_methods.dart';
import '../../../../core/viewmodels/business_model.dart';
import '../../../../ui/theme.dart';

class ConfirmationPaymentMethodSection extends StatefulWidget {

  final Order order;
  final Function onSelectPaymentMethod;

  ConfirmationPaymentMethodSection({@required this.order, @required this.onSelectPaymentMethod} );

  @override
  _ConfirmationPaymentMethodSectionState createState() => _ConfirmationPaymentMethodSectionState();
}

class _ConfirmationPaymentMethodSectionState extends State<ConfirmationPaymentMethodSection> {

  String _value;
  PaymentMethods _paymentMethods;
  dynamic _selectedPaymentMethod;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    BusinessModel businessProvider = Provider.of<BusinessModel>(context);
    _paymentMethods = businessProvider.paymentMethods;
    if (_selectedPaymentMethod == null) {
      setState(() {
        _value = 'cash';
        _selectedPaymentMethod = _paymentMethods.cash;
        widget.onSelectPaymentMethod(_selectedPaymentMethod);
      });
    }

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Elige como quieres pagar',
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
            ),
            Text(
              _selectedPaymentMethod.description,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 14
              ),
            ),
            DropdownButton(
              underline: Container(),
              value: _value,
              elevation: 2,
              isExpanded: true,
              items: _getAvailablePaymentMethods(order: widget.order),
              onChanged: (value) {
                setState(() {
                  _value = value;
                  switch (value) {
                    case 'cash':
                      _selectedPaymentMethod = _paymentMethods.cash;
                      break;
                    case 'paypal':
                      _selectedPaymentMethod = _paymentMethods.paypal;
                      break;
                    case 'stripe':
                      _selectedPaymentMethod = _paymentMethods.stripe;
                      break;
                    case 'dataphone':
                      _selectedPaymentMethod = _paymentMethods.dataphone;
                      break;
                  }

                  widget.onSelectPaymentMethod(_selectedPaymentMethod);

                });
              },
            )
          ],
        ),
      ),
    );
  }

  List<DropdownMenuItem> _getAvailablePaymentMethods({Order order}) {

    if (order.store.orderTypes.delivery.paymentMethods == null ) {
      return [];
    }
    List<DropdownMenuItem> items = [];
    order.store.orderTypes.delivery.paymentMethods.forEach((key) {
      dynamic data;
      if (key as String == 'stripe') {
        data = {
          'payment_method': _paymentMethods.stripe,
          'icon': SimpleLineIcons.credit_card
        };
      } else if (key.toString() == 'cash') {
        data = {
          'payment_method': _paymentMethods.cash,
          'icon': SimpleLineIcons.wallet
        };
      } else if (key.toString() == 'dataphone') {
        data = {
          'payment_method': _paymentMethods.dataphone,
          'icon': SimpleLineIcons.credit_card
        };
      } else if (key.toString() == 'paypal') {
        data = {
          'payment_method': _paymentMethods.paypal,
          'icon': SimpleLineIcons.paypal
        };
      }
      items.add(DropdownMenuItem(
        value: data['payment_method'].id,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Icon(
                data['icon'],
                color: theme.primaryColor,
              ),
              SizedBox(
                width: 5,
              ),
              Text(data['payment_method'].label),
            ],
          ),
        ),
      ));
    });

    return items;

  }
}
