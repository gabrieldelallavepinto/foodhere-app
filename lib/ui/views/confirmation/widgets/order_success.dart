import 'package:flutter/material.dart';
import 'package:tabitas/core/models/order.dart';
import 'package:tabitas/ui/theme.dart';

class OrderSuccess extends StatefulWidget {
  final Order order;
  final Function onTapCheckStatus;
  final Function onTapContinue;

  OrderSuccess(
      {@required this.order,
      @required this.onTapCheckStatus,
      @required this.onTapContinue});

  @override
  _OrderSuccessState createState() => _OrderSuccessState();
}

class _OrderSuccessState extends State<OrderSuccess> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: Image.asset(widget.order.type == 'delivery'
                ? 'assets/images/delivery_success.png'
                : 'assets/images/collection_success.png'),
          ),
          Text(
            '¡Gracias por tu pedido!',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: theme.primaryColor,
                fontSize: 22,
                fontWeight: FontWeight.w700),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Puedes hacer seguimiento del estado de tu pedido desde la sección pedidos.',
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.7,
            child: FlatButton(
              child: Text('Ver estado del pedido'),
              textColor: Colors.white,
              color: theme.primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
              onPressed: () => _onTapCheckStatus(),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.7,
            child: FlatButton(
              child: Text('Aceptar'),
              textColor: Colors.black87,
              color: Colors.black12,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(18.0),
              ),
              onPressed: () => _onTapContinue(),
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  _onTapContinue() {
    widget.onTapContinue();
  }

  _onTapCheckStatus() {
    widget.onTapCheckStatus();
  }
}
