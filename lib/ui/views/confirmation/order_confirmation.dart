import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../../core/models/client.dart';
import '../../../core/models/payment_methods.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/userViewModel.dart';
import '../../../ui/theme.dart';
import './widgets/amount_section.dart';
import './widgets/datetime_delivery_section.dart';
import './widgets/delivery_section.dart';
import './widgets/order_success.dart';
import './widgets/payment_method_section.dart';
import './widgets/store_section.dart';

class OrderConfirmation extends StatefulWidget {
  @override
  _OrderConfirmationState createState() => _OrderConfirmationState();
}

class _OrderConfirmationState extends State<OrderConfirmation> {
  OrderModel _orderProvider;
  Client _user;
  bool _saved = false;

  @override
  Widget build(BuildContext context) {
    OrderModel orderProvider = Provider.of<OrderModel>(context);
    UserViewModel userProvider = Provider.of<UserViewModel>(context);
    _user = userProvider.userProfile;
    _orderProvider = orderProvider;

    return Scaffold(
        appBar: AppBar(
          title: Text('Confirmación'),
        ),
        body: ModalProgressHUD(
          inAsyncCall: _saved,
          dismissible: false,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      orderProvider.order.type == 'delivery'
                          ? SizedBox()
                          : ConfirmationStoreSection(
                              order: orderProvider.order,
                            ),
                      orderProvider.order.type == 'delivery'
                          ? ConfirmationDeliverySection(
                              order: orderProvider.order,
                            )
                          : SizedBox(),
                      ConfirmationDateTimeDeliverySection(),
                      ConfirmationPaymentMethodSection(
                        order: orderProvider.order,
                        onSelectPaymentMethod: _onSelectPaymentMethod,
                      ),
                      ConfirmationAmountSection(
                        order: orderProvider.order,
                      ),
                    ],
                  ),
                )),
              ),
              SafeArea(
                child: FlatButton(
                  child: Text(
                    'Confirmar pedido',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: theme.accentColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0),
                    side: BorderSide(color: theme.accentColor),
                  ),
                  onPressed: () {
                    setState(() {
                      _saved = true;
                    });
                    _setDeliveryData();
                    orderProvider.order.storeId = orderProvider.order.store.id;
                    orderProvider.addOrder().then((_) {
                      showModalBottomSheet(
                        context: context,
                        builder: (_) => OrderSuccess(
                          order: orderProvider.order,
                          onTapCheckStatus: _onTapCheckStatus,
                          onTapContinue: _onTapContinue,
                        ),
                        isDismissible: false,
                        isScrollControlled: true,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(36),
                              topRight: Radius.circular(36)),
                        ),
                      );
                      setState(() {
                        _saved = false;
                      });
                    });
                  },
                ),
              )
            ],
          ),
        ));
  }

  _onSelectPaymentMethod(dynamic paymentMethod) {
    var pm;
    switch (paymentMethod.id) {
      case 'cash':
        pm = paymentMethod as CashPaymentMethod;
        break;
      case 'paypal':
        pm = paymentMethod as PaypalPaymentMethod;
        break;
      case 'stripe':
        pm = paymentMethod as StripePaymentMethod;
        break;
      case 'dataphone':
        pm = paymentMethod as DataphonePaymentMethod;
        break;
    }
    _orderProvider.order.payments = [];
    _orderProvider.order.payments.add(pm.toJson());
    print(_orderProvider.order.payments[0]);
  }

  _onTapContinue() {
    _orderProvider.cancelOrder().then((value) {
      Navigator.pushNamedAndRemoveUntil(context, 'home', (_) => false,
          arguments: _user);
    });
  }

  _onTapCheckStatus() {
    Navigator.pushNamedAndRemoveUntil(context, 'trackingOrder', (_) => false,
        arguments: {'order': _orderProvider.order, 'fromConfirmation': true});
  }

  _setDeliveryData() {
    if (_orderProvider.order.type == 'delivery') {
      Map<String, dynamic> zone = _orderProvider.order.store.orderTypes.delivery.zones.firstWhere((element) => element['postal_code'] == _orderProvider.order.deliveryAddress.postalCode);
      _orderProvider.order.deliveryAmount = double.parse(zone['delivery_amount'].toString());
    }
  }
}
