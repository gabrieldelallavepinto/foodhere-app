import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/theme.dart';

class QuantityStepper extends StatefulWidget {
  final Function onChange;
  final int initialQuantity;
  final int minQuantity;
  final int maxQuantity;
  final TextStyle quantityTextStyle;
  final double sizeButtons;
  final bool disabled;
  final bool disabledIncrement;

  QuantityStepper({
    @required this.onChange,
    @required this.initialQuantity,
    @required this.minQuantity,
    @required this.maxQuantity,
    this.disabled = false,
    this.disabledIncrement = false,
    this.quantityTextStyle,
    this.sizeButtons = 40,
  });

  @override
  _QuantityStepperState createState() => _QuantityStepperState();
}

class _QuantityStepperState extends State<QuantityStepper> {
  int quantity;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    quantity = widget.initialQuantity;
  }

  @override
  Widget build(BuildContext context) {

    final ProductModel productProvider = Provider.of<ProductModel>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          iconSize: widget.sizeButtons,
          disabledColor: Colors.red,
          icon: Icon(
              quantity > widget.minQuantity
                  ? widget.disabled == false ? Icons.remove_circle : Icons
                  .remove_circle_outline
                  : Icons.remove_circle_outline,
              color: quantity > widget.minQuantity
                  ? widget.disabled == false ? theme.accentColor : Colors
                  .black26
                  : Colors.black26),
          onPressed: quantity > widget.minQuantity
              ? () =>
          widget.disabledIncrement == false || widget.disabled == false
              ? _decrement(productProvider, -1)
              : null
              : null,
        ),
        Container(
          width: 10,
          child: Center(
            child: Text(
              quantity.toString(),
              style: widget.quantityTextStyle != null
                  ? widget.quantityTextStyle
                  : TextStyle(
                  fontSize: 24,
                  color: Colors.black54,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
        IconButton(
          iconSize: widget.sizeButtons,
          icon: Icon(
              quantity < widget.maxQuantity
                  ? widget.disabledIncrement == false ? Icons.add_circle : Icons
                  .add_circle_outline
                  : Icons.add_circle_outline,
              color: quantity < widget.maxQuantity
                  ? widget.disabledIncrement == false
                  ? theme.accentColor
                  : Colors.black26
                  : Colors.black26),
          onPressed: quantity < widget.maxQuantity
              ? () =>
          widget.disabledIncrement == false
              ? _increment(productProvider, 1)
              : null
              : null,
        ),
      ],
    );
  }

  /// Descrementa el contador de cantidad
  _decrement(ProductModel productProvider, int sign) {
    if (quantity == widget.minQuantity) return;
    setState(() {
      quantity -= 1;
      widget.onChange(quantity: quantity, sign: sign);
    });
  }

  /// Incrementa el contador de cantidad
  _increment(ProductModel productProvider, int sign) {
    if (quantity == widget.maxQuantity) return;
    setState(() {
      quantity += 1;
      widget.onChange(quantity: quantity, sign: sign);
    });
  }
}
