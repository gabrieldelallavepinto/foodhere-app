import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/order_line.dart';
import 'package:tabitas/core/viewmodels/order_model.dart';


class ShoppingCartBadge extends StatelessWidget {

  final Function onTap;
  final int number;

  const ShoppingCartBadge ({ Key key, @required this.onTap, @required this.number }): super(key: key);

  @override
  Widget build(BuildContext context) {

    var f = new NumberFormat("##0.00", "es_ES");
    OrderModel orderProvider = Provider.of<OrderModel>(context);

    return Padding(
      padding: EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text('${f.format(orderProvider.order.totalAmount)}€', style: TextStyle(fontWeight: FontWeight.w700)),
          Badge(
            position: BadgePosition.topRight(top: 0, right: 0),
            animationDuration: Duration(milliseconds: 300),
            animationType: BadgeAnimationType.slide,
            badgeColor: Colors.orangeAccent,
            showBadge: number > 0,
            badgeContent: Text(
              number.toString(),
              style: TextStyle(color: Colors.black87),
            ),
            child: IconButton(
                icon: Icon(Entypo.shopping_cart),
                onPressed: () => onTap()),
          ),
        ],
      ),
    );
  }
}
