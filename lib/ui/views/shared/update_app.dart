import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:open_appstore/open_appstore.dart';
import 'package:package_info/package_info.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tabitas/appConfig/appconfig.dart';
import 'package:tabitas/appConfig/constants.dart';
import 'package:tabitas/core/models/app_channel.dart';
import 'package:tabitas/core/services/app_version.dart';
import 'package:tabitas/ui/views/shared/do_not_ask_again_dialog.dart';

class UpdateApp extends StatefulWidget {
  final Widget child;

  UpdateApp({this.child});

  @override
  _UpdateAppState createState() => _UpdateAppState();
}

class _UpdateAppState extends State<UpdateApp> {
  @override
  void initState() {
    super.initState();
    checkLatestVersion(context);
  }

  checkLatestVersion(context) async {
    var doc = await AppVersion.getVersion();
    print('doc: ${doc.data}');
    AppChannel appChannel = AppChannel.fromMap(doc.data);

    Version minAppVersion = Version.parse(appChannel.minVersion);
    Version latestAppVersion = Version.parse(appChannel.version);

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    Version currentVersion = Version.parse(packageInfo.version);

    print(currentVersion);

    if (minAppVersion > currentVersion) {
      _showCompulsoryUpdateDialog(
        context,
        "Por favor, es necesario actualizar la app para poder continuar",
      );
    } else if (latestAppVersion > currentVersion) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      bool showUpdates = false;
      showUpdates = sharedPreferences.getBool(kUpdateDialogKeyName);
      if (showUpdates != null && showUpdates == false) {
        return;
      }

      _showOptionalUpdateDialog(
        context,
        "Ya tienes disponible nueva versión de la app",
      );
      print('Update available');
    } else {
      print('App is up to date');
    }
  }

  _showOptionalUpdateDialog(context, String message) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        String title = "Actualización disponible";
        String btnLabel = "Actualizar ahora";
        String btnLabelCancel = "Mas tarde";
        String btnLabelDontAskAgain = "No me preguntes de nuevo";
        return DoNotAskAgainDialog(
          kUpdateDialogKeyName,
          title,
          message,
          btnLabel,
          btnLabelCancel,
          _onUpdateNowClicked,
          doNotAskAgainText:
              Platform.isIOS ? btnLabelDontAskAgain : 'Nunca preguntar mas',
        );
      },
    );
  }

  _onUpdateNowClicked() async {
    print('On update app clicked');
    OpenAppstore.launch(androidAppId: "${AppConfig.androidAppId}&gl=ES", iOSAppId: AppConfig.iOSAppId);
  }

  _showCompulsoryUpdateDialog(context, String message) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "Actualización disponible";
        String btnLabel = "Actualizar ahora";
        return Platform.isIOS
            ? new CupertinoAlertDialog(
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(title),
                ),
                content: Text(message),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text(
                      btnLabel,
                    ),
                    isDefaultAction: true,
                    onPressed: _onUpdateNowClicked,
                  ),
                ],
              )
            : new AlertDialog(
                title: Text(
                  title,
                  style: TextStyle(fontSize: 22),
                ),
                content: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text(btnLabel),
                    onPressed: _onUpdateNowClicked,
                  ),
                ],
              );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
