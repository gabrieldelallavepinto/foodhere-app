import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'values/colors.dart';
import 'widgets/page_title.dart';
import 'widgets/page_body.dart';



class Start extends StatefulWidget {
  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  double showSkipButtom = 1.0;

  changePage(data) {
    setState(() {
      if (data == 2) {
        showSkipButtom = 0.0;
      } else {
        showSkipButtom = 1.0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // final channelSettingProvider = Provider.of<ChannelSettingModel>(context);

    return Container();
    /*
    return Scaffold(
        backgroundColor: ModuleColors.pageColor,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                  child: Opacity(
                opacity: showSkipButtom,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'home');
                  },
                  child: Text(
                    'Saltar >>',
                    style: TextStyle(
                        color: ModuleColors.skipTextColor,
                        fontWeight: FontWeight.normal),
                  ),
                ),
              )),
              Expanded(
                flex: 1,
                child: IntroductionScreen(
                  pages: generatePages(channelSettingProvider.channelSetting.startingPage),
                  onChange: (index) => changePage(index),
                  onDone: () => Navigator.pushNamed(context, 'home'),
                  onSkip: () => Navigator.pushNamed(context, 'home'),
                  done: Container(
                      width: double.infinity,
                      decoration: new BoxDecoration(
                          color: ModuleColors.primaryColor,
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Comenzar',
                            style: TextStyle(
                              color: ModuleColors.initTextButtonColor,
                            )),
                      )),
                  dotsDecorator: DotsDecorator(
                      size: Size.square(10.0),
                      activeSize: Size(40.0, 10.0),
                      activeColor: ModuleColors.dotActiveColor,
                      color: ModuleColors.dotColor,
                      spacing: EdgeInsets.symmetric(horizontal: 3.0),
                      activeShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0))),
                  showSkipButton: false,
                ),
              ),
            ],
          ),
        ));

     */
  }

  PageDecoration buildPageDecoration() {
    return PageDecoration(
        pageColor: ModuleColors.pageColor, bodyFlex: 7, imageFlex: 10);
  }

  List<PageViewModel> generatePages(List<dynamic> channelSetting) {

    final size = MediaQuery.of(context).size;
    List<PageViewModel> pages = [];

    channelSetting.forEach((element) {
      PageViewModel page = new PageViewModel(
        titleWidget: PageTitle(title: element['title'] ?? ''),
        bodyWidget: PageBody(message: element['message'] ?? ''),
        image: Center(
            child: Container(
          width: size.width * 0.7,
          child: CachedNetworkImage(
            placeholder: (context, url) => Container(
                width: 40,
                height: 40,
                child: CircularProgressIndicator(


                )
            ),
            imageUrl: element['image_url'],
          ),
        )),
        decoration: buildPageDecoration(),
      );
      pages.add(page);
    });

    return pages;
  }
}
