import 'dart:ui';
import 'package:flutter/material.dart';

class ModuleColors {
  static const Color primaryColor = Color.fromARGB(255, 229, 30, 37);
  static const Color secondaryColor = Color.fromARGB(255, 0, 152, 77);
  static const Color accentColor = Color.fromARGB(255, 115, 115, 115);
  static const Color initTextButtonColor = Color.fromARGB(255, 255, 255, 255);
  static const Color PageBodyText = Colors.black45;
  static const Color PageTitleText = Colors.black87;
  static const Color skipTextColor = Colors.black87;
  static const Color pageColor = Colors.white;
  static const Color dotColor = Colors.black26;
  static const Color dotActiveColor = Color.fromARGB(255, 0, 152, 77);
}