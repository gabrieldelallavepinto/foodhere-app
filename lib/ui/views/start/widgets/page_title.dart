import 'package:flutter/material.dart';
import '../values/colors.dart';

class PageTitle extends StatelessWidget {

  final String title;

  PageTitle({this.title});

  @override
  Widget build(BuildContext context) {
    return Text(title,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.w800,
            height: 1.0,
            fontSize: 35.0,
            color: ModuleColors.PageTitleText)
    );
  }
}
