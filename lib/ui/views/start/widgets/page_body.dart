import 'package:flutter/material.dart';
import '../values/colors.dart';


class PageBody extends StatelessWidget {

  final String message;

  PageBody({this.message});

  @override
  Widget build(BuildContext context) {
    return Text(message,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 16.0,
            color: ModuleColors.PageBodyText)
    );
  }
}
