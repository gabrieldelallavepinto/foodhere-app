import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/userViewModel.dart';
import '../../../ui/theme.dart';
import '../../../ui/views/shopping_cart/widgets/empty_message.dart';
import '../../../ui/views/shopping_cart/widgets/order_line_item.dart';

class ShoppingCart extends StatefulWidget {
  @override
  _ShoppingCartState createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {
  @override
  Widget build(BuildContext context) {
    final orderProvider = Provider.of<OrderModel>(context);
    final userProvider = Provider.of<UserViewModel>(context);
    var f = new NumberFormat("##0.00", "es_ES");

    _onTapItemDelete(int index) {
      setState(() {
        orderProvider.removeOrderLine(index: index);
      });
    }

    _decrement(int index) {
      if (orderProvider.order.orderLines[index].quantity == 1) {
        return;
      }
      setState(() {
        orderProvider.updateQuantity(index, -1);
      });
    }

    _increment(int index) {
      if (orderProvider.order.orderLines[index].quantity == 10) {
        return;
      }
      setState(() {
        orderProvider.updateQuantity(index, 1);
      });
    }

    Future<bool> _cancelOrder() {
      return showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Tabita\'s'),
              content: Text('¿Estas seguro que quieres cancelar su pedido?'),
              actions: <Widget>[
                FlatButton(
                  child: Text("NO"),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                FlatButton(
                  child: Text("SI"),
                  onPressed: () {
                    orderProvider.cancelOrder().then((value) {
                      Navigator.pushNamedAndRemoveUntil(
                          context, 'home', (_) => false,
                          arguments: userProvider.userProfile);
                    });
                  },
                ),
              ],
            ),
          ) ??
          false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Tu selección'),
        actions: <Widget>[
          FlatButton.icon(
            onPressed: () => _cancelOrder(),
            icon: Icon(
              Icons.clear,
              color: Colors.white,
            ),
            label: Text(
              'Anular',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      body: orderProvider.order.orderLines != null &&
              orderProvider.order.orderLines.isNotEmpty
          ? ListView.builder(
              itemCount: orderProvider.order.orderLines.length,
              itemBuilder: (buildContext, index) => OrderLineItem(
                line: orderProvider.order.orderLines[index],
                onTapItemDelete: _onTapItemDelete,
                onIncrementQuantity: _increment,
                onDecrementQuantity: _decrement,
                index: index,
              ),
            )
          : ShoppingCartEmptyMessage(
              msg: 'No se han añadido productos en el carrito'),
      bottomNavigationBar: BottomAppBar(
        elevation: 2,
        color: theme.accentColor,
        child: Container(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                orderProvider.order.orderLines != null &&
                        orderProvider.order.orderLines.isNotEmpty
                    ? FlatButton(
                        onPressed: () {
                          Navigator.pushNamed(context, 'confirmation');
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: Icon(Icons.shopping_cart,
                                  size: 16, color: Colors.white),
                            ),
                            Text(
                              'Pasar por caja',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      )
                    : Spacer(),
                Text(
                  '${f.format(orderProvider.order.totalAmount)} €',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
