import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class ShoppingCartEmptyMessage extends StatelessWidget {
  final String msg;

  ShoppingCartEmptyMessage({@required this.msg});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              SimpleLineIcons.basket,
              size: 100,
              color: Colors.black12,
            ),
            SizedBox(
              height: 40,
            ),
            Text(
              msg,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black26,
                  fontWeight: FontWeight.w400,
                  fontSize: 16),
            )
          ],
        ),
      ),
    );
  }
}
