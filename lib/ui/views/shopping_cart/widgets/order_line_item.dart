import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import '../../../../core/models/order_line.dart';
import '../../../../ui/theme.dart';

class OrderLineItem extends StatefulWidget {
  final OrderLine line;
  final int index;
  final Function onTapItemDelete;
  final Function onIncrementQuantity;
  final Function onDecrementQuantity;

  OrderLineItem({
    @required this.line,
    @required this.onTapItemDelete,
    @required this.onIncrementQuantity,
    @required this.onDecrementQuantity,
    @required this.index,
  });

  @override
  _OrderLineItemState createState() => _OrderLineItemState();
}

class _OrderLineItemState extends State<OrderLineItem> {
  var f = new NumberFormat("##0.00", "es_ES");
  bool showDetail = false;

  @override
  Widget build(BuildContext context) {
    //print(widget.line.childrenOrderLines[0].childrenOrderLines[0].toJson());
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
      child: Card(
          child: Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          '${widget.line.quantity} x ${widget.line.label}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              color: theme.accentColor,
                              fontSize: 17,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          showDetail == true
                              ? SimpleLineIcons.arrow_up
                              : SimpleLineIcons.arrow_down,
                          size: 20,
                          color: Colors.blueAccent,
                        ),
                        onPressed: () {
                          setState(() {
                            showDetail = !showDetail;
                          });
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          SimpleLineIcons.trash,
                          size: 20,
                          color: theme.primaryColor,
                        ),
                        onPressed: () => widget.onTapItemDelete(widget.index),
                      )
                    ],
                  ),
                ),
                showDetail == true
                    ? widget.line.type == 'PRODUCT_SIMPLE'
                    ? _generateOptions(orderLine: widget.line)
                    : _generateProducts()
                    : SizedBox(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Precio'),
                    Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: Text(
                        '${f.format(widget.line.totalAmount)} €',
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 20,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.black26,
                  height: 0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Cantidad'), _quantitySteeper()],
                ),
              ],
            ),
          )),
    );
  }

  Widget _quantitySteeper() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          iconSize: 30,
          disabledColor: Colors.red,
          icon: Icon(
              widget.line.quantity > 1
                  ? Icons.remove_circle
                  : Icons.remove_circle_outline,
              color: widget.line.quantity > 1
                  ? theme.accentColor
                  : Colors.black26),
          onPressed: widget.line.quantity > 1
              ? () => widget.onDecrementQuantity(widget.index)
              : null,
        ),
        Container(
          width: 30,
          child: Center(
            child: Text(
              widget.line.quantity.toString(),
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
        IconButton(
          iconSize: 30,
          icon: Icon(
              widget.line.quantity < 10
                  ? Icons.add_circle
                  : Icons.add_circle_outline,
              color: widget.line.quantity < 10
                  ? theme.accentColor
                  : Colors.black26),
          onPressed: widget.line.quantity < 10
              ? () => widget.onIncrementQuantity(widget.index)
              : null,
        ),
      ],
    );
  }

  Widget _generateProducts() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _generateProduct(),
    );
  }

  Widget _generateOptions({OrderLine orderLine}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _generateModifier(orderLine: orderLine),
    );
  }

  List<Widget> _generateProduct() {
    List<Widget> widgets = [];
    widget.line.childrenOrderLines.forEach((product) {
      widgets.add(Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            product.label,
            style: TextStyle(
                fontWeight: FontWeight.w700, color: theme.primaryColor),
          ),
          _generateOptions(orderLine: product)
        ],
      ));
    });

    return widgets;
  }

  List<Widget> _generateModifier({OrderLine orderLine}) {
    List<Widget> widgets = [];

    orderLine.childrenOrderLines?.forEach((modifierOrderLine) {
      widgets.add(
          _composeOptions(options: modifierOrderLine.childrenOrderLines) == ''
              ? SizedBox()
              : Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  '${modifierOrderLine.label}:',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                      color: Colors.black87),
                ),
              ),
              Expanded(
                child: Text(
                  _composeOptions(
                      options: modifierOrderLine.childrenOrderLines),
                  style: TextStyle(fontSize: 13, color: Colors.black54),
                ),
              )
            ],
          ));
    });
    return widgets;
  }

  String _composeOptions({List<OrderLine> options}) {
    if (options == null) return '';
    String result = '';
    int counter = options.length;
    options.forEach((option) {
      if (option.modifierShowSelectedOptions == true &&
          option.modifierShowUnselectedOptions == false) {
        String optionItem = '';
        if (option.totalAmount == 0) {
          optionItem = option.quantity > 1
              ? '${option.quantity} x ${option.label.toUpperCase()}'
              : '${option.label.toUpperCase()}';
        } else {
          optionItem = option.quantity > 1
              ? '${option.quantity} x ${option.label.toUpperCase()}(${f.format(option.totalAmount)}€)'
              : '${option.label.toUpperCase()}(${f.format(option.totalAmount)}€)';
        }
        counter--;
        if (counter > 0) {
          optionItem += ', ';
        }
        result += optionItem;
      }
    });
    int counterUnselected = options.length;
    options.forEach((option) {
      if (option.modifierShowUnselectedOptions == true) {
        print(option.toJson());
        if (option.quantity == 0) {
          result += 'SIN ${option.label.toUpperCase()}';
          counterUnselected--;
          if (counterUnselected > 0) {
            result += ', ';
          }
        }
      }
    });

    print(result);
    return result;
  }
}
