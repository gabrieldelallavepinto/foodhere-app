import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../../../core/models/link.dart';

class DocView extends StatelessWidget {
  final DataLinkConfig linkConfig;

  DocView(this.linkConfig);

  @override
  Widget build(BuildContext context) {
    String html = this.linkConfig.url;
    return WebviewScaffold(
      url: html,
      appBar: AppBar(title: Text(this.linkConfig.title)),
      withZoom: this.linkConfig.zoom,
      hidden: true,
    );
  }
}
