import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tabitas/core/models/stripe_card.dart';
import 'package:tabitas/ui/theme.dart';

class CardItem extends StatelessWidget {
  final StripeCard card;
  final Function onItemTap;
  final Function onItemDelete;

  CardItem({@required this.card, @required this.onItemTap, @required this.onItemDelete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => _onItemTap(),
      child: Row(
        children: [
          IconButton(
            onPressed: () {},
            icon: Icon(card.selected == true? Icons.check_box : Icons.check_box_outline_blank, color: theme.accentColor,),
          ),
          SizedBox(
            width: 20,
          ),
          Image.asset(
            'assets/images/${card.card['brand']}.png',
            width: 40,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text('**** ${card.card['last4']}     ${card.card['expMonth']}/${card.card['expYear']}'),
          ),
          FlatButton(
            onPressed: () => _onItemDelete(),
            child: Icon(Icons.remove_circle, color: Colors.red,),
          )
        ],
      ),
    );
  }

  _onItemTap() {
    onItemTap(card);
  }

  _onItemDelete() {
    onItemDelete(card);
  }
}
