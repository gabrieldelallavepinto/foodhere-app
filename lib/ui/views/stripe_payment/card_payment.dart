import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:tabitas/appConfig/appconfig.dart';
import 'package:tabitas/appConfig/constants.dart';
import 'package:tabitas/core/models/client.dart';
import 'package:tabitas/core/models/stripe_card.dart';
import 'package:tabitas/core/services/payment_service.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import 'package:tabitas/ui/theme.dart';
import 'package:tabitas/ui/views/stripe_payment/card_item.dart';
import 'package:http/http.dart' as http;

class CardPayment extends StatefulWidget {

  @override
  _CardPaymentState createState() => _CardPaymentState();
}

class _CardPaymentState extends State<CardPayment> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  String defaultCardId;
  List<StripeCard> stripeCards = [];
  String _paymentMethodId;
  String _uid;

  void setError(dynamic error) {
      //Handle your errors
  }

  @override
  void initState() {
    // TODO: implement initState

    StripePayment.setOptions(
      StripeOptions(
          publishableKey: "pk_test_NaE90fKI3tH17QPGgWGxEy8e",
          merchantId: "acct_1B8W5fHEJroi6sYw",
          androidPayMode: "test"),
    );

    _getCardDefault();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final UserViewModel userProvider = Provider.of<UserViewModel>(context);
    _uid = userProvider.user.uid;
    _onItemTap(StripeCard card) async {
      String cardId = stripeCards[stripeCards.indexOf(card)].id;
      _saveCardDefault(cardId);
      setState(() {
        defaultCardId = cardId;
        print(defaultCardId);
      });
    }

    _onItemDelete(StripeCard card) {
      PaymentService().deleteCard(card.id, userProvider.user.uid);
      _removeCardDefault();
      setState(() {
        defaultCardId = null;
      });

    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Pago con tarjeta'),
        actions: [
          FlatButton.icon(
            onPressed: () {
              _createPaymentMethod(null, _uid, 'some_order_id');
            },
            icon: Icon(Icons.add, color: Colors.white),
            label: Text('Nueva', style: TextStyle(color: Colors.white)),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: stripeCards.isNotEmpty ? Text(
                'Selecciona una método de pago',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: theme.accentColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 16),
              ) : Column(
                children: [
                  Icon(SimpleLineIcons.credit_card, size: 60, color: Colors.black26,),
                  SizedBox(height: 10,),
                  Text(
                    'No hay métodos de pago disponibles',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.black26,
                        fontWeight: FontWeight.normal,
                        fontSize: 14),
                  ),
                ],
              ),
            ),
            Expanded(
              child: StreamBuilder(
                  stream: userProvider.fetchStripeCardsAsStream(),
                  builder: (context, AsyncSnapshot<QuerySnapshot> snapshot)  {
                    if (snapshot.hasData) {
                      List<StripeCard> cards = snapshot.data.documents
                          .map((doc) => StripeCard.fromMap(doc.data))
                          .toList();

                      if (cards.isNotEmpty) {
                        if (defaultCardId != null) {

                          StripeCard selectedCard;
                          cards.forEach((element) {
                            if (element.id == defaultCardId) {
                              selectedCard = element;
                            }
                          });
                          if (selectedCard != null) {
                            cards[cards.indexOf(selectedCard)].selected = true;
                          } else {
                            cards[0].selected = true;
                            _saveCardDefault(cards[0].id);
                            defaultCardId = cards[0].id;
                          }
                        } else {
                          cards[0].selected = true;
                          _saveCardDefault(cards[0].id);
                          defaultCardId = cards[0].id;
                        }
                      }
                      stripeCards = cards;

                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.separated(
                          separatorBuilder: (context, index) => Divider(
                            color: Colors.black26,
                          ),
                          itemCount: stripeCards.length,
                          itemBuilder: (buildContext, index) => CardItem(
                            card: stripeCards[index],
                            onItemTap: _onItemTap,
                            onItemDelete: _onItemDelete,
                          ),
                        ),
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }),
            ),
            stripeCards.isEmpty || defaultCardId == null ? SizedBox() : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child : FlatButton(
                onPressed: () {
                  _createPayment(userProvider);
                },
                color: theme.accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                  side: BorderSide(color: theme.accentColor),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.check, color: Colors.white),
                          SizedBox(
                            width: 10,
                          ),
                          Text('Realizar pago',
                              style: TextStyle(color: Colors.white))
                        ],
                      ),
                      Text(
                        '16,00€',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _createPaymentMethod(
      String paymentMethodId, String uid, String orderId) async {
    PaymentMethod paymentMethod = PaymentMethod();
    if (paymentMethodId == null) {
      paymentMethod = await StripePayment.paymentRequestWithCardForm(
        CardFormPaymentRequest(),
      ).then((PaymentMethod paymentMethod) {
        print(paymentMethod.toJson());
        _paymentMethodId = paymentMethod.id;
        PaymentService().addCard(paymentMethod: paymentMethod.toJson(), paymentMethodId: paymentMethod.id, uid: uid, orderId: orderId );
        return paymentMethod;
      }).catchError(setError);
    }
  }

  Future<void> _createPayment(UserViewModel userProvider) async {
    _paymentMethodId = defaultCardId;
    final response = await http.post(kUrlStripePaymentCreateIntent,
        headers: <String, String>{
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: jsonEncode({
          "payment_method_id": _paymentMethodId,
          "business_id": AppConfig.businessId,
          "order_id": 'some_order_id',  //TODO obtener el order id
          "client_id": _uid,
        }));

    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);

      StripePayment.confirmPaymentIntent(
        PaymentIntent(
          clientSecret: json['intent']['client_secret'],
          paymentMethodId: _paymentMethodId,
        ),
      ).then((PaymentIntentResult paymentIntentResult) async {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text('Received ${paymentIntentResult.paymentIntentId}')));

        if (paymentIntentResult.status == 'succeeded') {
          print('PAGO COMPLETADO');
          StripePayment.completeNativePayRequest();
        } else if (paymentIntentResult.status == 'processing') {
          StripePayment.completeNativePayRequest();
          print(
              'The payment is still in \'processing\' state. This is unusual. Please contact us');
        } else {
          StripePayment.cancelNativePayRequest();
          print(
              'There was an error to confirm the payment. Details: $paymentIntentResult.status');
        }
      }).catchError(setError);
    } else {
      throw Exception('Failed to load data');
    }
  }

  _saveCardDefault(String cardId) async {
    SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setString(kCardDefaultKeyName, cardId);

  }

  _getCardDefault() async {
    SharedPreferences sharedPreferences =
    await SharedPreferences.getInstance();
    defaultCardId = sharedPreferences.getString(kCardDefaultKeyName);
  }

  _removeCardDefault() async {
    SharedPreferences sharedPreferences =
    await SharedPreferences.getInstance();
    sharedPreferences.remove(kCardDefaultKeyName);
  }
}
