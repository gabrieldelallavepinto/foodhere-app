import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';
import 'package:tabitas/ui/theme.dart';
import 'package:intl/intl.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {

  final TextEditingController _firstNameController = new TextEditingController();
  final TextEditingController _lastNameController = new TextEditingController();
  final TextEditingController _phoneController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final TextEditingController _repeatPasswordController = new TextEditingController();

  final _formKeyProfile = GlobalKey<FormState>();
  final _keyProfile = GlobalKey<ScaffoldState>();

  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();

  bool _editing = false;
  bool _obscureText = true;

  String _firstName = '';
  String _lastName = '';
  String _phone = '';
  String _email = '';
  String _password = '';
  String _repeatPassword = '';
  DateTime _birthdate;

  @override
  void initState() {
    super.initState();

    _password = '';
    _repeatPassword = '';
    _birthdate = _birthdate != null
        ? _birthdate
        : null;
  }

  @override
  void dispose() {
    // other dispose methods
    _firstNameController.dispose();
    _lastNameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _repeatPasswordController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print('didchange');
    final userProvider = Provider.of<UserViewModel>(context);
    _firstName = userProvider.userProfile.firstName;
    _lastName = userProvider.userProfile.lastName;
    _phone = userProvider.userProfile.phone;
    _email = userProvider.userProfile.email;
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserViewModel>(context);

    return Scaffold(
      key: _keyProfile,
      appBar: AppBar(
        backgroundColor: theme.accentColor,
        centerTitle: true,
        title: Center(
          child: Text(
            'Tus datos',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        actions: <Widget>[
          _editing == false
              ? IconButton(
                  icon: Icon(SimpleLineIcons.pencil,
                      size: 20, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      _editing = true;
                    });
                  })
              : IconButton(
                  icon: Icon(Icons.check, size: 20, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      _editing = false;
                      _saveProfile(userProvider, context);
                    });
                  })
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKeyProfile,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.account_circle,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    _editing == false
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                            child: Text(
                              _firstName != '' ? '${_firstName}' : 'Queremos saber como te llamas',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 16.0),
                            ))
                        : Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                initialValue: _firstName,
                                textInputAction: TextInputAction.next,
                                autofocus: true,
                                keyboardType: TextInputType.text,
                                onChanged: (String value) {
                                  _firstName = value;
                                },
                                onFieldSubmitted: (term) {
                                  _firstNameFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(_lastNameFocus);
                                },

                                decoration: InputDecoration(
                                  labelText: 'Nombre',
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      SimpleLineIcons.user,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    _editing == false
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                            child: Text(
                              _lastName == ''
                                  ? 'Dinos tus apellidos'
                                  : '${_lastName}',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 16.0),
                            ))
                        : Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                textInputAction: TextInputAction.next,
                                initialValue: _lastName ?? '',
                                keyboardType: TextInputType.text,
                                onChanged: (String value) {
                                  _lastName = value;
                                },
                                onFieldSubmitted: (term) {
                                  _lastNameFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(_phoneFocus);
                                },
                                validator: (value) => (value.isEmpty)
                                    ? "Por favor introduce tus apellidos"
                                    : "Pruebaa",
                                decoration: InputDecoration(
                                  labelText: 'Apellidos',
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.phone,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    _editing == false
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                            child: Text(
                              '${_phone}',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 16.0),
                            )
                        )
                        : Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                initialValue: _phone ?? '',
                                focusNode: _phoneFocus,
                                keyboardType: TextInputType.phone,
                                onChanged: (String value) {
                                  _phone = value;
                                },
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term) {
                                  _phoneFocus.unfocus();
                                },
                                validator: (value) => (value.isEmpty)
                                    ? "Por favor introduce tu teléfono móvil"
                                    : null,
                                decoration: InputDecoration(
                                  labelText: 'Teléfono',
                                ),
                              ),
                            ),
                          ),
                    _editing == false ? Expanded(
                      child: Chip(
                        backgroundColor: Colors.blueAccent,
                        label: Text('Verificado', style: TextStyle(fontSize: 10.0, color: Colors.white),),
                      ),
                    ) : Container()
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.cake,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                          child: Text(
                            _birthdate == null
                                ? 'Tu fecha de cumpleaños'
                                : '${_formatDate(_birthdate)}',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.black54, fontSize: 16.0),
                          )),
                    ),
                    _editing == true
                        ? IconButton(
                            onPressed: () async {
                              DateTime newDateTime =
                                  await showRoundedDatePicker(
                                context: context,
                                locale: Locale("es", "ES"),
                                theme: ThemeData(primarySwatch: Colors.red),
                                initialDate: DateTime.now(),
                                firstDate: DateTime(DateTime.now().year - 100),
                                lastDate: DateTime.now(),
                                borderRadius: 16,
                                initialDatePickerMode: DatePickerMode.year,
                              );
                              if (newDateTime != null) {
                                setState(() => _birthdate = newDateTime);
                              }
                            },
                            icon: Icon(
                              Icons.calendar_today,
                              size: 30,
                              color: theme.accentColor,
                            ),
                          )
                        : SizedBox()
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: Text(
                  'Datos de acceso',
                  style: TextStyle(color: theme.primaryColor, fontSize: 18),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.email,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    _editing == false
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                            child: Text(
                              '${_email}',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 16.0),
                            ))
                        : Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                initialValue: _email ?? '',
                                focusNode: _emailFocus,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term) {
                                  _emailFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(_passwordFocus);
                                },
                                onChanged: (String value) {
                                  _email = value;
                                },
                                validator: (value) => (value.isEmpty)
                                    ? "Por favor introduce tu email"
                                    : null,
                                decoration: InputDecoration(
                                  labelText: 'Email',
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                decoration: _editing == true
                    ? null
                    : BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                                style: BorderStyle.solid))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.lock_outline,
                      size: 30,
                      color: theme.accentColor,
                    ),
                    _editing == false
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 0, 16),
                            child: Text(
                              '*****',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 16.0),
                            ))
                        : Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                obscureText: _obscureText,
                                controller: _passwordController,
                                focusNode: _passwordFocus,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (term) {
                                  _passwordFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(_repeatPasswordFocus);
                                },
                                onChanged: (String value) {
                                  _password = value;
                                },
                                validator: (value) => (value.isEmpty)
                                    ? "Por favor introduce nueva contraseña, mínimo 6 caracteres"
                                    : null,
                                decoration: InputDecoration(
                                  labelText: 'Nueva contraseña',
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
              _editing == true
                  ? Container(
                      padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
                      decoration: _editing == true
                          ? null
                          : BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: Colors.black12,
                                      width: 1.0,
                                      style: BorderStyle.solid))),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.lock_outline,
                            size: 30,
                            color: theme.accentColor,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                              child: TextFormField(
                                obscureText: _obscureText,
                                controller: _repeatPasswordController,
                                focusNode: _repeatPasswordFocus,
                                keyboardType: TextInputType.visiblePassword,
                                textInputAction: TextInputAction.next,
                                onChanged: (String value) {
                                  _repeatPassword = value;
                                },
                                onFieldSubmitted: (term) {
                                  _passwordFocus.unfocus();
                                },
                                validator: (value) => (value.isEmpty)
                                    ? "Por favor introduce tu contraseña, de mínimo 6 caractéres"
                                    : null,
                                decoration: InputDecoration(
                                    labelText: 'Repetir contraseña',
                                    suffixIcon: IconButton(
                                      icon: _obscureText == true
                                          ? Icon(Icons.visibility)
                                          : Icon(Icons.visibility_off),
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                    )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  String _formatDate(DateTime date) {
    setState(() => _birthdate = date);
    return DateFormat('dd/MM/yyyy').format(date);
  }

  _saveProfile(UserViewModel user, BuildContext context) {

    if (_formKeyProfile.currentState.validate()) {
      _formKeyProfile.currentState.save();
      user.saveClientProfile({
        'first_name': _firstName != '' ? _firstName : '',
        'last_name': _lastName != '' ? _lastName : '',
        'birthdate': _birthdate != null
            ? Timestamp.fromDate(_birthdate)
            : null,
      }).then((value) {
        print('updated client data!!');
        setState(() {
          print('save!!');
          _editing = false;
        });
      });
    } else {

    }
  }
}
