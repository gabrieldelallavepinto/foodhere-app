import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/userViewModel.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _email;
  TextEditingController _password;
  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();

  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _email = TextEditingController(text: "");
    _password = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserViewModel>(context);
    Size size = MediaQuery.of(context).size;

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background_home.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          color: Color.fromRGBO(0, 0, 0, 0.5),
        ),
        Scaffold(
          key: _key,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: Form(
            key: _formKey,
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      padding: EdgeInsets.only(bottom: 30),
                      child: Image.asset(
                        "assets/images/logo-white.png",
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(32.0, 16.0, 32.0, 4.0),
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30.0),
                              topRight: Radius.circular(30.0),
                              bottomLeft: Radius.circular(30.0),
                              bottomRight: Radius.circular(30.0)),
                        ),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              'Bienvenido',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 30.0),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                TextFormField(
                                  focusNode: _emailFocus,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  autofocus: false,
                                  controller: _email,
                                  validator: (value) => (value.isEmpty)
                                      ? "Por favor introduce un correo electrónico"
                                      : null,
                                  onFieldSubmitted: (value) {
                                    FocusScope.of(context).requestFocus(_passwordFocus);
                                  },
                                  decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.email),
                                      labelText: "Correo electrónico"),
                                ),
                                SizedBox(height: 10.0),
                                TextFormField(
                                  focusNode: _passwordFocus,
                                  keyboardType: TextInputType.visiblePassword,
                                  textInputAction: TextInputAction.next,
                                  controller: _password,
                                  obscureText: true,
                                  validator: (value) => (value.isEmpty)
                                      ? "Instroduce una contraseña"
                                      : null,
                                  onFieldSubmitted: (value) {
                                    _passwordFocus.unfocus();
                                  },

                                  decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.email),
                                      labelText: "Contraseña"),
                                ),
                                SizedBox(height: 10.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, 'remember');
                                      },
                                      child: Text('¿Olvaste tu contraseña?',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor)),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                user.status == Status.Authenticating
                                    ? Center(child: CircularProgressIndicator())
                                    : FlatButton(
                                        padding: EdgeInsets.all(16.0),
                                        child: Text(
                                          'Acceder',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color: Theme.of(context).accentColor,
                                        shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(10.0),
                                        ),
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            if (!await user.signIn(
                                                _email.text, _password.text)) {
                                              _key.currentState
                                                  .showSnackBar(SnackBar(
                                                content: Text(
                                                    "No ha sido posible acceder, compruebe sus credenciales"),
                                              ));
                                            } else {
                                              Navigator.pop(context);
                                            }
                                          }
                                        },
                                      ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('¿No tienes cuenta?'),
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, 'register');
                                      },
                                      child: Text('Registrar',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor)),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          ],
                        )),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
