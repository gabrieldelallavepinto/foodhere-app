import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/category.dart';
import 'package:tabitas/core/models/link.dart';
import 'package:tabitas/core/viewmodels/business_model.dart';
import 'package:tabitas/core/viewmodels/category_model.dart';
import '../../../../core/models/client.dart';
import '../../../../core/models/promotion.dart';
import '../../../../core/viewmodels/promotion_model.dart';
import '../../../../core/viewmodels/userViewModel.dart';
import '../../../../ui/theme.dart';

class HomeDrawerContent extends StatelessWidget {
  List<Category> categories;

  HomeDrawerContent(this.categories);

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> options = [];
    List<dynamic> promotions = [];
    final Size size = MediaQuery.of(context).size;
    final promotionsProvider = Provider.of<PromotionModel>(context);
    final businessProvider = Provider.of<BusinessModel>(context);
    final userProvider = Provider.of<UserViewModel>(context);
    final categoryProvider = Provider.of<CategoryModel>(context);
    Category promotionCategory;
    categoryProvider.categories.forEach((category) {
      if (category.type == 'promotions') {
        promotionCategory = category;
      }
    });

    String allergensUrl;
    String termsConditionsUrl;
    if (businessProvider.business != null) {
      if (businessProvider.business?.links?.length > 0) {
        businessProvider.business.links.forEach((link) {
          if (link.id == 'allergens') {
            allergensUrl = link.url;
          }

          if (link.id == 'termsConditions') {
            termsConditionsUrl = link.url;
          }
        });
      }
    }

    List<Map<String, dynamic>> optionsNoLogged = [
      promotionCategory != null
          ? {
              "title": promotionCategory.name,
              "icon": Icons.grade,
              "route": "promotions"
            }
          : null,
      {"title": "Nuestra carta", "icon": Icons.fastfood, "route": "categories"},
      //{"title": "Preferencias", "icon": Icons.settings, "route": "settings"},
      termsConditionsUrl != null
          ? {
              "title": "Términos y condiciones",
              "icon": Icons.insert_drive_file,
              "route": "termsConditions"
            }
          : null,
      allergensUrl != null
          ? {
              "title": "Información alergenos",
              "icon": Icons.info,
              "route": "allergens"
            }
          : null,
      {"title": "Acceder", "icon": Icons.account_box, "route": "login"}
    ];

    List<Map<String, dynamic>> optionsLogged = [
      promotionCategory != null
          ? {
              "title": promotionCategory.name,
              "icon": Icons.grade,
              "route": "promotions"
            }
          : null,
      {"title": "Nuestra carta", "icon": Icons.fastfood, "route": "categories"},
      {
        "title": "Mis direcciones",
        "icon": Icons.directions,
        "route": "deliveryAddresses"
      },
      {"title": "Mis pedidos", "icon": Icons.receipt, "route": "myOrders"},
      //{"title": "Preferencias", "icon": Icons.settings, "route": "settings"},
      termsConditionsUrl != null
          ? {
              "title": "Términos y condiciones",
              "icon": Icons.insert_drive_file,
              "route": "termsConditions"
            }
          : null,
      allergensUrl != null
          ? {
              "title": "Información alergenos",
              "icon": Icons.info,
              "route": "allergens"
            }
          : null,
    ];

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(top: 10, left: 20),
          child: StreamBuilder(
              stream: promotionsProvider.fetchPromotionsAsStream(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  promotions = snapshot.data.documents;
                  print(promotions);
                  promotions.map((doc) => Promotion.fromMap(doc.data)).toList();

                  if (userProvider.userProfile == null) {
                    options = optionsNoLogged;
                  } else {
                    options = optionsLogged;
                  }

                  if (promotions.length == 0) {
                    options.removeAt(0);
                  }

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      userProvider.userProfile == null
                          ? Container(
                              padding: EdgeInsets.only(top: 40.0),
                              width: size.width * 0.4,
                              child: Image.asset(
                                "assets/images/logo.png",
                                fit: BoxFit.fill,
                                color: Colors.white,
                              ),
                            )
                          : _userInfoWidget(userProvider.userProfile, context),
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: ListView.builder(
                            itemBuilder: (context, position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                  if (options[position]['route'] ==
                                      'deliveryAddresses') {
                                    Navigator.pushNamed(
                                        context, options[position]['route'],
                                        arguments: true);
                                  } else if (options[position]['route'] ==
                                      'promotions') {
                                    Navigator.pushNamed(
                                        context, options[position]['route'],
                                        arguments: promotionCategory);
                                  } else if (options[position]['route'] ==
                                      'termsConditions') {
                                    Navigator.pushNamed(context, 'docView',
                                        arguments: new DataLinkConfig(
                                            title: 'Términos y condiciones',
                                            url: termsConditionsUrl,
                                            zoom: false));
                                  } else if (options[position]['route'] ==
                                      'allergens') {
                                    Navigator.pushNamed(context, 'docView',
                                        arguments: new DataLinkConfig(
                                            title: 'Información Alergenos',
                                            url: allergensUrl,
                                            zoom: true));
                                  } else {
                                    Navigator.pushNamed(
                                        context, options[position]['route']);
                                  }
                                },
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        options[position]['icon'],
                                        color: Colors.white70,
                                        size: 30.0,
                                      ),
                                    ),
                                    Text(
                                      options[position]['title'],
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600),
                                    )
                                  ],
                                ),
                              );
                            },
                            itemCount: options.length,
                          ),
                        ),
                      ),
                      Text(
                        'Versión 1.0 build 10',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      ),
                      Text(
                        'Powered by FoodHere Platform 2020',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      )
                    ],
                  );
                } else {
                  return Text('');
                }
              }),
        ),
      ),
    );
  }

  Widget _userInfoWidget(Client userProfile, BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 30, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            maxRadius: 30,
            minRadius: 30,
            backgroundColor: Colors.white,
            backgroundImage: userProfile.avatarUrl != null
                ? NetworkImage(userProfile.avatarUrl)
                : null,
            child: Text(
              userProfile.avatarUrl == null
                  ? _formatName(userProfile: userProfile, avatar: true)
                  : '',
              style: TextStyle(
                  color: theme.accentColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 20),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _formatName(userProfile: userProfile),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      '${userProfile.email}',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 11,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, 'register');
              },
              color: Colors.white70,
            ),
          ),
        ],
      ),
    );
  }

  String _formatName({Client userProfile, bool avatar = false}) {
    String fn = '';
    String ln = '';

    if (userProfile.firstName == '' || userProfile.firstName == null) {
      fn = avatar == true ? '' : 'Sin nombre';
    } else {
      fn = avatar == true
          ? userProfile.firstName[0].toUpperCase()
          : userProfile.firstName.toUpperCase();
    }

    if (userProfile.lastName == '' || userProfile.lastName == null) {
      ln = '';
    } else {
      ln = avatar == true
          ? userProfile.lastName[0].toUpperCase()
          : userProfile.lastName.toUpperCase();
    }

    return avatar == true ? '$fn$ln' : '$fn $ln';
  }
}
