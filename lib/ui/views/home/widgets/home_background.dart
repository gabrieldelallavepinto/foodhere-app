import 'package:flutter/material.dart';
import 'package:tabitas/ui/views/home/values.dart';

class HomeBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Expanded(
          flex: 6,
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Container(
                  child: CustomPaint(
                    size: Size(size.width, 400),
                    painter: CurvePainter2(),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  child: CustomPaint(
                    size: Size(size.width, 300),
                    painter: CurvePainter1(),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  child: CustomPaint(
                    size: Size(size.width, 200),
                    painter: CurvePainter(),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            color: ModuleColors.lowerBgColorStrip,
          ),
        )
      ],
    );
  }


}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = ModuleColors.lowerBgColorStrip;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, 200);
    path.quadraticBezierTo(size.width / 2, size.height / 1.6, size.width, 120);
    path.lineTo(size.width, 200);
    path.lineTo(0, 200);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvePainter1 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = ModuleColors.middleBgColorStrip;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, 280);
    path.quadraticBezierTo(size.width / 2, size.height / 2.0, size.width, 100);
    path.lineTo(size.width, 300);
    path.lineTo(0, 300);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvePainter2 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = ModuleColors.upperBgColorStrip;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, 360);
    path.quadraticBezierTo(size.width / 2, size.height / 2.2, size.width, 120);
    path.lineTo(size.width, 400);
    path.lineTo(0, 400);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
