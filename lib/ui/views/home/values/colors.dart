import 'dart:ui';
import 'package:flutter/material.dart';

class ModuleColors {

// Panel
  static const Color storeSelectorBackground = Color.fromARGB(255, 194, 0, 0);
  static const Color storeSelectorPanelBackground = Colors.white;
  static const Color textSelectorColor = Colors.white;
  static const Color changeButtonBgColor = Colors.white;

  //Slider menu
  static const Color menuButton = Colors.black87;
  static const Color splashSliderMenuButton = Colors.red;

  //Options menu
  static const Color menuOptionsSeparatorColor = Color.fromRGBO(0, 152, 77, 1.0);
  static const Color menuOptionsTextColor = Color.fromRGBO(0, 152, 77, 1.0);

  //Alert no active stores
  static const Color alertNoActiveStoresBackgroundColor = Colors.red;
  static const Color alertNoActiveStoresTextColor = Colors.white;

  //Drawer
  static const Color drawerColorTransition = Colors.black54;
  static const Color drawerBackgroundColor = Color.fromARGB(255, 194, 0, 0);

  //Shopping button
  static const Color shoppingButtonBgColor = Color.fromRGBO(0, 152, 77, 1.0);
  static const Color iconShoppingColor = Colors.white;

  //Background design
  static const Color pageColor = Colors.white;
  static const Color upperBgColorStrip = Color.fromRGBO(245, 245, 245, 1.0);
  static const Color middleBgColorStrip = Color.fromRGBO(245, 245, 245, 1.0);
  static const Color lowerBgColorStrip = Color.fromRGBO(235, 235, 235, 1.0);

}
