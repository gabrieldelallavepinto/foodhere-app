import 'package:flutter_inner_drawer/inner_drawer.dart';

class ModuleConfig {

  // Background
  static const bool darkMode = false;
  static const String backgroundType = 'solid'; // solid, radial, linear, asset
  // Logo
  static const double logoWidth = 200.0;
  //Spinner
  static const String SpinnerType = 'ChasingDots';
  static const double SpinnerSize = 40.0;
  //Drawer
  static const double drawerBorderRadius = 40.0;
  static const InnerDrawerAnimation drawerLeftAnimationType = InnerDrawerAnimation.quadratic;
  static const InnerDrawerAnimation drawerRightAnimationType = InnerDrawerAnimation.quadratic;
  static const IDOffset drawerOffset = IDOffset.only(top: 0.0, left: 0.6);
  static const IDOffset drawerScale = IDOffset.horizontal(0.8);

}