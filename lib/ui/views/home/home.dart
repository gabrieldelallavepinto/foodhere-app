import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:tabitas/appConfig/appconfig.dart';
import 'package:tabitas/core/viewmodels/category_model.dart';

import '../../../core/models/client.dart';
import '../../../core/models/promotion.dart';
import '../../../core/services/promotion_service.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/promotion_model.dart';
import '../../../core/viewmodels/userViewModel.dart';
import '../../../core/viewmodels/store_model.dart';
import 'widgets/home_drawer_content.dart';
import '../store_select/store_selector.dart';
import 'widgets/home_background.dart';
import 'values.dart';

class Home extends StatefulWidget {
  final Client user;

  const Home({this.user});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<InnerDrawerState> _innerDrawerKey =
      GlobalKey<InnerDrawerState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    PromotionModel promotionProvider = Provider.of<PromotionModel>(context);
    StoreModel storeProvider = Provider.of<StoreModel>(context);
    OrderModel orderProvider = Provider.of<OrderModel>(context);
    UserViewModel userProvider = Provider.of<UserViewModel>(context);
    CategoryModel categoryProvider = Provider.of<CategoryModel>(context);

    List<dynamic> promotions = [];

    return InnerDrawer(
      key: _innerDrawerKey,
      onTapClose: true,
      swipe: true,
      colorTransitionChild: ModuleColors.drawerColorTransition,
      offset: ModuleConfig.drawerOffset,
      scale: ModuleConfig.drawerScale,
      proportionalChildArea: true,
      borderRadius: ModuleConfig.drawerBorderRadius,
      leftAnimationType: InnerDrawerAnimation.quadratic,
      backgroundDecoration:
          BoxDecoration(color: ModuleColors.drawerBackgroundColor),
      leftChild: HomeDrawerContent(categoryProvider.categories),
      scaffold: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: _showHilighted(promotionProvider)
              ? Center(
                  child: Container(
                    width: size.width * 0.25,
                    child: Image.asset(
                      "assets/images/logo.png",
                      fit: BoxFit.fill,
                    ),
                  ),
                )
              : SizedBox(width: size.width * 0.25),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: storeProvider.selectedStore == null
              ? SizedBox()
              : IconButton(
                  icon: Icon(SimpleLineIcons.menu,
                      size: 20, color: ModuleColors.menuButton),
                  onPressed: () {
                    _toggle();
                  }),
          actions: <Widget>[
            IconButton(
                icon: Icon(SimpleLineIcons.menu,
                    size: 20, color: Colors.transparent),
                onPressed: null)
          ],
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[
            HomeBackground(),
            Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: StreamBuilder(
                          stream: promotionProvider
                              .fetchHighlightedPromotionsAsStream(),
                          builder: (context, snapshot) {
                            print(snapshot.connectionState);
                            if (snapshot.connectionState !=
                                ConnectionState.active) {
                              return _buildSwiper(promotions, size);
                            } else {
                              if (snapshot.hasData) {
                                promotions = snapshot.data.documents
                                    .map((doc) => Promotion.fromMap(doc.data))
                                    .toList();

                                return _buildSwiper(promotions, size);
                              } else {
                                return Center(
                                  child: Container(
                                    width: size.width * 0.6,
                                    child: Image.asset(
                                      "assets/images/logo.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                );
                              }
                            }
                          },
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: storeProvider.selectedStore == null
                            ? Chip(
                                label:
                                    Text('ERROR: No existen tiendas activas'),
                                backgroundColor: ModuleColors
                                    .alertNoActiveStoresBackgroundColor,
                                labelStyle: TextStyle(
                                    color: ModuleColors
                                        .alertNoActiveStoresTextColor),
                              )
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FlatButton(
                                    onPressed: () {
                                      _actionOpenCarta(orderProvider);
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: ModuleColors
                                              .menuOptionsTextColor),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16.0),
                                      child: Text(
                                        'Nuestra carta',
                                        style: TextStyle(
                                          color:
                                              ModuleColors.menuOptionsTextColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () => _actionInitOrder(
                                        storeProvider,
                                        orderProvider,
                                        userProvider),
                                    color: ModuleColors.menuOptionsTextColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: ModuleColors
                                              .menuOptionsTextColor),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16.0),
                                      child: Text(
                                        'INICIAR PEDIDO',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ),
                    ],
                  ),
                ),
                storeProvider.getNumberAvailableStores() > 1
                    ? StoreSelector(
                        onTapChangeStore: _onTapChangeStore,
                      )
                    : SizedBox()
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _toggle() {
    _innerDrawerKey.currentState.toggle(direction: InnerDrawerDirection.start);
  }

  _actionOpenCarta(OrderModel orderProvider) {
    orderProvider.order = null;
    Navigator.pushNamed(context, 'categories');
  }

  _actionInitOrder(StoreModel storeProvider, OrderModel orderProvider,
      UserViewModel userProvider) {
    if (userProvider.user != null) {
      if (storeProvider.selectedStore.status == 'opened') {
        var now = new DateTime.now();
        var store = storeProvider.selectedStore;
        if (storeProvider.selectedStore.schedule['mode'] == 'manual' || storeProvider.selectedStore.schedule['mode'] ==
            'programmed') {
          _initOrder(orderProvider, userProvider);
        } else if (storeProvider.selectedStore.schedule['mode'] == 'limited') {
          var scheduleDay = store.schedule['days'][now.weekday - 1];
          DateTime hoursRange1Opening = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(
              '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_1']['opening_time']}:00');
          DateTime hoursRange1Closing = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(
              '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_1']['closing_time']}:00');
          if (scheduleDay['hours_range_2']['opening_time'] != "") {
            DateTime hoursRange2Opening = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(
                '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_2']['opening_time']}:00');
            DateTime hoursRange2Closing = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(
                '${now.year}-${now.month}-${now.day} ${scheduleDay['hours_range_2']['closing_time']}:00');
            if (scheduleDay['status'] == 'opened') {
              if (now.isAfter(hoursRange1Opening) &&
                  now.isBefore(hoursRange1Closing) ||
                  now.isAfter(hoursRange2Opening) &&
                      now.isBefore(hoursRange2Closing)) {
                _initOrder(orderProvider, userProvider);
              } else {
                _showAlertStoreClosed(storeProvider);
              }
            } else {
              _showAlertStoreClosed(storeProvider);
            }
          } else {
            if (scheduleDay['status'] == 'opened') {
              if (now.isAfter(hoursRange1Opening) &&
                  now.isBefore(hoursRange1Closing)) {
                _initOrder(orderProvider, userProvider);
              } else {
                _showAlertStoreClosed(storeProvider);
              }
            } else {
              _showAlertStoreClosed(storeProvider);
            }
          }
        }
      } else if (storeProvider.selectedStore.status == 'closed') {
        // Show alert, closed store
        _showAlertStoreClosed(storeProvider);
      } else if (storeProvider.selectedStore.status == 'paused') {
        //Show alert, store paused
        _showAlertStorePaused(storeProvider);
      }
    } else {
      Navigator.pushNamed(context, 'login');
    }
  }

  _onTapChangeStore() {
    Navigator.pushNamed(context, 'StoreSelector',
        arguments: {'cancelOrder': false, 'inital': true}).then((value) {
      setState(() {});
    });
  }

  bool _showHilighted(PromotionModel promotionProvider) {
    if (promotionProvider.promotions != null &&
        promotionProvider.promotions.isNotEmpty) {
      var promotions = promotionProvider.promotions
          .where((element) => element.highlighted == true)
          .toList();
      return promotions.length > 0;
    } else {
      return false;
    }
  }

  _showAlertStoreClosed(StoreModel storeProvider) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
              '${AppConfig.businessName}\n${storeProvider.selectedStore.name}'),
          content: Text(
              'Lo sentimos la tienda está en estos momentos cerrada, el horario es ${storeProvider.selectedStore.scheduleLabel}'),
          actions: [
            FlatButton(
              child: Text('ACEPTAR'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _showAlertStorePaused(StoreModel storeProvider) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
              '${AppConfig.businessName}\n${storeProvider.selectedStore.name}'),
          content: Text(
              'Lo sentimos en estos momentos no podemos atender su pedido, pruebe un poco mas tarde, discculpen las molestias. \nGracias'),
          actions: [
            FlatButton(
              child: Text('ACEPTAR'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _initOrder(OrderModel orderProvider, UserViewModel userProvider) {
    print('initOrder');
    orderProvider.initOrder(userProvider.userProfile);
    Navigator.pushNamed(context, 'typeOrder', arguments: false)
        .then((value) => print(value));
  }

  Widget _buildSwiper(List<dynamic> promotions, Size size) {
    if (promotions.isEmpty) {
      return Center(
          child: Container(
        width: size.width * 0.6,
        child: Image.asset(
          "assets/images/logo.png",
          fit: BoxFit.fill,
        ),
      ));
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Swiper(
        index: 0,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              print(promotions[index].name);
            },
            child: Container(
              child: CachedNetworkImage(
                imageUrl:
                    PromotionService.getPromoHighlighted(promotions[index]),
                placeholder: (context, url) => Container(
                  padding: EdgeInsets.all(10.0),
                  width: double.infinity,
                  child: SpinKitCircle(
                    color: Colors.greenAccent,
                    size: 40,
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          );
        },
        autoplay: true,
        autoplayDelay: 5000,
        itemCount: promotions.length,
        viewportFraction: 0.8,
        scale: 0.9,
        itemWidth: 300.0,
        itemHeight: 400.0,
        layout: SwiperLayout.DEFAULT,
        pagination: SwiperPagination(),
      ),
    );
  }
}
