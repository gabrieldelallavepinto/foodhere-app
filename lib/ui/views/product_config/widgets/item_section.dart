import 'package:flutter/material.dart';
import '../../../../core/models/modifier.dart';
import '../../../../core/models/product_section.dart';
import '../../../../ui/theme.dart';
import '../../../../ui/views/product_config/widgets/chip_option.dart';
import '../../../../ui/views/product_config/widgets/chip_option_inverse.dart';

class ItemSection extends StatefulWidget {
  final Function onTapSelect;
  final ProductSection section;

  ItemSection({@required this.onTapSelect, @required this.section});

  @override
  _ItemSectionState createState() => _ItemSectionState();
}

class _ItemSectionState extends State<ItemSection> {
  @override
  void initState() {
    if (widget.section.products.length == 1) {}
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding:
            const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0, bottom: 16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Text(
                      widget.section.name,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: theme.accentColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                  ),
                  widget.section.selected?.name != null
                      ? Text(
                          widget.section.selected.name,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w700),
                        )
                      : Text(
                          'Ningún producto seleccionado',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black54,
                          ),
                        ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, right: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _createModifierOptions(),
                    ),
                  )
                ],
              ),
            ),
            widget.section.selected.modifiers != null || widget.section.products.length > 1 ? FlatButton(
              child: Text(
                widget.section.selected?.name != null
                    ? widget.section.products.length == 1
                    ? 'Configurar'
                    : 'Cambiar'
                    : 'Seleccionar',
                style: TextStyle(fontSize: 12),
              ),
              textColor: theme.primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: theme.primaryColor)),
              onPressed: () => _onTapSelect(),
            ): SizedBox(),
          ],
        ),
      ),
    );
  }

  void _onTapSelect() {
    widget.onTapSelect(section: widget.section);
  }

  List<Widget> _createModifierOptions() {
    List<Widget> widgets = [];
    if (widget.section.selected == null) {
      return widgets;
    }
    widget.section.selected?.modifiers?.forEach((modifier) {
      widgets.add(Wrap(
        direction: Axis.horizontal,
        spacing: 5,
        crossAxisAlignment: WrapCrossAlignment.start,
        children: _createSelectedOptions(modifier),
      ));
    });
    return widgets;
  }

  List<Widget> _createSelectedOptions(Modifier modifier) {
    List<Widget> options = [];
    int quantityCount = modifier.maxFreeOptions;
    modifier.options.forEach((opt) {
      if (opt.active == true) {
        if (modifier.showSelectedOptions == true) {
          if (opt.quantity > 0) {
            int c = 0;
            for (var i = opt.quantity; i > 0; i--) {
              if (quantityCount - i >= 0) {
                options.add(ChipOption(
                    option: opt, free: quantityCount >= 0, quantity: i));
                quantityCount = quantityCount - i;
                break;
              } else {
                c++;
              }
            }
            if (c > 0) {
              quantityCount = quantityCount - c;
              options.add(
                  ChipOption(
                      option: opt, free: quantityCount >= 0, quantity: c));
            }
          }
        }
      }
    });

    if (modifier.showUnselectedOptions == true) {
      modifier.options.forEach((opt) {
        if (opt.active == true) {
          if (opt.quantity == 0) {
            options.add(ChipOptionInverse(option: opt));
          }
        }
      });
    }
    return options;
  }
}
