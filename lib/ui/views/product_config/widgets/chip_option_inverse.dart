import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tabitas/core/models/modifier_option.dart';
import 'package:tabitas/ui/theme.dart';

class ChipOptionInverse extends StatelessWidget {

  final ModifierOption option;



  ChipOptionInverse({this.option});

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("##0.00", "es_ES");
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(bottom:8.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.orange
          ),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              'sin ${option.name}',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),


        ),
      ),
    );
  }
}
