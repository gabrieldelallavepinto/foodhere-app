import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../../core/models/modifier.dart';
import '../../../../core/viewmodels/product_model.dart';
import '../../../../ui/theme.dart';

class ModifierDropdown extends StatefulWidget {

  final Modifier modifier;
  final int modifierIndex;
  final Function onSelect;

  ModifierDropdown({@required this.modifier, @required this.modifierIndex, @required this.onSelect});

  @override
  _ModifierDropdownState createState() => _ModifierDropdownState();
}

class _ModifierDropdownState extends State<ModifierDropdown> {

  var f = new NumberFormat("##0.00", "es_ES");
  int _value = 0;
  Modifier _modifier;

  @override
  Widget build(BuildContext context) {

    final ProductModel productProvider = Provider.of<ProductModel>(context);
    _modifier = productProvider.currentConfigProduct.modifiers
        .toList()[widget.modifierIndex];

    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                     _modifier.label,
                      style: TextStyle(
                          color: theme.accentColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16
                      ),
                    ),
                    _modifier.description != '' ? Text(_modifier.description) : SizedBox()
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: DropdownButton(
                underline: Container(),
                items: _createDropdownItems(_modifier.options, _modifier),
                onChanged: (value) {
                  setState(() {
                    _value = value;
                    widget.onSelect(value: _value, modifierIndex: widget.modifierIndex);
                  });
                },
                value: _value,
                elevation: 2,
                isExpanded: true,
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem> _createDropdownItems(dynamic options, Modifier modifier) {
    List<DropdownMenuItem> items = [];
    options.asMap().forEach((index, opt) {
      items.add(
        DropdownMenuItem(
          value: index,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(opt.name),
              opt.price > 0 && modifier.maxFreeOptions == 0
                  ? Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Text(opt.price > 0 ? '+${f.format(opt.price)}€' : '',
                    style: TextStyle(color: Colors.black87)),
              )
                  : SizedBox(),
            ],
          ),
        ),
      );
    });
    return items;
  }
}
