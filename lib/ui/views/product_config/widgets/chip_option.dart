import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tabitas/core/models/modifier_option.dart';
import 'package:tabitas/ui/theme.dart';

class ChipOption extends StatelessWidget {

  final ModifierOption option;
  final bool free;
  final int quantity;

  ChipOption({this.option, this.free, this.quantity});

  var f = new NumberFormat("##0.00", "es_ES");
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Badge(
        badgeColor: theme.primaryColor,
        shape: BadgeShape.circle,
        position: BadgePosition.topRight(top: -16, right: -7),
        borderRadius: 20,
        toAnimate: false,
        showBadge: quantity > 1,
        badgeContent:
            Text('$quantity', style: TextStyle(color: Colors.white)),
        child: Padding(
          padding: const EdgeInsets.only(bottom:8.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: free ? Colors.green : Colors.blueAccent[100]
            ),
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                _chipText(option),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
            ),


          ),
        ),
      ),
    );
  }

  _chipText(ModifierOption option) {
    String name = option.name;
    String price = option.price > 0 ? ' +${f.format(option.price * quantity)}€' : '';
    return free == true ? '$name' : '$name $price';
  }
}
