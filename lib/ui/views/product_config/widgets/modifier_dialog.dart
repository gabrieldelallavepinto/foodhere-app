import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/modifier.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/theme.dart';
import 'package:tabitas/ui/views/product_config/widgets/chip_option.dart';
import 'package:tabitas/ui/views/product_config/widgets/chip_option_inverse.dart';
import 'package:tabitas/ui/views/product_config/widgets/modal_dialog.dart';

class ModifierDialog extends StatefulWidget {
  final int modifierIndex;
  final Function onChange;

  ModifierDialog({@required this.modifierIndex, @required this.onChange});

  @override
  _ModifierDialogState createState() => _ModifierDialogState();
}

class _ModifierDialogState extends State<ModifierDialog> {
  int totalQuantity = 0; // TODO aplicar la cantidad correctament inicial
  bool disabled = false;
  var f = new NumberFormat("##0.00", "es_ES");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ProductModel productProvider = Provider.of<ProductModel>(context);
    Modifier modifier = productProvider.currentConfigProduct.modifiers
        .toList()[widget.modifierIndex];

    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        modifier.label,
                        style: TextStyle(
                            color: theme.accentColor,
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      ),
                      modifier.description != ''
                          ? Text(modifier.description)
                          : SizedBox()
                    ],
                  ),
                ),
                IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      _showDialog();
                    }),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Wrap(
              direction: Axis.horizontal,
              spacing: 5,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: _createSelectedOptions(modifier),
            ),
          )
        ],
      ),
    );
  }

  _showDialog() async {
    await showDialog(
        context: context,
        child: ModalDialog(
          modifierIndex: widget.modifierIndex,
          onChange: _onChange,
        ),
        barrierDismissible: false);
  }

  List<Widget> _createSelectedOptions(Modifier modifier) {
    List<Widget> options = [];
    int quantityCount = modifier.maxFreeOptions;
    modifier.options.forEach((opt) {
      if (opt.active == true) {
        if (modifier.showSelectedOptions == true) {
          if (opt.quantity > 0) {
            int c = 0;
            for (var i = opt.quantity; i > 0; i--) {
              if (quantityCount - i >= 0) {
                options.add(ChipOption(
                    option: opt, free: quantityCount >= 0, quantity: i));
                quantityCount = quantityCount - i;
                break;
              } else {
                c++;
              }
            }
            if (c > 0) {
              quantityCount = quantityCount - c;
              options.add(
                  ChipOption(option: opt, free: quantityCount >= 0, quantity: c));
            }
          }
        }
      }
    });

    if (modifier.showUnselectedOptions == true) {
      modifier.options.forEach((opt) {
        if (opt.active == true) {
          if (opt.quantity == 0) {
            options.add(ChipOptionInverse(option: opt));
          }
        }
      });
    }
    return options;
  }

  _onChange({int quantity, int optionIndex}) {
    widget.onChange(
        quantity: quantity,
        optionIndex: optionIndex,
        modifierIndex: widget.modifierIndex);
  }
}
