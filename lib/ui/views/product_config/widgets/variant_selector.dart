import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/variant.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/theme.dart';

class VariantSelector extends StatefulWidget {

  final List<Variant> variants;
  final Function onSelect;

  VariantSelector({this.variants, this.onSelect});

  @override
  _VariantSelectorState createState() => _VariantSelectorState();
}

class _VariantSelectorState extends State<VariantSelector> {

  var f = new NumberFormat("##0.00", "es_ES");
  int _value = 0;

  @override
  Widget build(BuildContext context) {

    final ProductModel productProvider = Provider.of<ProductModel>(context);

    return productProvider.currentConfigProduct.useVariants && widget.variants.length > 1 ? Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: Text(
              'Elige tamaño',
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: theme.accentColor,
                  fontWeight: FontWeight.w600,
                  fontSize: 16
              ),
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              child: DropdownButton(
                underline: Container(),
                items: _createDropdownItems(),
                onChanged: (value) {
                  setState(() {
                    _value = value;
                    widget.onSelect(value: _value);
                  });
                },
                value: _value,
                elevation: 2,
                isExpanded: true,
              ),
            ),
          ),
        ],
      ),
    ) : SizedBox();
  }

  List<DropdownMenuItem> _createDropdownItems() {
    List<DropdownMenuItem> items = [];
    widget.variants.where((variant) => variant.active == true).toList().asMap().forEach((index, variant) {
      items.add(
        DropdownMenuItem(
          value: index,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(variant.name),
                  Text(variant.description, style: TextStyle(fontSize: 12, color: Colors.black54), overflow: TextOverflow.ellipsis,)
                ],
              ),
              variant.price > 0
                  ? Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Text(variant.price > 0 ? '${f.format(variant.price)}€' : '',
                    style: TextStyle(color: Colors.black87)),
              )
                  : SizedBox(),
            ],
          ),
        ),
      );
    });
    return items;
  }
}
