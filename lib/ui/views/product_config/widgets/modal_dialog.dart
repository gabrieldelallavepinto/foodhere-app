import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../../core/models/modifier.dart';
import '../../../../core/models/modifier_option.dart';
import '../../../../core/viewmodels/product_model.dart';
import '../../../../ui/theme.dart';
import '../../../../ui/views/product_config/widgets/dialog_item.dart';

class ModalDialog extends StatefulWidget {
  final int modifierIndex;
  final Function onChange;

  ModalDialog({@required this.modifierIndex, @required this.onChange});

  @override
  _ModalDialogState createState() => _ModalDialogState();
}

class _ModalDialogState extends State<ModalDialog> {
  int _counter = 0;
  ProductModel _product;
  Modifier _modifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ProductModel productProvider = Provider.of<ProductModel>(context);
    print(productProvider.currentConfigProduct.modifiers);
    _modifier = productProvider.currentConfigProduct.modifiers
        .toList()[widget.modifierIndex];
    _product = productProvider;
    _counter = _getTotalQuantitySelected();

    return Padding(
      padding: const EdgeInsets.only(
          top: 16.0, bottom: 16.0, left: 16.0, right: 16.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: theme.primaryColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            productProvider
                                    .currentConfigProduct.modifiers.isNotEmpty
                                ? _modifier.label
                                : '',
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            productProvider
                                    .currentConfigProduct.modifiers.isNotEmpty
                                ? _modifier.description
                                : '',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.close, color: Colors.white),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
            Expanded(
                flex: 1,
                child:
                    Consumer<ProductModel>(builder: (context, product, child) {
                  return ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                            color: Colors.black26,
                            height: 0,
                          ),
                      itemCount: _modifier.options.length,
                      itemBuilder: (buildContext, index) {
                        ModifierOption option = _modifier.options[index];
                        return option.active == true ? DialogItem(
                            modifier: _modifier,
                            option: option,
                            optionIndex: index,
                            onChange: _onChange,
                            disabledIncrement: _counter ==
                                _modifier.maxOptions,
                            disabled: _counter ==
                                    _modifier.maxOptions &&
                                option.quantity == 0) : SizedBox();
                      });
                })),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                child: Text(
                  'Aceptar',
                  style: TextStyle(color: Colors.white),
                ),
                color: theme.primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(
                    color: theme.primaryColor,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  int _getTotalQuantitySelected() {
    int total = 0;
    if (_product.currentConfigProduct.modifiers.isNotEmpty) {
      _modifier.options
          .forEach((opt) {
        total += opt.quantity;
      });
    }

    return total;
  }

  _onChange({int quantity, int optionIndex, int sign}) {
    widget.onChange(quantity: quantity, optionIndex: optionIndex);
    setState(() {
      sign < 0 ? _counter-- : _counter++;
    });
  }
}
