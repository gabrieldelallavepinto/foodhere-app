import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:tabitas/core/models/modifier.dart';
import 'package:tabitas/core/models/modifier_option.dart';
import 'package:tabitas/ui/views/shared/quantity_stepper.dart';

class DialogItem extends StatefulWidget {
  final Modifier modifier;
  final ModifierOption option;
  final Function onChange;
  final int optionIndex;
  final bool disabled;
  final bool disabledIncrement;
  Uint8List _bytes;
  int _quantity;
  BuildContext c;

  DialogItem(
      {this.modifier,
      this.option,
      this.optionIndex,
      this.onChange,
      this.disabled = false,
      this.disabledIncrement = false});

  @override
  _DialogItemState createState() => _DialogItemState();
}

class _DialogItemState extends State<DialogItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.c = context;

    /*
    if (widget.option.icon != null) {
      widget._bytes =
          base64Decode(widget.option.icon.split('data:image/png;base64,')[1]);
    }
     */

    return Container(
      color: widget.option.quantity > 0 ? Colors.blue[50] : Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            widget.option.icon != null
                ? SizedBox()
                /*Image.memory(widget._bytes,
                    height: 28,
                    width: 28,
                    color: widget.disabled ? Colors.black12 : null) */
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                widget.option.name,
                style: TextStyle(
                    fontWeight: widget.option.quantity > 0
                        ? FontWeight.bold
                        : FontWeight.normal,
                    color: widget.disabled ? Colors.black12 : Colors.black87),
              ),
            ),
            Spacer(),
            QuantityStepper(
              initialQuantity: widget.option.quantity,
              minQuantity: widget.option.minQuantity,
              maxQuantity: widget.option.maxQuantity,
              sizeButtons: 30,
              disabled: widget.disabled,
              disabledIncrement: widget.disabledIncrement,
              onChange: _onChangeQuantity,
              quantityTextStyle: TextStyle(
                  fontSize: 20,
                  color: Colors.black54,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

  _onChangeQuantity({int quantity, int sign}) {
    print(quantity);
    print(sign);
    setState(() {
      widget.onChange(
          quantity: quantity, optionIndex: widget.optionIndex, sign: sign);
    });
  }
}
