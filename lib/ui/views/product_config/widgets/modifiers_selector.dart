import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import '../../../../core/models/modifier.dart';
import '../../../../ui/views/product_config/widgets/modifier_dialog.dart';
import '../../../../ui/views/product_config/widgets/modifier_dropdown.dart';
import '../../../../core/models/product.dart';


class ModifiersSelector extends StatefulWidget {
  final Product product;
  final Function onDropDownModifierSelected;
  final Function onDialogModifierSelected;

  ModifiersSelector(
      {@required this.product,
      @required this.onDropDownModifierSelected,
      @required this.onDialogModifierSelected});

  @override
  _ModifiersSelectorState createState() => _ModifiersSelectorState();
}

class _ModifiersSelectorState extends State<ModifiersSelector> {
  int mIndex;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ProductModel productProvider = Provider.of<ProductModel>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: _createModifiers(context),
    );
  }

  /// Crea el selector de Modificadores
  List<Widget> _createModifiers(BuildContext context) {
    List<Widget> widgets = [];

    widget.product.modifiers?.where((v) => v.active == true)?.forEach(
      (modifier) {
        int index = widget.product.modifiers
            .toList()
            .indexOf(modifier);
        mIndex = index;
        if (modifier.maxOptions == 1) {
          widgets.add(ModifierDropdown(
            modifier: modifier,
            modifierIndex: index,
            onSelect: _onSelectDropdown,
          ));
        } else {
          widgets.add(ModifierDialog(
            modifierIndex: index,
            onChange: _onSelectDialog,
          ));
        }
      },
    );
    return widgets;
  }

  _onSelectDropdown({int value, int modifierIndex}) {
    widget.onDropDownModifierSelected(
        value: value, modifierIndex: modifierIndex);
  }

  _onSelectDialog({int quantity, int optionIndex, int modifierIndex}) {
    widget.onDialogModifierSelected(
        quantity: quantity,
        optionIndex: optionIndex,
        modifierIndex: modifierIndex);
  }
}
