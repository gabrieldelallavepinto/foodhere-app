import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/views/shared/quantity_stepper.dart';

class HeaderProductConfig extends StatefulWidget {
  final Function onChangeQuantity;
  final double price;
  final bool setQuantity;

  HeaderProductConfig(
      {@required this.onChangeQuantity, @required this.price, this.setQuantity});

  @override
  _HeaderProductConfigState createState() => _HeaderProductConfigState();
}

class _HeaderProductConfigState extends State<HeaderProductConfig> {
  var f = new NumberFormat("##0.00", "es_ES");
  int _quantity;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _quantity = 1;

  }

  @override
  Widget build(BuildContext context) {

    return Column(
      children: [
        _createHeaderDescription(),
        SizedBox(height: 10),
        widget.setQuantity == true ? _createPriceAndQuantity() : SizedBox(),
        SizedBox(height: 20)
      ],
    );
  }

  /// Crea la cabecera conn Nombre y descripción
  Widget _createHeaderDescription() {

    final ProductModel productProvider = Provider.of<ProductModel>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                productProvider.currentConfigProduct.variants?.length == 1 ? '${productProvider.currentConfigProduct.name} ${productProvider.currentConfigProduct.variants[0].shortName}': productProvider.currentConfigProduct.name,
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.green,
                    fontSize: 24,
                    fontWeight: FontWeight.w800),
              ),
              Text(
                productProvider.currentConfigProduct.description,
                style: TextStyle(
                  color: Colors.black87,
                ),
              ),
            ],
          ),
        ),
        (productProvider.currentConfigProduct.isNew == true)
            ? Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: Image.asset(
                  'assets/images/icon_new.png',
                  width: 40,
                  height: 40,
                ),
              )
            : SizedBox(
                height: 0,
                width: 0,
              ),
      ],
    );
  }

  /// Crea el precio y el widget de cantidad
  Widget _createPriceAndQuantity() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          '${f.format(widget.price)} €',
          style: TextStyle(
              color: Colors.red, fontSize: 30, fontWeight: FontWeight.w700),
        ),
        QuantityStepper(
            initialQuantity: _quantity,
            minQuantity: 1,
            maxQuantity: 5,
            onChange: _onChangeQuantity
        ),
      ],
    );
  }

  _onChangeQuantity({int quantity, int sign}) {
    setState(() {
      _quantity = quantity;
      widget.onChangeQuantity(quantity: quantity, sign: sign);
    });
  }
}
