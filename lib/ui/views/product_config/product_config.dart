import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../core/models/modifier_option.dart';
import 'widgets/header_product_config.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/product_model.dart';
import '../../../core/models/product.dart';
import '../../../ui/theme.dart';
import '../../../ui/views/product_config/widgets/modifiers_selector.dart';
import '../../../ui/views/product_config/widgets/variant_selector.dart';
import '../../../ui/views/shared/shoppingCartBadge.dart';

class ProductConfig extends StatefulWidget {
  final DataProductConfig dataProductConfig;

  ProductConfig(this.dataProductConfig);

  @override
  _ProductConfigState createState() => _ProductConfigState();
}

class _ProductConfigState extends State<ProductConfig> {
  ProductModel _product;
  OrderModel _orderModel;
  int _quantity = 1;
  int _variantIndexSelected = 0;
  double _totalAmount = 0;
  bool _setQuantity;
  List<ModifierOption> recipe;

  final TextEditingController _commentController = new TextEditingController();
  String comment;
  var f = new NumberFormat("##0.00", "es_ES");

  @override
  void initState() {
    _setQuantity = widget.dataProductConfig.section == null;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final OrderModel orderProvider = Provider.of<OrderModel>(context);
    final ProductModel productProvider = Provider.of<ProductModel>(context);
    _product = productProvider;
    _orderModel = orderProvider;
    _product.currentConfigProduct = widget.dataProductConfig.product;

    _calculateTotalAmount();

    return Scaffold(
      appBar: AppBar(
        title: Text('Configurador'),
        centerTitle: true,
        actions: <Widget>[
          widget.dataProductConfig.section != null
              ? SizedBox()
              : ShoppingCartBadge(
                  onTap: _showShoppingCart,
                  number: _orderModel.counterOrderLines(),
                )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: theme.accentColor,
          label: Text(widget.dataProductConfig.section != null
              ? widget.dataProductConfig.section.products.length > 1 ? 'Seleccionar':'Aceptar'
              : 'Añadir'),
          icon: Icon(widget.dataProductConfig.section != null
              ? Icons.check
              : Icons.add),
          onPressed: () {
            if (widget.dataProductConfig.section != null) {
              _addProductToSelection(context: context);
            } else {
              _addProductToCart(
                  orderProvider: orderProvider,
                  product: _product.currentConfigProduct,
                  context: context);
            }
          }),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HeaderProductConfig(
                price: _totalAmount,
                onChangeQuantity: _onChangeQuantity,
                setQuantity: _setQuantity,
              ),
              _product.currentConfigProduct.variants != null &&
                      _product.currentConfigProduct.variants
                          .where((v) => v.active == true)
                          .toList()
                          .isNotEmpty
                  ? VariantSelector(
                      onSelect: _onSelectVariant,
                      variants: _product.currentConfigProduct.variants
                          .where((v) => v.active == true)
                          .toList(),
                    )
                  : SizedBox(),

              ModifiersSelector(
                  product: _product.currentConfigProduct,
                  onDropDownModifierSelected: _onDropDownModifierSelected,
                  onDialogModifierSelected: _onDialogModifierSelected),

              _product.currentConfigProduct.allowObservations == true
                  ? Card(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, left: 8.0, right: 8.0),
                            child: Text(
                              'Observaciones',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: theme.accentColor,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                              controller: _commentController,
                              maxLines: 5,
                              onChanged: (String value) {
                                comment = value;
                              },
                              decoration: InputDecoration.collapsed(
                                  hintText: "Introduce un comentario aquí"),
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }

  _selectComboVariant() {
    if (widget.dataProductConfig.section != null) {
      setState(() {
        _variantIndexSelected = 0;
        if (widget.dataProductConfig.variantId != null) {
          _product.currentConfigProduct.variants = _product
              .currentConfigProduct.variants
              .where((variant) => variant.active == true)
              .where(
                  (variant) => variant.id == widget.dataProductConfig.variantId)
              .toList();
          _product.currentConfigProduct.variants[0].selected = true;
          _product.currentConfigProduct.price =
              _product.currentConfigProduct.variants[0].price;
          _product.currentConfigProduct.modifiers =
              _product.currentConfigProduct.variants[0].modifiers;
          if (_product.currentConfigProduct.variants.length == 1) {
            _variantIndexSelected = 0;
          }
        } else {
          _product.currentConfigProduct.variants =
              _product.currentConfigProduct.variants;
        }
      });
    }
  }

  _calculateTotalAmount() {
    setState(() {
      _selectComboVariant();
      _setVariants(_variantIndexSelected);
      _setDropDownOptions();
      _totalAmount = _product.setTotalAmount(
          product: _product.currentConfigProduct, quantity: _quantity);
    });
  }

  double _setDropDownOptions() {
    return _product.initProductOptions();
  }

  _setVariants(int value) {
    _product.initVariantSelection(value);
  }

  _onSelectVariant({int value}) {
    setState(() {
      _variantIndexSelected = value;
    });
  }

  _onChangeQuantity({int quantity, int sign}) {
    setState(() {
      _quantity = quantity;
    });
  }

  _onDialogModifierSelected(
      {int quantity, int optionIndex, int modifierIndex}) {
    setState(() {
      _product.currentConfigProduct.modifiers
          ?.toList()[modifierIndex]
          .options[optionIndex]
          .quantity = quantity;
    });
  }

  _onDropDownModifierSelected({int value, int modifierIndex}) {
    setState(() {
      _product.setSelectedSingle(value: value, modifierIndex: modifierIndex);
    });
  }

  _showShoppingCart() {
    Navigator.pushNamed(context, 'shoppingCart').then((value) {
      setState(() {});
    });
  }

  void _addProductToCart(
      {OrderModel orderProvider, Product product, BuildContext context}) {
    setState(() {
      orderProvider
          .generateOrder(
              totalAmount: _totalAmount,
              quantity: _quantity,
              product: product,
              comment: comment)
          .then((_) {
        Navigator.pop(context, _product.currentConfigProduct.name);
      });
    });
  }

  void _addProductToSelection({BuildContext context}) {
    if (widget.dataProductConfig.section.products.length > 1) {
      widget.dataProductConfig.section.selected =
          widget.dataProductConfig.product;
      Navigator.pop(context, _product.currentConfigProduct.name);
    } else {
      widget.dataProductConfig.section.selected =
          widget.dataProductConfig.product;
    }
    Navigator.pop(context, _product.currentConfigProduct.name);
  }
}
