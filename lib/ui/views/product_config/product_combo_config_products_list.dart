import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/category.dart';
import 'package:tabitas/core/models/product.dart';
import 'package:tabitas/core/models/product_item.dart';
import 'package:tabitas/core/models/product_section.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/theme.dart';

class ProductComboConfigProductsList extends StatefulWidget {
  final DataProductConfig dataProductConfig;

  ProductComboConfigProductsList(this.dataProductConfig);

  @override
  _ProductComboConfigProductsListState createState() =>
      _ProductComboConfigProductsListState();
}

class _ProductComboConfigProductsListState
    extends State<ProductComboConfigProductsList> {
  bool _saved = false;
  ProductModel _product;
  ProductSection _section;
  Category _category;

  @override
  Widget build(BuildContext context) {
    final ProductModel productProvider = Provider.of<ProductModel>(context);
    _product = productProvider;
    productProvider.currentConfigComboProduct = widget.dataProductConfig.product;
    _section = widget.dataProductConfig.section;
    _category = widget.dataProductConfig.category;

    return Scaffold(
      appBar: AppBar(
        title: Text(_section.name),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _saved,
        dismissible: false,
        progressIndicator: CircularProgressIndicator(),
        child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.black26,
                ),
            itemCount: _section.products.length,
            itemBuilder: (buildContext, index) => GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => _onItemTap(
                      item: _section.products[index],
                      productProvider: productProvider),
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          _section.products[index].name,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              color: theme.accentColor,
                              fontSize: 19,
                              fontWeight: FontWeight.w800),
                        ),
                        _section.products[index].description == null
                            ? SizedBox()
                            : Text(
                                _section.products[index].description,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 14),
                              ),
                      ],
                    ),
                  ),
                )),
      ),
    );
  }

  _onItemTap({ProductItem item, ProductModel productProvider}) {
    setState(() {
      _saved = true;
    });
    productProvider.getProductById(item.id).then((product) {
      if (product.modifiers != null && product.modifiers.isNotEmpty) {
        DataProductConfig dataProductConfig = new DataProductConfig(
            product: product,
            category: _category,
            section: _section,
            variantId: item.variantId);
        setState(() {
          _saved = false;
        });
        Navigator.pushNamed(context, 'productConfig',
            arguments: dataProductConfig);
      } else {
        setState(() {
          _saved = false;
        });
        // Producto no es configurable
        _section.selected = product;
        if (_section.selected.variants != null) {
          if (_section.selected.variants.length >= 1) {
            _section.selected.variants
                .firstWhere((e) => e.id == item.variantId)
                .selected = true;
          }
        }
        Navigator.pop(context);
      }
    });
  }
}
