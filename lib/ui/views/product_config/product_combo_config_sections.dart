import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../core/models/product_item.dart';
import '../../../core/models/variant.dart';
import '../../../core/models/product.dart';
import '../../../core/models/product_section.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/product_model.dart';
import '../../../ui/theme.dart';
import '../../../ui/views/product_config/widgets/header_product_config.dart';
import '../../../ui/views/product_config/widgets/item_section.dart';
import '../../../ui/views/shared/shoppingCartBadge.dart';

class ProductComboConfigSections extends StatefulWidget {
  final DataProductConfig dataProductConfig;

  ProductComboConfigSections(this.dataProductConfig);

  @override
  _ProductComboConfigSectionsState createState() =>
      _ProductComboConfigSectionsState();
}

class _ProductComboConfigSectionsState
    extends State<ProductComboConfigSections> {
  ProductModel _product;
  OrderModel _orderModel;
  double _totalAmount = 0;
  int _quantity = 1;
  String comment;
  List<Widget> _sections = [];
  var f = new NumberFormat("##0.00", "es_ES");

  @override
  Widget build(BuildContext context) {
    final OrderModel orderProvider = Provider.of<OrderModel>(context);
    final ProductModel productProvider = Provider.of<ProductModel>(context);
    productProvider.currentConfigComboProduct =
        widget.dataProductConfig.product;
    productProvider.currentConfigProduct = widget.dataProductConfig.product;
    _product = productProvider;
    _orderModel = orderProvider;
    _calculateTotalAmount();

    return Scaffold(
      appBar: AppBar(
        title: Text('Configurador'),
        centerTitle: true,
        actions: <Widget>[
          widget.dataProductConfig.section != null
              ? SizedBox()
              : ShoppingCartBadge(
              onTap: _showShoppingCart,
              number: _orderModel.counterOrderLines())
        ],
      ),
      floatingActionButton: _checkCompletedSelections() == true
          ? FloatingActionButton.extended(
              backgroundColor: theme.accentColor,
              label: Text('Añadir'),
              icon: Icon(Icons.add),
              onPressed: () => _addProductToCart(
                  orderProvider: orderProvider,
                  product: _product.currentConfigProduct,
                  context: context),
            )
          : SizedBox(),
      body: FutureBuilder<dynamic>(
        future: _configSections(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  HeaderProductConfig(
                    price: _totalAmount,
                    onChangeQuantity: _onChangeQuantity,
                    setQuantity: true,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: _generateSections(),
                  )
                ]),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        }
      ),
    );
  }

  _configSections() async {

    for (ProductSection section in _product.currentConfigComboProduct.productsSections) {
      if (section.selected == null) {
        if (section.products.length == 1) {
          Product product = await _getProduct(section: section,
              productProvider: _product,
              item: section.products[0]);
          section.selected = product;
          if (section.selected.variants != null) {
            section.selected.variants
                .firstWhere((e) => e.id == section.products[0].variantId)
                .selected = true;
            Variant variant = section.selected.variants.firstWhere((e) => e.selected == true);
            section.selected.modifiers = variant.modifiers;
          }
        }
      }
    }
  }

  List<Widget> _generateSections() {
    List<Widget> sections = [];
    _product.currentConfigComboProduct.productsSections.forEach((section)  {
      sections.add(ItemSection(
        onTapSelect: _onTapSelect,
        section: section,
      ));
    });

    return sections;
  }

  _showShoppingCart() {
    Navigator.pushNamed(context, 'shoppingCart').then((value) {
      setState(() {});
    });
  }

  Future<Product> _getProduct({ProductSection section, ProductModel productProvider, ProductItem item}) async {
    Product product = await productProvider.getProductById(item.id);
    return product;
  }

  void _addProductToCart(
      {OrderModel orderProvider, Product product, BuildContext context}) {
    setState(() {
      orderProvider
          .generateOrder(
              totalAmount: _totalAmount,
              quantity: _quantity,
              product: product,
              comment: comment)
          .then((_) {
        Navigator.pop(context, _product.currentConfigComboProduct.name);
      });
    });
  }

  _calculateTotalAmount() {
    setState(() {
      _totalAmount = _product.setTotalComboAmount(
          product: _product.currentConfigComboProduct, quantity: _quantity);
    });
  }

  _onChangeQuantity({int quantity, int sign}) {
    setState(() {
      _quantity = quantity;
    });
  }

  _onTapSelect({ProductSection section}) {
    if (section.products.length == 1) {

      if (section.selected.modifiers != null && section.selected.modifiers.isNotEmpty) {
        print('modifiers not empty ${section.selected.modifiers}');
        DataProductConfig dataProductConfig = new DataProductConfig(
            product: section.selected,
            category: widget.dataProductConfig.category,
            section: section,
            variantId: section.products[0].variantId);
        Navigator.pushNamed(context, 'productConfig',
            arguments: dataProductConfig).then((value) {
              print(value);
              setState(() {});
        });
      } else {
        print('modifiers empty');
        section.selected.variants.firstWhere((e) => e.id == section.products[0].variantId).selected = true;
      }


    } else {
      DataProductConfig data = new DataProductConfig(
          section: section,
          category: widget.dataProductConfig.category,
          product: section.selected != null
              ? section.selected
              : _product.currentConfigComboProduct);
      Navigator.of(context)
          .pushNamed('productComboConfigProductsList', arguments: data)
          .then((_) {
        setState(() {});
      });
    }

  }

  bool _checkCompletedSelections() {
    int totalSections =
        _product.currentConfigComboProduct.productsSections.length;
    List<ProductSection> sections = _product
        .currentConfigComboProduct.productsSections
        .where((section) => section.selected != null)
        .toList();

    return totalSections == sections.length;
  }
}
