import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tabitas/core/models/category.dart';
import 'package:tabitas/core/viewmodels/product_model.dart';
import 'package:tabitas/ui/theme.dart';
import '../../../../core/models/product.dart';
import 'package:intl/intl.dart';

class ProductItem extends StatelessWidget {
  final Product productData;
  final Category categoryData;
  final bool descuentoDemo = false;
  final Function onItemTap;

  ProductItem({@required this.productData, @required this.categoryData, @required this.onItemTap});

  var f = new NumberFormat("##0.00", "es_ES");

  @override
  Widget build(BuildContext context) {
    //formats = _getFormats(this.productData.formats);

    return Consumer<ProductModel>(
      builder: (context, ProductModel product, _) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () => onItemTap(productData),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  productData.name,
                                  textAlign: TextAlign.start,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: TextStyle(
                                      color: theme.accentColor,
                                      fontSize: 19,
                                      fontWeight: FontWeight.w800),
                                ),
                              ),
                              (productData.isNew == true)
                                  ? Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Image.asset(
                                  'assets/images/icon_new.png',
                                  width: 30,
                                  height: 30,
                                ),
                              )
                                  : SizedBox(
                                height: 0,
                                width: 0,
                              ),
                            ],
                          ),
                          Text(
                            productData.description,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w300,
                                fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top:8.0, left: 80.0, right: 8.0, bottom: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          (productData.variants != null && productData.variants.isNotEmpty) ? Text(
                            'desde',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                             fontSize: 12
                            ),
                          ) : SizedBox(),
                          Text(
                            //productData.variants != null && productData.variants.isNotEmpty ? '${f.format(productData.variants[0].price)}€' : '${f.format(productData.price)}€',
                            '${f.format(productData.price)}€',

                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: theme.primaryColor,
                                fontWeight: FontWeight.w700,
                                fontSize: 20),
                          ),
                          /*
                          (descuentoDemo == true) ? Text(
                            '10,00€',
                            style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                            ),
                          ) : SizedBox(),
                          Text(
                            '${f.format(productData.price)}€',
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: theme.primaryColor,
                                fontWeight: FontWeight.w700,
                                fontSize: 20),
                          ),
                          */
                        ],
                      ),
                    ),
                  ],
                ),
                /*
              (formats.length > 1)
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: _createFormats(productData.formats),
                      ),
                    )
                  : SizedBox(
                      height: 0,
                      width: 0,
                    ),

               */
              ],
            ),
          ),
        );
      });
  }

  List<Widget> _createFormats(List<dynamic> rawFormats) {
    List<Widget> items = [];
    rawFormats.forEach((f) {
      ProductFormat format = ProductFormat.fromMap(f, f['id']);
      items.add(_buildFormatItem(format));
    });
    return items;
  }

  List<ProductFormat> _getFormats(List<dynamic> rawFormats) {
    List<ProductFormat> items = [];
    rawFormats.forEach((f) {
      ProductFormat format = ProductFormat.fromMap(f, f['id']);
      items.add(format);
    });

    return items;
  }

  Widget _buildFormatItem(ProductFormat format) {
    var f = new NumberFormat("##0.00", "es_ES");

    return Row(
      children: <Widget>[
        Text(
          '${format.name} (${format.description})',
          textAlign: TextAlign.left,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
          style: TextStyle(color: Colors.black54, fontSize: 14),
        ),
        Expanded(
          child: SizedBox(),
        ),
        Text(
          '${f.format(format.price)} €',
          textAlign: TextAlign.right,
          style: TextStyle(
              color: theme.primaryColor,
              fontSize: 14,
              fontWeight: FontWeight.w600),
        ),
      ],
    );
  }
}
