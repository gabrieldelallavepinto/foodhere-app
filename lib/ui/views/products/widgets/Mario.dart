import 'package:flutter/material.dart';
import '../../../../core/models/product.dart';

class ProductItem extends StatelessWidget {
  final Product productData;

  ProductItem({@required this.productData});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print(productData.toJson());
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(color: Colors.blue, width: 100, height: 50),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                  child: Text(
                    productData.name.toUpperCase(),
                style: TextStyle(
                  color: Colors.red,
                    fontSize: 20,
                    fontWeight: FontWeight.w800),
              )),
            ),
          ],
        ),
      ),
    );
  }
}
