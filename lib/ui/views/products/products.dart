import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../core/models/category.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../ui/views/shared/shoppingCartBadge.dart';
import '../../../core/models/product.dart';
import '../../../core/viewmodels/product_model.dart';
import '../../../ui/views/products/widgets/productItem.dart';

class Products extends StatefulWidget {
  final Category category;

  Products(this.category);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Product> products;
    final productProvider = Provider.of<ProductModel>(context);
    final orderProvider = Provider.of<OrderModel>(context);

    _showShoppingCart() {
      Navigator.pushNamed(context, 'shoppingCart').then((value) {
        setState(() {});
      });
    }

    _onItemTap(Product product) {
      if (orderProvider.order != null) {
        DataProductConfig data =
            new DataProductConfig(category: widget.category, product: product);
        Navigator.pushNamed(
                context,
                product.type == 'simple'
                    ? 'productConfig'
                    : 'productComboConfigSections',
                arguments: data)
            .then((productName) {
          productProvider.currentConfigProduct = null;
          setState(() {});
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category.name),
        centerTitle: true,
        actions: <Widget>[
          orderProvider.order != null
              ? ShoppingCartBadge(
                  onTap: _showShoppingCart,
                  number: orderProvider.counterOrderLines())
              : SizedBox()
        ],
      ),
      body: Container(
        child: StreamBuilder(
            stream: productProvider.fetchProductsAsStream(widget.category.id),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                products = snapshot.data.documents
                    .map((doc) => Product.fromMap(doc.data, doc.documentID))
                    .skipWhile((value) => value.isPublic == false)
                    .skipWhile((value) => value.isAvailable == false)
                    .toList();
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.black26,
                    ),
                    itemCount: products.length,
                    itemBuilder: (buildContext, index) => ProductItem(
                      productData: products[index],
                      categoryData: widget.category,
                      onItemTap: _onItemTap,
                    ),
                  ),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      ),
    );
  }

  int _counterProducts({List<dynamic> orderLines}) {
    if (orderLines == null) return 0;
    return orderLines.length;
  }
}
