import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';
import '../../../appConfig/appconfig.dart';
import '../../../core/viewmodels/userViewModel.dart';
import '../../../ui/theme.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKeyRegister = GlobalKey<FormState>();
  final _keyRegister = GlobalKey<ScaffoldState>();
  TextEditingController _passwordController =
      new TextEditingController(text: '');
  FocusNode _firstNameFocus = FocusNode();
  FocusNode _lastNameFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _repeatPasswordFocus = FocusNode();
  String _firstName, _lastName, _phone, _email, _password, _repeatPassword = '';
  bool _showVerification = false;

  int _otpCodeLength = 4;
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getSignatureCode();
  }

  /// get signature code
  _getSignatureCode() async {
    String signature = await SmsRetrieved.getAppSignature();
    print("signature $signature");
  }

  _onSubmitOtp() {
    setState(() {
      _isLoadingButton = !_isLoadingButton;
      _verifyOtpCode();
    });
  }

  _onOtpCallBack(String otpCode, bool isAutofill) {
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutofill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode();
      } else if (otpCode.length == _otpCodeLength && !isAutofill) {
        _enableButton = true;
        _isLoadingButton = false;
      } else {
        _enableButton = false;
      }
    });
  }

  _verifyOtpCode() {
    FocusScope.of(context).requestFocus(new FocusNode());
    Timer(Duration(milliseconds: 4000), () {
      setState(() {
        _isLoadingButton = false;
        _enableButton = false;
      });

      _keyRegister.currentState.showSnackBar(
          SnackBar(content: Text("Verification OTP Code $_otpCode Success")));
    });
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserViewModel>(context);

    return Scaffold(
        key: _keyRegister,
        appBar: AppBar(
          centerTitle: true,
          title: Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Image.asset(
                "assets/images/logo-white.png",
                fit: BoxFit.fill,
              ),
            ),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(SimpleLineIcons.menu,
                    size: 20, color: Colors.transparent),
                onPressed: null)
          ],
        ),
        body: _showVerification == false
            ? SingleChildScrollView(
                child: Form(
                  key: _formKeyRegister,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Registro de usuarios',
                          style: TextStyle(
                              color: theme.accentColor,
                              fontSize: 20,
                              fontWeight: FontWeight.w700),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            labelText: 'Nombre',
                            prefixIcon: Icon(Icons.account_circle),
                          ),
                          focusNode: _firstNameFocus,
                          onFieldSubmitted: (_) {
                            fieldFocusChange(
                                context, _firstNameFocus, _lastNameFocus);
                          },
                          onSaved: (firstName) => _firstName = firstName,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          focusNode: _lastNameFocus,
                          decoration: InputDecoration(
                            labelText: 'Apellidos',
                            prefixIcon: Icon(Icons.account_circle),
                          ),
                          onFieldSubmitted: (_) {
                            fieldFocusChange(
                                context, _lastNameFocus, _phoneFocus);
                          },
                          onSaved: (lastName) => _lastName = lastName,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          focusNode: _phoneFocus,
                          decoration: InputDecoration(
                            labelText: 'Teléfono *',
                            prefixIcon: Icon(Icons.phone),
                          ),
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(9)
                          ],
                          onFieldSubmitted: (value) {
                            fieldFocusChange(context, _phoneFocus, _emailFocus);
                          },
                          validator: (phone) {
                            if (phone.isEmpty) {
                              return 'El teléfono es requerido';
                            } else if (phone.length < 9) {
                              return 'El formato no es correcto';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (phone) => _phone = phone,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Text(
                            'Datos de acceso',
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.next,
                          focusNode: _emailFocus,
                          decoration: InputDecoration(
                            labelText: 'Email *',
                            prefixIcon: Icon(Icons.email),
                          ),
                          validator: (email) => EmailValidator.validate(email)
                              ? null
                              : "Formato de email inválido",
                          onFieldSubmitted: (_) {
                            fieldFocusChange(
                                context, _emailFocus, _passwordFocus);
                          },
                          onSaved: (email) => _email = email,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.visiblePassword,
                          textInputAction: TextInputAction.next,
                          focusNode: _passwordFocus,
                          maxLength: 12,
                          controller: _passwordController,
                          decoration: InputDecoration(
                            labelText: 'Contraseña12345a *',
                            prefixIcon: Icon(Icons.lock),
                          ),
                          validator: (password) {
                            Pattern pattern =
                                r'^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$';
                            RegExp regex = new RegExp(pattern);
                            if (!regex.hasMatch(password))
                              return 'Debe contener letras, número y mínimo 6 caractéres';
                            else
                              return null;
                          },
                          onFieldSubmitted: (_) {
                            fieldFocusChange(
                                context, _passwordFocus, _repeatPasswordFocus);
                          },
                          onSaved: (password) => _password = password,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.visiblePassword,
                          textInputAction: TextInputAction.done,
                          focusNode: _repeatPasswordFocus,
                          maxLength: 12,
                          decoration: InputDecoration(
                            labelText: 'Repetir contraseña *',
                            prefixIcon: Icon(Icons.lock),
                          ),
                          validator: (password) {
                            Pattern pattern =
                                r'^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$';
                            RegExp regex = new RegExp(pattern);
                            if (password != _passwordController.text) {
                              return 'Las contraseñas no coinciden';
                            } else {
                              if (!regex.hasMatch(password)) {
                                return 'Debe contener letras, número y mínimo 6 caractéres';
                              } else {
                                return null;
                              }
                            }
                          },
                          onSaved: (password) => _repeatPassword = password,
                        ),
                        RaisedButton(
                            child: Text('Desconectar'),
                            onPressed: () {
                              userProvider.signOut().then((value) {
                                Navigator.pushReplacementNamed(context, 'home');
                              });
                            }),
                        RaisedButton(
                          child: Text('Registrar'),
                          onPressed: () {
                            if (_formKeyRegister.currentState.validate()) {
                              _formKeyRegister.currentState.save();

                              userProvider
                                  .register(_email, _password)
                                  .then((user) async {
                                Map<String, dynamic> data = {
                                  'first_name': _firstName,
                                  'last_name': _lastName,
                                  'phone': _phone,
                                  'email': _email,
                                  'customer_id': 'new',
                                  'business_id': AppConfig.businessId
                                };

                                await userProvider
                                    .createUserProfile(user.uid, data)
                                    .then((_) {
                                  print(userProvider.userProfile.toJson());
                                  Navigator.pushReplacementNamed(
                                      context, 'home',
                                      arguments: userProvider.userProfile);
                                });
                              }).catchError((e) async {
                                String errorMessage = '';
                                switch (e.code) {
                                  case 'ERROR_EMAIL_ALREADY_IN_USE':
                                    final HttpsCallable callable =
                                        CloudFunctions.instance
                                            .getHttpsCallable(
                                      functionName: 'auth-getUserId',
                                    );

                                    dynamic resp =
                                        await callable.call(<String, dynamic>{
                                      'email': _email,
                                    });
                                    DocumentSnapshot snapshot =
                                        await userProvider.getClientProfileById(
                                            resp.data['uid']);
                                    if (snapshot.data != null) {
                                      errorMessage =
                                          'Error, el email introducido ya está en uso.';
                                    } else {
                                      Map<String, dynamic> data = {
                                        'first_name': _firstName,
                                        'last_name': _lastName,
                                        'phone': _phone,
                                        'email': _email,
                                        'customer_id': 'new',
                                        'business_id': AppConfig.businessId
                                      };
                                      await userProvider
                                          .createUserProfile(
                                              resp.data['uid'], data)
                                          .then((_) async {
                                        print(
                                            userProvider.userProfile.toJson());
                                        if (await userProvider.signIn(
                                            _email, _password)) {
                                          Navigator.pushReplacementNamed(
                                              context, 'home',
                                              arguments:
                                                  userProvider.userProfile);
                                        }
                                      });
                                    }
                                    break;
                                  case 'ERROR_TOO_MANY_REQUESTS':
                                    errorMessage =
                                        'Error, se ha intentado registrar muchas veces, pruebe mas tarde.';
                                    break;
                                  case 'ERROR_NETWORK_REQUEST_FAILED':
                                    errorMessage =
                                        'Error, parece que hay algún problema de conexión, pruebe mas tarde.';
                                    break;
                                  default:
                                    errorMessage =
                                        'Se ha producido un error inesperado, por favor intentelo mas tarde';
                                    break;
                                }
                                _keyRegister.currentState.showSnackBar(SnackBar(
                                  content: Text(errorMessage),
                                ));
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextFieldPin(
                        filled: true,
                        codeLength: _otpCodeLength,
                        boxSize: 46,
                        filledAfterTextChange: false,
                        textStyle: TextStyle(fontSize: 16),
                        borderStyle: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(34)),
                        onOtpCallback: (code, isAutofill) =>
                            _onOtpCallBack(code, isAutofill),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      Container(
                        child: MaterialButton(
                          onPressed: _enableButton ? _onSubmitOtp : null,
                          child: _setUpButtonChild(),
                          color: Colors.blue,
                          disabledColor: Colors.blue[100],
                        ),
                      )
                    ],
                  ),
                ),
              ));
  }

  Widget _setUpButtonChild() {
    if (_isLoadingButton) {
      return Container(
        width: 19,
        height: 19,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );
    } else {
      return Text(
        "Verficar",
        style: TextStyle(color: Colors.white),
      );
    }
  }

  void fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

}
