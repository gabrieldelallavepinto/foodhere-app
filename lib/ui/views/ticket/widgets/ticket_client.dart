import 'package:flutter/material.dart';
import '../../../../core/models/order.dart';

class TicketClient extends StatelessWidget {
  final Order order;

  TicketClient({@required this.order});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          '--------------------------------------------------------',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
        Text(
          'CLIENTE: ${order.client.firstName} ${order.client.lastName}',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
        Text(
          'TELÉFONO: ${order.client.phone}',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'ENTREGA: ${_getOrderType().toUpperCase()}',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
        order.type == 'delivery'
            ? Text(
          'DIRECCIÓN ENTREGA:',
          textAlign: TextAlign.left,
          maxLines: 1,
        )
            : SizedBox(),
        order.type == 'delivery'
            ? Text(
          order.deliveryAddress.formattedAddress,
          textAlign: TextAlign.left,
        )
            : SizedBox(),
        order.type != 'delivery'
            ? Text(
          'DIRECCIÓN RECOGIDA:',
          textAlign: TextAlign.left,
          maxLines: 1,
        )
            : SizedBox(),
        order.type != 'delivery'
            ? Text(
          order.store.name,
          textAlign: TextAlign.left,
        )
            : SizedBox(),
        order.type != 'delivery'
            ? Text(
          order.store.address.formattedAddress,
          textAlign: TextAlign.left,
        )
            : SizedBox(),
      ],
    );
  }

  String _getOrderType() {
    switch (order.type) {
      case 'delivery':
        return 'Domicilio';
        break;
      case 'collect':
        return 'Recoger en local';
        break;
      case 'in_situ':
        return 'Tomar en local';
        break;
      default:
        return '';
        break;
    }
  }
}
