import 'package:flutter/material.dart';
import 'package:tabitas/core/models/order.dart';
import 'package:tabitas/ui/views/ticket/widgets/order_line.dart';
import 'package:tabitas/ui/views/ticket/widgets/order_line_delivery.dart';

class TicketConcepts extends StatelessWidget {

  final Order order;
  TicketConcepts({@required this.order});

  @override
  Widget build(BuildContext context) {
    return _generateConcepts();
  }

  Widget _generateConcepts() {
    return Column(
      children: _generateOrderLines(),
    );
  }

  List<Widget> _generateOrderLines() {
    List<Widget> widgets = [];
    order.orderLines.forEach((orderLine) {
      widgets.add(OrderLineDetail(orderLine: orderLine));
    });

    if (order.type == 'delivery') {
      widgets.add(OrderLineDelivery(deliveryAmount: order.deliveryAmount,));
    }
    return widgets;
  }
}
