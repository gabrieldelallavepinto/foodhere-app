import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OrderLineDelivery extends StatelessWidget {
  final double deliveryAmount;

  OrderLineDelivery({this.deliveryAmount});

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("##0.00", "es_ES");
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'ENVÍO A DOMICILIO',
          textAlign: TextAlign.left,
          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black87),
        ),
        Text(
          '${f.format(deliveryAmount)} €',
        ),
      ],
    );
  }
}
