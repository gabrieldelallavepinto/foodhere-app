import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tabitas/core/models/order.dart';
class TicketTotal extends StatelessWidget {

  final Order order;
  TicketTotal({@required this.order});

  @override
  Widget build(BuildContext context) {
    NumberFormat f = new NumberFormat("##0.00", "es_ES");
    return Column(
      children: [
        Text(
          '--------------------------------------------------------',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              'BASE IMPONIBLE',
              textAlign: TextAlign.right,
            ),
            Container(
              width: 60,
              child: Text(
                '${f.format(_getBase())} €',
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              'IVA 10%',
              textAlign: TextAlign.right,
            ),
            Container(
              width: 60,
              child: Text(
                '${f.format(_getTaxAmount())} €',
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              'TOTAL',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontWeight: FontWeight.w700, fontSize: 20),
            ),
            Container(
              width: 60,
              child: Text(
                '${f.format(_getTotalAmount())} €',
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontWeight: FontWeight.w700, fontSize: 20),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),

      ],
    );
  }

  double _getBase() {
    return _getTotalAmount() - _getTaxAmount();
  }

  double _getTaxAmount() {
    double tax = 0.1;
    return _getTotalAmount() * tax ;
  }

  double _getTotalAmount() {

    double deliveryAmount = 0;
    if (order.type == 'delivery') {
      deliveryAmount = order.deliveryAmount;
    }
    return order.totalAmount + deliveryAmount;
  }
}
