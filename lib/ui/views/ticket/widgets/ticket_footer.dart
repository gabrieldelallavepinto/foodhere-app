import 'package:flutter/material.dart';

class TicketFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'IVA INCLUIDO',
          textAlign: TextAlign.center,
        ),
        Text(
          'GRACIAS POR SU VISITA',
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
