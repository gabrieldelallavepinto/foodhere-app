import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tabitas/core/models/order_line.dart';

class OrderLineDetail extends StatelessWidget {

  final OrderLine orderLine;

  OrderLineDetail({@required this.orderLine});

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("##0.00", "es_ES");

    print(orderLine);
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('${orderLine.quantity}  '),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  orderLine.label.toUpperCase(),
                  textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black87)
                ),
                orderLine.type == 'PRODUCT_SIMPLE' ? _generateOptions(orderLine: orderLine) : _generateProducts(),
              ],
            ),
          ),
          Text(
            '${f.format(orderLine.totalAmount)} €',
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }

  Widget _generateOptions({OrderLine orderLine}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _generateModifier(orderLine: orderLine),
    );
  }

  List<Widget> _generateModifier({OrderLine orderLine}) {
    List<Widget> widgets = [];
    if (orderLine.childrenOrderLines != null && orderLine.childrenOrderLines.isNotEmpty) {
      /*
      List<String> modifiers = orderLine.childrenOrderLines?.map((e) => e.modifierName.toString())?.toSet()?.toList();
      modifiers?.forEach((modifierName) {
        widgets.add(Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: Text('   ${modifierName.toUpperCase()}:',
                style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54),
              ),
            ),
            Expanded(
              child: Text(
                _composeOptions(orderLine.childrenOrderLines.where((e) => e.modifierName == modifierName).toList()),
                style: TextStyle( color: Colors.black54),
              ),
            )
          ],
        ));
      });
       */
      orderLine.childrenOrderLines?.forEach((modifierOrderLine) {
        widgets.add(Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: Text('   ${modifierOrderLine.label.toUpperCase()}:',
                style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54),
              ),
            ),
            Expanded(
              child: Text(
                _composeOptions(options: modifierOrderLine.childrenOrderLines),
                style: TextStyle( color: Colors.black54),
              ),
            )
          ],
        ));
      });
    }

    return widgets;
  }

  String _composeOptions({List<OrderLine> options}) {
    var f = new NumberFormat("##0.00", "es_ES");
    if (options == null) return '';
    String result = '';
    int counter = options.length;
    options.forEach((option) {
      if (option.modifierShowSelectedOptions == true) {
        String optionItem = '';
        if (option.totalAmount == 0) {
          optionItem += option.quantity > 1
              ? '${option.quantity} x ${option.label.toUpperCase()}'
              : '${option.label.toUpperCase()}';
        } else {
          optionItem += option.quantity > 1
              ? '${option.quantity} x ${option.label.toUpperCase()}(${f.format(option.totalAmount)}€)'
              : '${option.label.toUpperCase()}(${f.format(option.totalAmount)}€)';
        }
        counter--;
        if (counter > 0) {
          optionItem += ', ';
        }
        result += optionItem;
      }

    });
    int counterUnselected = options.length;
    options.forEach((option) {

      if (option.modifierShowUnselectedOptions == true) {
        if (option.quantity == 0) {
          result += 'SIN ${option.label.toUpperCase()}';
        }

        counterUnselected--;
        if (counterUnselected > 0) {
          result += ', ';
        }
      }
    });
    return result;
  }

  List<Widget> _generateProduct() {

    List<Widget> widgets = [];
    orderLine.childrenOrderLines.forEach((product) {
      widgets.add(
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('  ${product.label}', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54),),
              _generateOptions(orderLine: product)
            ],
          )
      );
    });

    return widgets;
  }

  Widget _generateProducts() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _generateProduct(),
    );
  }
}
