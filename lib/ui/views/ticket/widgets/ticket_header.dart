import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../core/models/business.dart';
import '../../../../core/models/order.dart';

class TicketHeader extends StatelessWidget {

  final Order order;
  final Business business;
  TicketHeader({@required this.order, @required this.business});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment:CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 50,
            child: FittedBox(
              fit: BoxFit.fitHeight,
              child: Image.asset('assets/images/logo_ticket.png',
                  color: Colors.black87),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          business.businessName.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.w900),
        ),
        Text(
          'CIF: ${business.documentation}',
          textAlign: TextAlign.center,
        ),
        Text(
          business.address.formattedAddress,
          textAlign: TextAlign.center,
          maxLines: 2,
        ),
        Text(
          'Tel. ${business.phone}${business.optionalPhone != null && business.optionalPhone != '' ? '/ ${business.optionalPhone}' : ''}',
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 20),
        Text(
          'FECHA: ${_getDate()}',
          textAlign: TextAlign.left,
        ),
        order.type != 'delivery' ? Text(
          'HORA RECOGIDA: ${order.dateLabel}',
          textAlign: TextAlign.left,
        ) : SizedBox(),
        order.type == 'delivery' ? Text(
          'HORA ENTREGA: ${order.dateLabel}',
          textAlign: TextAlign.left,
        ) : SizedBox(),
        Text(
          'FACTURA: #${order.ref}',
          textAlign: TextAlign.left,
        ),
        Text(
          '--------------------------------------------------------',
          textAlign: TextAlign.left,
          maxLines: 1,
        ),
      ],
    );
  }

  String _getDate() {
    DateTime date = order.date.toDate();
    DateFormat formatter = DateFormat.yMd().add_Hms();
    String formatted = formatter.format(date);
    return formatted;
  }
}
