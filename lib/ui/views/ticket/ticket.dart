import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../../../core/models/order.dart';
import '../../../core/viewmodels/business_model.dart';

import '../../../ui/views/ticket/widgets/ticket_client.dart';
import '../../../ui/views/ticket/widgets/ticket_concepts.dart';
import '../../../ui/views/ticket/widgets/ticket_footer.dart';
import '../../../ui/views/ticket/widgets/ticket_header.dart';
import '../../../ui/views/ticket/widgets/ticket_total.dart';

class Ticket extends StatefulWidget {
  final Order order;

  Ticket({@required this.order});

  @override
  _TicketState createState() => _TicketState();
}

class _TicketState extends State<Ticket> {
  var f = new NumberFormat("##0.00", "es_ES");

  @override
  Widget build(BuildContext context) {
    final BusinessModel businessProvider = Provider.of<BusinessModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Ticket'),
      ),
      body: Theme(
        data: ThemeData(fontFamily: 'Receipt'),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    TicketHeader(
                        order: widget.order,
                        business: businessProvider.business),
                    SizedBox(height: 10),
                    TicketConcepts(order: widget.order),
                    SizedBox(height: 10),
                    TicketTotal(order: widget.order),
                    TicketClient(order: widget.order),
                    SizedBox(height: 50),
                    TicketFooter(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
