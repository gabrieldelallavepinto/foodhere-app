import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import '../../../core/models/category.dart';
import '../../../core/models/product.dart';
import '../../../core/viewmodels/order_model.dart';
import '../../../core/viewmodels/product_model.dart';
import '../../../ui/views/shared/shoppingCartBadge.dart';
import 'widgets/promotion_item.dart';
import '../../../core/viewmodels/promotion_model.dart';
import '../../../core/models/promotion.dart';

class Promotions extends StatefulWidget {
  final Category category;

  Promotions(this.category);

  @override
  _PromotionsState createState() => _PromotionsState();
}

class _PromotionsState extends State<Promotions> {
  List<dynamic> promotions;
  bool _saved = false;

  @override
  Widget build(BuildContext context) {
    final PromotionModel promotionsProvider =
        Provider.of<PromotionModel>(context);
    final OrderModel orderProvider = Provider.of<OrderModel>(context);
    final ProductModel productProvider = Provider.of<ProductModel>(context);

    _onTapPromotion({Promotion promotion}) {
      if (promotion.action.link != null) {
        switch (promotion.action.link) {
          case 'product':
            {
              print(
                  'open a product id: ${promotion.action.filters.products[0].id}');
              print(
                  'open a product name: ${promotion.action.filters.products[0].name}');
              _getProduct(orderProvider, productProvider,
                  promotion.action.filters.products[0].id);
            }
        }
      }
    }

    _showShoppingCart() {
      Navigator.pushNamed(context, 'shoppingCart').then((value) {
        setState(() {});
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category.name),
        centerTitle: true,
        actions: <Widget>[
          orderProvider.order != null
              ? ShoppingCartBadge(
                  onTap: _showShoppingCart,
                  number: orderProvider.counterOrderLines())
              : SizedBox()
        ],
      ),
      body: ModalProgressHUD(
        inAsyncCall: _saved,
        dismissible: false,
        progressIndicator: CircularProgressIndicator(),
        child: Container(
          child: StreamBuilder(
              stream: promotionsProvider.fetchPromotionsAsStream(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  promotions = snapshot.data.documents
                      .map((doc) => Promotion.fromMap(doc.data))
                      .toList();
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      itemCount: promotions.length,
                      itemBuilder: (buildContext, index) => PromotionItem(
                        promotionDetails: promotions[index],
                        onTap: _onTapPromotion,
                      ),
                    ),
                  );
                } else {
                  return Text('fetchings');
                }
              }),
        ),
      ),
    );
  }

  _getProduct(
      OrderModel orderProvider, ProductModel productProvider, String id) {
    if (orderProvider.order == null) return;
    setState(() {
      _saved = true;
    });
    productProvider.getProductById(id).then((product) {
      DataProductConfig data =
          new DataProductConfig(product: product, category: widget.category);
      setState(() {
        _saved = false;
      });
      Navigator.pushNamed(
              context,
              product.type == 'simple'
                  ? 'productConfig'
                  : 'productComboConfigSections',
              arguments: data)
          .then((productName) {
        setState(() {});
      });
    });
  }
}
