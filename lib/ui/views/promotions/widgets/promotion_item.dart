import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:tabitas/core/models/image_config.dart';
import 'package:tabitas/core/services/promotion_service.dart';
import '../../../../core/models/promotion.dart';

class PromotionItem extends StatelessWidget {
  final Promotion promotionDetails;
  final Function onTap;

  PromotionItem({@required this.promotionDetails, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: GestureDetector(
        onTap: () => _onTapPromotion(),
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: CachedNetworkImage(
                imageUrl: PromotionService.getPromoList(promotionDetails),
                placeholder: (context, url) => Container(
                  padding: EdgeInsets.all(10.0),
                  width: double.infinity,
                  child: SpinKitCircle(
                    color: Colors.greenAccent,
                    size: 40,
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _onTapPromotion() {
    onTap(promotion: promotionDetails);
  }
}
