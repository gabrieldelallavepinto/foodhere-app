import 'package:flutter/material.dart';

final ThemeData theme = new ThemeData(
    fontFamily: 'Poppins',
    primaryColor: Color.fromARGB(255, 229, 30, 37),
    accentColor: Color.fromARGB(255, 0, 152, 77),
    scaffoldBackgroundColor: Colors.grey[200],
);