import 'package:flutter/material.dart';
import '../core/models/link.dart';
import 'views/docView/docView.dart';
import '../core/models/category.dart';
import '../core/models/client.dart';
import '../core/models/order.dart';
import '../core/models/product.dart';
import 'views/confirmation/order_confirmation.dart';
import 'views/delivery_addresses/delivery_addresses.dart';
import 'views/login/login.dart';
import 'views/my_orders/orders.dart';
import 'views/my_profile/my_profile.dart';
import 'views/product_config/product_combo_config_products_list.dart';
import 'views/product_config/product_combo_config_sections.dart';
import 'views/product_config/product_config.dart';
import 'views/promotions/promotions.dart';
import 'views/register/register.dart';
import 'views/remember/remember.dart';
import 'views/shopping_cart/shopping_cart.dart';
import 'views/stores/store_selection.dart';
import 'views/stripe_payment/card_payment.dart';
import 'views/ticket/ticket.dart';
import 'views/tracking/tracking_order.dart';
import 'views/type/type_order.dart';
import 'views/categories/categories.dart';
import 'views/products/products.dart';
import 'views/loading/loading.dart';
import 'views/start/start.dart';
import 'views/home/home.dart';

import '../wrapper.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'wrapper':
        return MaterialPageRoute(builder: (_) => Wrapper());
      case 'loading':
        return MaterialPageRoute(builder: (_) => Loading());
      case 'start':
        return MaterialPageRoute(builder: (_) => Start());
      case 'home':
        var user = settings.arguments as Client;
        return MaterialPageRoute(builder: (_) => Home(user: user));
      case 'categories':
        return MaterialPageRoute(builder: (_) => Categories());
      case 'typeOrder':
        var isCancellable = settings.arguments as bool;
        return MaterialPageRoute(builder: (_) => TypeOrder(cancelOrder: isCancellable,));
      case 'products':
        var data = settings.arguments as Category;
        return MaterialPageRoute(builder: (_) => Products(data));
      case 'login':
        return MaterialPageRoute(builder: (_) => Login());
      case 'register':
        return MaterialPageRoute(builder: (_) => Register());
      case 'remember':
        return MaterialPageRoute(builder: (_) => Remember());
      case 'profile':
        return MaterialPageRoute(builder: (_) => MyProfile());
      case 'promotions':
        var data = settings.arguments as Category;
        return MaterialPageRoute(builder: (_) => Promotions(data));
      case 'docView':
        var data = settings.arguments as DataLinkConfig;
        return MaterialPageRoute(builder: (_) => DocView(data));

      case 'productConfig':
        var data = settings.arguments as DataProductConfig;
        return MaterialPageRoute(builder: (_) => ProductConfig(data));

      case 'productComboConfigSections':
        var data = settings.arguments as DataProductConfig;
        return MaterialPageRoute(builder: (_) => ProductComboConfigSections(data));
      case 'productComboConfigProductsList':
        var data = settings.arguments as DataProductConfig;
        return MaterialPageRoute(builder: (_) => ProductComboConfigProductsList(data));

      case 'shoppingCart':
        return MaterialPageRoute(builder: (_) => ShoppingCart());

      case 'confirmation':
        return MaterialPageRoute(builder: (_) => OrderConfirmation());

      case 'ticket':
        Order order = settings.arguments as Order;
        return MaterialPageRoute(builder: (_) => Ticket(order: order));

      case 'myOrders':
        return MaterialPageRoute(builder: (_) => Orders());

      case 'trackingOrder':
        dynamic data = settings.arguments;
        return MaterialPageRoute(builder: (_) => TrackingOrder(data: data));

      case 'cardPayment':
        return MaterialPageRoute(builder: (_) => CardPayment());

      case 'deliveryAddresses':
        var isEditor = settings.arguments as bool;
        return MaterialPageRoute(builder: (_) => DeliveryAddresses(editor: isEditor,));
      case 'StoreSelector':
        dynamic data = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => StoreSelecction(data: data));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
